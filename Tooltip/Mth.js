var Mth = {

	E: 2.718281828459045,
	LN2: 0.6931471805599453,
	LN10: 2.302585092994046,
	LOG2E: 1.4426950408889634,
	LOG10E: 0.4342944819032518,
	PI: 3.141592653589793,
	SQRT1_2: 0.7071067811865476,
	SQRT2: 1.4142135623730951,

	floor: function(x) {
		return x | 0;
	},

	ceil: function(x) {
		return x + 1 | 0;
	},

	round: function(x) {
		return x + 0.5 | 0;
	},

	abs: function(x) {
		return x > 0 ? x : -x;
	},

	pow: function(value, exp) {
		var result = value;
		while(--exp) { result *= value; }
		return result;
	},

	max: Math.max,
	min: Math.min,
	random: Math.random,
	acos: Math.acos,
	asin: Math.asin,
	atan: Math.atan,
	atan2: Math.atan2,
	cos: Math.cos,
	exp: Math.exp,
	log: Math.log,
	sin: Math.sin,
	sqrt: Math.sqrt,
	tan: Math.tan
};
