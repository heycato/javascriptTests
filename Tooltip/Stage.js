function Stage() {

	var _selectable = false,
		_backgroundColor = false;

	var name = 'Stage';

	var stage = {

		children: [],
		dragElements: [],
		activeFocus:undefined,
		element:document.body,
		listeners: globalEvents.listeners,
		dispatchEvent: globalEvents.dispatchEvent,
		addEventListener: globalEvents.addEventListener,
		removeEventListener: globalEvents.removeEventListener,

		addChild: function(child) {
			if(!child.element.parentNode) {
				document.body.appendChild(child.element);
			}
			child.parent = this;
			if(child.id) this[child.id] = child;
			this.children.push(child);
			child.dispatchEvent(CHILD_ADDED, child);
		},

		get topParentName() {
			return name;
		},

		addChildAt: function(child, index) {

		},

		removeChild: function(child) {
			if(child.removeLoader) child.removeLoader();
			if(child.element.parentNode) {
				document.body.removeChild(child.element);
			}
			if(child.id) delete this[child.id];
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					this.children.splice(i, 1);
				}
			}
			child = null;
		},

		removeChildren: function(parent) {
			if(parent && parent.children && parent.children.length > 0) {
				var i = parent.children.length;
				while(i--) {
					this.removeChildren(parent.children[i]);
					parent.removeChild(parent.children[i]);
				}
			}
		},

		recycleChild: function(child) {
			if(child.children.length > 0) {
				this.recycleChildren(child.children);
			} else {
				if(child.removeLoader) child.removeLoader();
				if(child.element.parentNode) {
					child.element.parentNode.removeChild(child.element);
				}
				if(child.id) child.parent[child.id] = null;
				var i = child.parent.children.length;
				while(i--) {
					if(child.parent.children[i] === child) {
						child.parent.children.splice(i, 1);
					}
				}
				this.pushToPool(child);
			}
		},

		recycleChildren: function(children) {
			var i = children.length;
			while(i--) {
				if(children[i].children.length > 0) {
					this.recycleChildren(children[i].children);
				}
				this.recycleChild(children[i]);
			}
		},

		pushToPool: function(child) {
			if(child.className) {
				switch(child.className) {
					case 'Bitmap':
						child.clean();
						/*trace('pushing Bitmap to pool');*/
						bitmapPool.push(child);
						break;
					case 'Button':
						child.clean();
						/*trace('pushing Button to pool');*/
						buttonPool.push(child);
						break;
					case 'Input':
						child.clean();
						/*trace('pushing Input to pool');*/
						inputPool.push(child);
						break;
					case 'Sprite':
						child.clean();
						/*trace('pushing Sprite to pool');*/
						/*trace(child);*/
						spritePool.push(child);
						break;
					case 'Svg':
						child.clean();
						/*trace('pushing Svg to pool');*/
						svgPool.push(child);
						break;
					case 'TextArea':
						child.clean();
						/*trace('pushing TextArea to pool');*/
						textAreaPool.push(child);
						break;
					case 'TextField':
						child.clean();
						/*trace('pushing TextField to pool');*/
						textFieldPool.push(child);
						break;
					case 'VideoPlayer':
						child.clean();
						/*trace('pushing VideoPlayer to pool');*/
						videoPlayerPool.push(child);
						break;
				}
			}
		},

		moveToTop: function(child) {
			var highestZ = 0;
			var i = this.children.length;
			while(i--) {
				if(this.children[i].zIndex > highestZ && !this.children[i] === child) {
					highestZ = this.children[i].zIndex + 1;
				}
			}
			child.zIndex = highestZ;
		},

		domContentLoaded: function() {
			if(!_selectable) {
				document.body.style.overflow = 'hidden';
			}
			this.dispatchEvent(LOAD, this);
		},

		documentReadyListener: function() {
			if(typeof document.onreadystatechange === 'object' || document.readyState === 'loading') {
				document.onreadystatechange = function() {
					if(document.readyState === 'complete') {
						stage.domContentLoaded();
					}
				};
			} else {
				document.addEventListener("DOMContentLoaded", stage.domContentLoaded, false );
			}
		},

		globalToLocal: function (point) {
			return {x:point.x, y:point.y};
		},

		localToGlobal: function (point) {
			return {x:point.x, y:point.y};
		},

		event: function(e) {

			var event = {
				type:e.type,
				target:e.target,
				currentTarget:e.target,
				x:e.x,
				y:e.y,
				keyCode:e.which,
				pageX:e.pageX,
				pageY:e.pageY,
				screenX:e.screenX,
				screenY:e.screenY,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX:e.clientX - stage.stageX,
				mouseY:e.clientY - stage.stageY,
				deltaX:-e.wheelDeltaX || -e.wheelDelta * 2 || e.deltaX || e.detail || 0,
				deltaY:-e.wheelDeltaY || -e.wheelDelta * 2 || e.deltaY || e.detail || 0,
				preventDefault: function() {
					e.preventDefault();
				},
				stopPropagation: function() {
					e.stopPropagation();
				},
				dataTransfer: e.dataTransfer
			};

			if(e.touches) event.touches = e.touches;

			if(e.type === "wheel") {
				event.type = MOUSE_WHEEL;
			}

			if(e.type === RESIZE || e.type === GESTURE_CHANGE || e.type === GESTURE_END || e.type === ORIENTATION_CHANGE) {
				_zoom = document.documentElement.clientWidth / window.innerWidth;
			}

			if(e.type === FOCUS) {
				if(stage.activeFocus && stage.activeFocus !== stage) {
					stage.activeFocus.dispatchEvent(BLUR, event);
					stage.activeFocus = stage;
				}
			}

			if(e.type === MOUSE_DOWN || e.type === TOUCH_START) {
				if(stage.activeFocus) stage.activeFocus.dispatchEvent(BLUR, event);
				stage.focus();
				setTimer(stage.dispatchEvent, 0, [FOCUS, event], stage);
			}

			if(Scrolls) {
				Scrolls.hitMask(e.clientX, e.clientY);
			}

			stage.dispatchEvent(e.type, event);
		},

		get getChildren() {
			return children;
		},
		get images() {
			return document.images;
		},
		get forms() {
			return document.forms;
		},
		get links() {
			return document.links;
		},
		get anchors() {
			return document.anchors;
		},
		get scripts() {
			return document.scripts;
		},
		get plugins() {
			return document.plugins;
		},
		get embeds() {
			return document.embeds;
		},
		get width() {
			return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		},
		get height() {
			return window.innerHeight || document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
		},
		get stageX() {
			return 0;
		},
		get stageY() {
			return 0;
		},
		get selectable() {
			return _selectable;
		},
		set selectable(value) {
			_selectable = value;
		},
		get backgroundColor() {
			return _backgroundColor;
		},
		set backgroundColor(value) {
			_backgroundColor = value;
			document.body.style.backgroundColor = value;
		},

		focus: function() {
			stage.activeFocus = stage;
		},
		blur: function() {}
	};

	stage.proxy = stage;

	window.addEventListener(TOUCH_START, stage.event, false);
	window.addEventListener(TOUCH_MOVE, stage.event, false);
	window.addEventListener(TOUCH_END, stage.event, false);
	window.addEventListener(TOUCH_CANCEL, stage.event, false);
	window.addEventListener(MOUSE_OVER, stage.event, false);
	window.addEventListener(MOUSE_DOWN, stage.event, false);
	window.addEventListener(MOUSE_MOVE, stage.event, false);
	window.addEventListener(MOUSE_UP, stage.event, false);
	window.addEventListener(MOUSE_OUT, stage.event, false);
	window.addEventListener(CLICK, stage.event, false);
	window.addEventListener(RESIZE, stage.event, false);
	window.addEventListener(ORIENTATION_CHANGE, stage.event, false);
	window.addEventListener(SCROLL, stage.event, false);
	window.addEventListener(GESTURE_START, stage.event, false);
	window.addEventListener(GESTURE_CHANGE, stage.event, false);
	window.addEventListener(GESTURE_END, stage.event, false);
	window.addEventListener(KEY_UP, stage.event, false);
	window.addEventListener(KEY_DOWN, stage.event, false);

	var resizeTimer;

	window.addEventListener('resize', function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			stage.dispatchEvent(RESIZE_END, e);
		}, 200);
	}, false);

	if("onmousewheel" in document) {
		window.addEventListener(MOUSE_WHEEL, stage.event, false);
	} else if("onwheel" in document) {
		window.addEventListener("wheel", stage.event, false);
	} else if("MozMousePixelScroll" in document) {
		window.addEventListener("MozMousePixelScroll", stage.event, false);
	}

	stage.documentReadyListener();

	return stage;
}

var stage = Stage.call(Stage);

function addChild(child) {
	stage.addChild(child);
}