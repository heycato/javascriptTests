function App() {

	var tooltip = new Tooltip();
		navbar = new Sprite(),
		fullscreen = new Sprite(),
		caption = new Sprite(),
		email = new Sprite(),
		thumbs = new Sprite();

		navbar.addEventListener(MOUSE_OVER, hover);

	navbar.id = 'navbar';
	navbar.width = 270;
	navbar.height = 40;
	navbar.x = 500;
	navbar.y = 500;
	navbar.backgroundColor = 'rgba(255, 127, 0, 0.1)';

	fullscreen.id = 'fullscreen';
	fullscreen.x = 10;
	fullscreen.y = 10;
	fullscreen.text = 'fullscreen';
	fullscreen.fontFamily = 'Helvetica';
	fullscreen.tooltip = 'click to enter fullscreen viewing';

	caption.id = 'caption';
	caption.x = 90;
	caption.y = 10;
	caption.text = 'caption';
	caption.fontFamily = 'Helvetica';
	caption.tooltip = 'click to see image caption';

	email.id = 'email';
	email.x = 150;
	email.y = 10;
	email.text = 'email';
	email.fontFamily = 'Helvetica';
	email.tooltip = 'click to send email';

	thumbs.id = 'thumbs';
	thumbs.x = 200;
	thumbs.y = 10;
	thumbs.text = 'thumbs';
	thumbs.fontFamily = 'Helvetica';
	thumbs.tooltip = 'click to view thumbnail image gallery';

	navbar.addChild(thumbs);
	navbar.addChild(email);
	navbar.addChild(caption);
	navbar.addChild(fullscreen);
	stage.addChild(navbar);

	function hover(e) {
		if(e.target.tooltip) {
			tooltip.show(e);
		}
	}

} // end App function

