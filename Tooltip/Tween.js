function Tween(target, duration, vars) {

	var delay = vars.delay || 0;

	function init(){
		setTransition(duration);
		var i; for(i in vars) {
			if(!/delay|ease|css|onComplete|onCompleteScope|onCompleteParams|onUpdate|onUpdateScope|onUpdateParams/.test(i)) {
				setStyle(i, vars[i]);
			}
		}
	}

	function complete() {
		setTransition(0);
		if(vars.onComplete) vars.onComplete.apply(vars.onCompleteScope || target, vars.onCompleteParams || [false]);
	}

	function setTransition(value) {
		if(target.element) {
			target.transition = value;
		} else if(target.style) {
			value = value === 0 ? '' : 'all ' + value + 's';
			target.style.WebkitTransition = value;
			target.style.MozTransition = value;
			target.style.OTransition = value;
			target.style.transition = value;
		}
	}

	function setStyle(style, value) {
		if(target.element) {
			target[style] = value;
		} else if(target.style) {
			target.style[style] = value;
		}
	}

	setTimeout(init, delay * 1000);
	setTimeout(complete, (duration + delay) * 1000);
}

/*	firefox appears to have a threading issue with tweens when back to back
	var isFirefox = navigator.userAgent.match(/Firefox/i) != null,
	threadDelay = isFirefox ? 0 : 0;*/
