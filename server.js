var http = require('http');
var createHandler = require('github-webhook-handler');
var handler = createHandler({ path: '/', secret: 'myhashsecret' });
var exec = require('child_process').exec;
var sys = require('sys');

http.createServer(function (req, res) {
  handler(req, res, function (err) {
    res.statusCode = 404;
    res.end('no such location');
  })
}).listen(7777);
console.log('server created, listening on port 7777');

handler.on('error', function (err) {
  console.err('ERROR EVENT:', err.message);
});

handler.on('push', function (event) {
  console.log('PUSH EVENT: %s to %s',
    event.payload.repository.name,
    event.payload.ref);
    exec('git pull', pull);
});

handler.on('issues', function (event) {
  var e = event.payload;
  console.log('ISSUE EVENT: % action=%s: #%d %s', e.repository.name, e.action, e.issue.number, e.issue.title);
});

function pull(error, stdout, stderr) { sys.puts(stdout) }