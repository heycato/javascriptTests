var Hub = {
	_constructors: {
		Detail: function() {
			var _records = [];

			function _register(name, func, single) {
				if(!single) {
					single = false;
				}
				_records.push({
					name: name,
					func: func,
					single: single
				});
			}

			function _make(moduleName) {
				var i = _records.length;
				Hub[moduleName] = new Hub._constructors.Module();
				Hub[moduleName]._name = moduleName;
				while(i --) {
					_records[i].parent = moduleName;
					Hub[moduleName]._write(_records[i]);
				}
			}

			return {
				get records() { return _records },
				register: _register,
				make: _make
			}
		},
		Module: function() {
			var me = this;
			me._registry = {};
			me._junk = [];

			me._write = function(entry) {
				var e = entry;
				e.inst = undefined;
				if(me._isRegistered(e.name)) {
					throw "'" + e.name + "' already registered";
				}
				me[e.name] = function(vars) {
					return me._resolve(e.name, vars);
				};
				me._registry[e.name] = e;
			};

			me._remove = function(name) {
				var r = me._registry[name];
				if(r.single && r.inst) {
					me._junk.push(r.inst);
					delete r.inst;
				} else {
					throw "'" + name + "' not removed";
				}
			};

			me._resolve = function(name, vars) {
				var r = me._registry[name];

				if(!me._isRegistered(name)) {
					throw "'" + name + "' not registered";
				}
				if(r.single) {
					if(!r.inst) {
						r.inst = me._instantiate(r, vars);
					}
					return r.inst;
				}
				return me._instantiate(r, vars);
			};

			me._instantiate = function(e, vars) {
				return e.func(this, vars);
			};

			me._isRegistered = function(name) {
				return me._registry[name] !== undefined;
			};
		}
	}
};