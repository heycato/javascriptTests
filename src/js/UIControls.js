function UIControls(skin) {
	var skin = skin || {},
		bgColorFalse = skin.bgColorFalse || 'rgb(221,221,221)',
		bgColorTrue = skin.bgColorTrue || 'rgb(0,127,255)',
		color = skin.color || 'rgb(255,255,255)';

	this.UIToggleSwitch = function() {
		var graphics = new Graphics(),
			ui = graphics.draw('svg'),
			uiBg = graphics.draw('rect'),
			uiSwitch = graphics.draw('rect');

		ui.value = false;
		ui.colorStateTrue = bgColorTrue;
		ui.colorStateFalse = bgColorFalse;

		uiBg.width = 38;
		uiBg.height = 20;
		uiBg.x = 0;
		uiBg.y = 0;
		uiBg.fill = ui.value ? bgColorTrue : bgColorFalse;

		uiSwitch.width = 16;
		uiSwitch.height = 16;
		uiSwitch.x = ui.value ? (uiBg.width - uiSwitch.width - 2) : 2;
		uiSwitch.y = 2;
		uiSwitch.fill = color;

		ui.width = uiBg.width;
		ui.height = uiBg.height;
		ui.x = 0;
		ui.y = 0;
		ui.uiBg = uiBg;
		ui.uiSwitch = uiSwitch;

		stage.addEventListener(CLICK, toggle);

		ui.update = function() {
			uiBg.fill = ui.value ? bgColorTrue : bgColorFalse;
			uiSwitch.x = ui.value ? (uiBg.width - uiSwitch.width - 2) : 2;
		};

		function toggle(e) {
			if(ui.hitTestPoint(e.mouseX, e.mouseY)) {
				if(!ui.value) {
					Tween.to(ui.uiBg, 0.1, {color:ui.colorStateTrue});
					Tween.to(ui.uiSwitch, 0.1, {x:ui.uiBg.width - ui.uiSwitch.width - 2});
					ui.value = true;
				} else if(ui.value) {
					Tween.to(ui.uiBg, 0.1, {color:ui.colorStateFalse});
					Tween.to(ui.uiSwitch, 0.1, {x:2});
					ui.value = false;
				}
			}
		}

		ui.addChild(uiBg);
		ui.addChild(uiSwitch);

		return makeProxy(ui);
	};

	function makeProxy(svg) {
		var proxy = {
			element: svg.element,
			listeners: Event.listeners,
			dispatchEvent: Event.dispatchEvent,
			addEventListener: Event.addEventListener,
			removeEventListener: Event.removeEventListener,
			event: function(e) {
				if(e.target.width && !proxy.width) {
					proxy.width = e.target.width;
				}
				if(e.target.height && !proxy.height) {
					proxy.height = e.target.height;
				}
				var event = {target: proxy, pageX:e.pageX, pageY:e.pageY, clientX:e.clientX, clientY:e.clientY};
				proxy.dispatchEvent({type:e.type, target:proxy, data:event});
			},
			get size() { return svg.uiSize },
			set size(value) {
				svg.uiSize = value;
				svg.update();
			},
			get color() { return svg.uiColor },
			set color(value) {
				svg.uiColor = value;
				svg.update();
			},
			get x() { return svg.x },
			set x(value) {
				svg.x = value;
			},
			get y() { return svg.y },
			set y(value) {
				svg.y = value;
			},
			get value() { return svg.value },
			set value(value) {
				svg.value = value;
				svg.update();
			}
		};

		return proxy;
	}

}
