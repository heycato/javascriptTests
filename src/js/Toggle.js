function Toggle(vars) {

	this.state = vars.state || false;
	this.target = vars.target;
	this.onCb = vars.on;
	this.offCb = vars.off;

	this.on = function() {
		this.onCb.call(this.target);
		this.state = true;
	};

	this.off = function() {
		this.offCb.call(this.target);
		this.state = false;
	};

	this.flip = function() {
		if(this.state) {
			this.off();
		} else {
			this.on();
		}
	};
}