function MakeProxy(imports) {

	var _proxy = {
		children: [],
		style: imports.element.style,
		addChild: function(child) {
			imports.element.appendChild(child);
			_proxy.children.push(child);
		}
	};

	for(var key in _proxy) {
		imports.element[key] = _proxy[key];
	}

	return imports.element;

}