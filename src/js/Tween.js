var Tweens = {

	arr: [],
	active: false,
	i: 0,

	register: function(tween) {
		this.arr.push(tween);
	},

	unRegister: function(tween) {
		var i = this.arr.length;
		while(i--) {
			if(tween === this.arr[i]) {
				this.arr.splice(i, 1);
			}
		}
		if(this.arr.length === 0) {
			this.active = false;
		}
	},

	render: function() {
		if(this.arr.length > 0 && !this.active) {
			this.active = true;
			this.fire();
		}
	},

	fire: function() {

		var tweenFrame;

		if(this.i < this.arr.length) {
			tweenFrame = this.arr[this.i];
			tweenFrame.render();
			this.i += 1;
			this.fire();
		}

		if(this.i > this.arr.length) {
			this.i = this.arr.length;
		}

		if(this.i === this.arr.length) {
			this.i = 0;
			this.nextFrame(this.fire);
		}
	},

	nextFrame: function(callback) {
		window.setTimeout(function() {
			callback.call(Tweens);
		}, 1000 / 60);
	}
};

function Tween(target, duration, vars) {

	var params = /delay|ease|css|onComplete|onCompleteScope|onCompleteParams|onUpdate|onUpdateScope|onUpdateParams/;

	var tween = this;
	target.tween = tween;
	tween.target = target;
	tween.vars = vars;
	tween.delay = vars.delay || 0;
	tween.start = 0;
	tween.duration = duration * 1000;
	tween.percent = 0;
	tween.progress = 0;

	this.render = function() {
		frame();
		if(tween.vars.onUpdate) {
			tween.vars.onUpdate.apply(tween.vars.onUpdateScope || tween, tween.vars.onUpdateParams || [false]);
		}
		return true;
	};

	function frame() {

		tween.percent = getPercent(Date.now(), tween.start, tween.delay, tween.duration);
		tween.progress = getProgress();

		var i;
		for(i in tween.props) {
			var prop = tween.props[i].value,
				orig = tween.props[i].origin,
				value = '';

			if(/^x|y|left|top|right|bottom|width|height|borderRadius$/.test(i)) {
				value = Math.round(position(orig, prop, tween.progress));
			} else if(i.search(/color/ig) > -1) {
				value = positionRGB(orig, prop, tween.progress);
			} else {
				value = position(orig, prop, tween.progress);
			}

			target[i] = value;
		}

		if(tween.percent >= 1) {
			Tweens.unRegister(tween);
			delete target.tween;
			if(tween.vars.onComplete) {
				complete();
			}
		}
	}

	function getPercent(now, start, delay, duration) {
		var percent = ((now - (start + (delay * 1000))) / duration);
		if(percent < 0) {
			return 0;
		} else if(percent > 1 || !isFinite(percent)) {
			return 1;
		} else {
			return percent;
		}
	}

	function position(orig, prop, progress) {
		return orig + ((prop - orig) * progress);
	}

	function positionRGB(orig, prop, prog) {
		var p1 = orig[0] + ((prop[0] - orig[0]) * prog),
			p2 = orig[1] + ((prop[1] - orig[1]) * prog),
			p3 = orig[2] + ((prop[2] - orig[2]) * prog),
			p4 = orig[3] + ((prop[3] - orig[3]) * prog),
			pfx = p4 ? 'rgba(' : 'rgb(',
			sfx = p4 ? ',' + Math.round(p4*100)/100 + ')' : ')';
		return pfx + Math.round(p1) + ',' + Math.round(p2) + ',' + Math.round(p3) + sfx;
	}

	function getProgress() {
		switch(tween.vars.ease) {
			case 'linear':
				return linear(tween.percent);
			case 'in':
				return easeIn(tween.percent, 3);
			case 'out':
				return easeOut(tween.percent, 8);
			case 'inOut':
				if(typeof tween.vars.ease === 'object') {
					return easeInOut(tween.percent, tween.vars.ease[0], tween.vars.ease[1]);
				} else {
					return easeInOut(tween.percent, 2, 3);
				}
			default:
				return easeOut(tween.percent, 2);
		}
	}

	function getOrigin(name) {
		if(/opacity/.test(name)) {
			return Number(target[name] || 1);
		} else if(/color/ig.test(name)) {
			return parseColor(target[name]);
		} else {
			return target[name];
		}
	}

	this.buildProps = function(vars) {
		var props = tween.props || {},
			i;
		for(i in vars) {
			if(vars.hasOwnProperty(i) && !params.test(i)) {
				if (i.search(/color/ig) > -1) {
					props[i] = {value:parseColor(vars[i]), origin:getOrigin(i)};
				} else {
					props[i] = {value:vars[i], origin:getOrigin(i)};
				}
			}
		}
		return props;
	};

	this.updateProps = function(props) {
		var i;
		for(i in props) {
			if (i.search(/color/ig) > -1) {
				props[i] = {value:props[i].value, origin:getOrigin(i)};
			} else {
				props[i] = {value:props[i].value, origin:getOrigin(i)};
			}
		}
		return props;
	};

	function parseColor(c) {
		c = c || '#000000';
		if (c.charAt(0) === "#") {
			c = parseInt(c.substr(1), 16);
			return [(c >> 16) & 255, (c >> 8) & 255, c & 255];
		} else if(c.charAt(0) === "r") {
			c = c.replace(/[rgba()]/ig,'').split(',');
			return [Number(c[0]), Number(c[1]), Number(c[2]), Number(c[3])];
		}
	}

	function cssToJs(str) {
		return str.replace(/(-[a-z])/g, function($1) {
			return $1.replace('-','').toUpperCase();
		});
	}

	function jsToCss(str) {
		return str.replace(/([A-Z])/g, function($1) {
			return $1.replace($1,'-'+$1.toLowerCase());
		});
	}

	function complete() {
		tween.vars.onComplete.apply(tween.vars.onCompleteScope || target, tween.vars.onCompleteParams || [false]);
	}

	function linear(p) {
		return p;
	}

	function easeIn(p, pow) {
		return Math.pow(p, pow);
	}

	function easeOut(p, pow) {
		return 1 - Math.pow(1 - p, pow);
	}

	function easeInOut(p, pow1, pow2) {
		return (p *= 2) < 1 ? 0.5 * Math.pow(p, pow1) : 0.5 * (2 - Math.pow(2 - p, pow2));
	}

	tween.props = this.buildProps(tween.vars);
	tween.start = Date.now();
	Tweens.register(this);
	Tweens.render();

	return tween;
}

Tween.to = function(target, duration, vars) {
	if(target.tween) {
		var tween = target.tween;
		tween.vars = vars;
		tween.start = Date.now();
		tween.duration = (duration * 1000);
		tween.props = tween.buildProps(vars);
		tween.props = tween.updateProps(tween.props);
		tween.delay = vars.delay || 0;
		Tweens.render();
	} else {
		new Tween(target, duration, vars);
	}
};