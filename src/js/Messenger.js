function Messenger() {
	return {
		messages: {},
		listen: function(route, callback, target) {
			var target = target || this;
			if(!this.messages[route]) this.messages[route] = [];
			this.messages[route].push({route:route, callback:callback, target:target});
		},
		ignore: function(route, callback, target) {
			var target = target || this;
			if(!this.messages[route]) {
				throw 'ignore: route not registered';
			}
			if(!callback || typeof callback !== 'function') {
				delete this.messages[route];
			} else {
				var i = 0,
					l = this.messages[route].length;
				for(;i < l; i ++) {
					if(this.messages[route][i].callback === callback && this.messages[route][i].target === target) {
						this.messages[route].splice(i, 1);
					}what
				}
			}
		},
		notify: function(route, payload) {
			if(!this.messages[route]) throw 'notify: ' + route + ' not registered';
			for(var i = 0, l = this.messages[route].length; i < l; i ++) {
				this.messages[route][i].callback.apply(this.messages[route].target, [payload]);
			}
		}
	}
}