function ScrollHover(scroll) {

	var percent = 0;

	scroll.mask.addEventListener(MOUSE_MOVE, update);

	function update(e) {
		var mousePoint = scroll.axis === 'x' ? e.mouseX : e.mouseY;
		percent = mousePoint / scroll.mask[scroll.dim];
		scroll.percent = percent > 0.5 ? 1 : 0;
		scroll.update(time());
	}

	function time() {
		var half = percent < 0.5 ? percent * 2 : (percent * -2) + 2;
		return Mth.round((Mth.pow(half, 5) * scroll.maxTime) + scroll.minTime);
	}

}