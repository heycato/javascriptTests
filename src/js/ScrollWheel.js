function ScrollWheel(scroll) {

	var active = false;

	stage.addEventListener(MOUSE_WHEEL, onMouseWheel);
	stage.addEventListener(MOUSE_MOVE, onMouseMove);

	function onMouseMove(e) {
		active = scroll.mask.hitTestPoint(e.clientX, e.clientY);
	}

	function onMouseWheel(e) {
		if(active) {
			update(e);
		}
	}

	function update(e) {
		var delta = scroll.axis === 'x' ? e.deltaX : e.deltaY;
			delta = scroll.align === 'bottom' ? delta = -delta : delta;

		scroll.percent += (delta * 0.0001);
		scroll.percent = scroll.percent < 0 ? 0 : scroll.percent;
		scroll.percent = scroll.percent > 1 ? 1 : scroll.percent;

		scroll.update(0);
	}

}