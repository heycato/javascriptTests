function Icons(vars) {
	vars = vars || {};
	var graphics = vars.Graphics() || new Graphics(),
		Event = vars.Event() || Event;
	this.video = {
		fullscreen: function(vars) {
			var fullscreen = graphics.draw('svg'),
				pline = graphics.draw('path'),
				pline2 = graphics.draw('path'),
				group = graphics.draw('g');

			fullscreen.iconColor = '#000000';
			fullscreen.x = 0;
			fullscreen.y = 0;
			fullscreen.degrees = 0;
			fullscreen.id = vars.id || 'fullscreen';
			fullscreen.width = 12;
			fullscreen.height = 9;
			fullscreen.left = fullscreen.x;
			fullscreen.top = fullscreen.y;

			pline.d = 'M 0,-9.9999999e-8 0,2.9999999 l 1,0 0,-2 2,0 L 3,-9.9999999e-8 l -3,0 z m 9,0 L 9,0.9999999 l 2,0 0,2 1,0 0,-2.999999999999999 -3,0 z m -9,6 L 0,9 3,9 3,8 1,8 1,5.9999999 l -1,0 z m 11,0 L 11,8 9,8 l 0,1 3,0 0,-3.0000001 -1,0 z';
			pline2.d = 'M 2,1.9999999 2,7 l 8,0 0,-5.0000001 -8,0 z m 1,1 6,0 0,3 -6,0 0,-3 z';
			pline.fill = '#FFFFFF';
			pline2.fill = '#FFFFFF';

			group.addChild(pline);
			group.addChild(pline2);

			fullscreen.update = function() {
				var scale = fullscreen.iconSize / fullscreen.origSize;
				group.transform = 'rotate(' + fullscreen.degrees + ',' + fullscreen.iconSize * 0.5 + ',' + fullscreen.iconSize * 0.5 + ') scale(' + scale + ')';
				pline.fill = fullscreen.iconColor;
				pline2.fill = fullscreen.iconColor;
				fullscreen.left = fullscreen.x;
				fullscreen.top = fullscreen.y;
			};

			fullscreen.addChild(group);
			fullscreen.update();

			var proxy = makeProxy(fullscreen);
			applyListeners(group.element, proxy);
			return proxy;
		},
		play: function(vars) {
			var play = graphics.draw('svg'),
				pline = graphics.draw('path'),
				group = graphics.draw('g');

			play.iconColor = '#FFFFFF';
			play.x = 0;
			play.y = 0;
			play.id = vars.id || 'play';
			play.width = 8;
			play.height = 10;
			play.left = play.x;
			play.top = play.y;

			pline.d = 'M0,0l8,5.031L0,10V0z';
			pline.fill = '#FFFFFF';

			group.addChild(pline);

			play.update = function() {
				pline.fill = play.iconColor;
				play.left = play.x;
				play.top = play.y;
			};

			play.addChild(group);
			play.update();

			var proxy = makeProxy(play);
			applyListeners(group.element, proxy);
			return proxy;
		},
		pause: function(vars) {
			var pause = graphics.draw('svg'),
				pline = graphics.draw('path'),
				group = graphics.draw('g');

			pause.iconColor = '#FFFFFF';
			pause.x = 0;
			pause.y = 0;
			pause.id = vars.id || 'pause';
			pause.width = 8;
			pause.height = 10;
			pause.left = pause.x;
			pause.top = pause.y;

			pline.d = 'M 0,1 H 3 V 9 H 0 V 1 z M 5,1 H 8 V 9 H 5 V 1 z';
			pline.fill = '#FFFFFF';

			group.addChild(pline);

			pause.update = function() {
				pline.fill = pause.iconColor;
				pause.left = pause.x;
				pause.top = pause.y;
			};

			pause.addChild(group);
			pause.update();

			var proxy = makeProxy(pause);
			applyListeners(group.element, proxy);
			return proxy;
		},
		volume: function(vars) {
			var volume = graphics.draw('svg'),
				pline = graphics.draw('path'),
				group = graphics.draw('g');

			volume.iconColor = '#FFFFFF';
			volume.x = 0;
			volume.y = 0;
			volume.id = vars.id || 'pause';
			volume.width = 20;
			volume.height = 16;
			volume.left = volume.x;
			volume.top = volume.y;

			pline.d = 'M0,12.445h1.818V16H0V12.445z M3.637,12.445h1.817V16H3.637V12.445z M7.272,10.667H9.09V16H7.272V10.667z M10.909,8.889 h1.817V16h-1.817V8.889z M14.546,5.333h1.817V16h-1.817V5.333z M18.182,0H20v16h-1.818V0z';
			pline.fill = '#FFFFFF';

			group.addChild(pline);

			volume.update = function() {
				pline.fill = volume.iconColor;
				volume.left = volume.x;
				volume.top = volume.y;
			};

			volume.addChild(group);
			volume.update();

			var proxy = makeProxy(volume);
			applyListeners(group.element, proxy);
			return proxy;
		},
		mute: function(vars) {
			var mute = graphics.draw('svg'),
				pline = graphics.draw('path'),
				group = graphics.draw('g');

			mute.iconColor = '#FFFFFF';
			mute.x = 0;
			mute.y = 0;
			mute.id = vars.id || 'mute';
			mute.width = 20;
			mute.height = 16;
			mute.left = mute.x;
			mute.top = mute.y;

			pline.d = 'M0,0h1.818v3H0V0z M3.637,0h1.817v3H3.637V0z M7.272,0H9.09v3H7.272V0z M10.909,0h1.817v3h-1.817V0z M14.545,0h1.818v3 h-1.818V0z M18.182,0H20v3h-1.818V0z';
			pline.fill = '#FFFFFF';

			group.addChild(pline);

			mute.update = function() {
				pline.fill = mute.iconColor;
				mute.left = mute.x;
				mute.top = mute.y;
			};

			mute.addChild(group);
			mute.update();

			var proxy = makeProxy(mute);
			applyListeners(group.element, proxy);
			return proxy;
		}
	};

	this.thumbnail = function(vars) {

		var thumbnail = graphics.draw('svg'),
			rect1 = graphics.draw('rect'),
			rect2 = graphics.draw('rect'),
			rect3 = graphics.draw('rect'),
			rect4 = graphics.draw('rect'),
			group = graphics.draw('g');

		attachVars(thumbnail, [15,9]);

		thumbnail.id = vars.id || 'thumbIcon';

		rect1.width = 7;
		rect1.height = 4;
		rect1.x = 0;
		rect1.y = 0;

		rect2.width = 7;
		rect2.height = 4;
		rect2.x = 8;
		rect2.y = 0;

		rect3.width = 7;
		rect3.height = 4;
		rect3.x = 0;
		rect3.y = 5;

		rect4.width = 7;
		rect4.height = 4;
		rect4.x = 8;
		rect4.y = 5;

		group.addChild(rect1);
		group.addChild(rect2);
		group.addChild(rect3);
		group.addChild(rect4);

		thumbnail.update = function() {
			rect1.fill = thumbnail.iconColor;
			rect2.fill = thumbnail.iconColor;
			rect3.fill = thumbnail.iconColor;
			rect4.fill = thumbnail.iconColor;
			thumbnail.width = thumbnail.iconSize[0];
			thumbnail.height = thumbnail.iconSize[1];
			thumbnail.left = thumbnail.x;
			thumbnail.top = thumbnail.y;
		};

		thumbnail.addChild(group);
		thumbnail.update();

		var proxy = makeProxy(thumbnail);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.info = function(vars) {
		var infoIcon = graphics.draw('svg'),
			path1 = graphics.draw('path'),
			path2 = graphics.draw('path'),
			circ = graphics.draw('circle'),
			group = graphics.draw('g');

		attachVars(infoIcon);

		infoIcon.id = vars.id || 'infoIcon';

		path1.d = 'm 7.4999998,0.66437815 c 3.7656382,0 6.8356222,3.06998385 6.8356222,6.83562205 0,3.7656388 -3.069984,6.8356218 -6.8356222,6.8356218 -3.7656382,0 -6.83562175,-3.069983 -6.83562175,-6.8356218 0,-3.7656382 3.06998355,-6.83562205 6.83562175,-6.83562205 z';
		path1.strokeWidth = 1.32875609;
		path1.fill = 'none';

		path2.d = 'm 6.372937,8.74605 c -1.0685237,1.47509 -1.8241409,2.84471 -0.6234246,3.38146 1.3420742,0.526286 4.8197016,-2.282474 3.8044246,-2.00246 -0.67,0.241 -1.341,0.647 -1.981,0.554 -0.501,-0.073 2.859,-3.844 1.208,-4.818 C 7.4827777,4.82194 3.6260169,8.28379 4.6687316,7.85361 4.8855705,7.75077 8.1054908,6.26323 6.372937,8.74605 z';

		circ.r = 1.133;
		circ.cx = 8.8199997;
		circ.cy = 3.947;

		group.addChild(path1);
		group.addChild(path2);
		group.addChild(circ);

		infoIcon.update = function() {
			var scale = infoIcon.iconSize[0] / infoIcon.origSize[0];
			path1.stroke = infoIcon.iconColor;
			path2.fill = infoIcon.iconColor;
			circ.fill = infoIcon.iconColor;
			group.transform = 'rotate(' + infoIcon.degrees + ',' + infoIcon.iconSize[0] * 0.5 + ',' + infoIcon.iconSize[1] * 0.5 + ') scale(' + scale + ')';
			infoIcon.width = infoIcon.iconSize[0];
			infoIcon.height = infoIcon.iconSize[1];
			infoIcon.left = infoIcon.x;
			infoIcon.top = infoIcon.y;
		};

		infoIcon.addChild(group);
		infoIcon.update();

		var proxy = makeProxy(infoIcon);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.navArrowLeft = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [10,14]);

		arrow.id = vars.id || 'navArrowLeft';

		pline.d = 'm 6,10 -4.537,-4.708 4.537,-4.708';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.navArrowRight = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [10,14]);

		arrow.id = vars.id || 'navArrowRight';

		pline.d = 'm 1,10 4.536,-4.536 -4.536,-4.6';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.navArrowUp = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [14,10]);

		arrow.id = vars.id || 'navArrowUp';

		pline.d = 'm 1.4562498,8.21905 5.1000001,-5.4 5.7000001,5.4';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.navArrowDown = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [14,10]);

		arrow.id = vars.id || 'navArrowDown';

		pline.d = 'm 1.4562498,1.44405 5.1000001,5.4 5.7000001,-5.4';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.arrowLeft = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [10,15]);

		arrow.id = vars.id || 'arrowLeft';

		pline.d = 'M 8.812269,1.1126749 2.1862736,7.2790289 8.876226,13.512325';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.arrowRight = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [10,15]);

		arrow.id = vars.id || 'arrowRight';

		pline.d = 'M 1.0939806,1.1126749 7.719976,7.2790289 1.0300236,13.512325';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.arrowUp = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [15,10]);

		arrow.id = vars.id || 'arrowUp';

		pline.d = 'M 1.1126747,8.812269 7.2790287,2.1862738 13.512325,8.876226';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.arrowDown = function(vars) {
		var arrow = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(arrow, [15,10]);

		arrow.id = vars.id || 'arrowDown';

		pline.d = 'M 1.1126747,1.0939808 7.2790287,7.719976 13.512325,1.0300238';
		pline.strokeWidth = 3;
		pline.fill = 'none';

		group.addChild(pline);

		arrow.update = function() {
			pline.stroke = arrow.iconColor;
			arrow.left = arrow.x;
			arrow.top = arrow.y;
			arrow.width = arrow.iconSize[0];
			arrow.height = arrow.iconSize[1];
		};

		arrow.addChild(group);
		arrow.update();

		var proxy = makeProxy(arrow);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.plus = function(vars) {
		var operand = graphics.draw('svg'),
			path1 = graphics.draw('path'),
			path2 = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(operand, [14,14]);

		operand.id = vars.id || 'plus';

		path1.d = 'M 6.5591462,0.00335385 6.5033537,13.059146';
		path1.strokeWidth = vars.thickness || 3;
		path1.fill = 'none';

		path2.d = 'M 0.0033543,6.5033535 13.059146,6.5591465';
		path2.strokeWidth = vars.thickness || 3;
		path2.fill = 'none';

		group.addChild(path1);
		group.addChild(path2);

		operand.update = function() {
			path1.stroke = operand.iconColor;
			path2.stroke = operand.iconColor;
			operand.left = operand.x;
			operand.top = operand.y;
			operand.width = operand.iconSize[0];
			operand.height = operand.iconSize[1];
		};

		operand.addChild(group);
		operand.update();

		var proxy = makeProxy(operand);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.minus = function(vars) {
		var operand = graphics.draw('svg'),
			path1 = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(operand, [14,14]);

		operand.id = vars.id || 'minus';

		path1.d = 'M 0.0033543,6.5033535 13.059146,6.5591465';
		path1.strokeWidth = vars.thickness || 3;
		path1.fill = 'none';

		group.addChild(path1);

		operand.update = function() {
			path1.stroke = operand.iconColor;
			operand.left = operand.x;
			operand.top = operand.y;
			operand.width = operand.iconSize[0];
			operand.height = operand.iconSize[1];
		};

		operand.addChild(group);
		operand.update();

		var proxy = makeProxy(operand);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.operand = function(vars) {
		var operand = graphics.draw('svg'),
			path1 = graphics.draw('path'),
			path2 = null,
			group = graphics.draw('g');

		attachVars(operand);

		operand.id = vars.id || 'plus';

		path1.d = 'm 0.07131516,7.5193495 14.99702684,0';
		path1.strokeWidth = vars.thickness || 5;
		path1.fill = 'none';

		if(vars.type === 'plus') {
			path2 = graphics.draw('path');

			path2.d = 'm 7.5698286,0.01921217 0,15.00027383';
			path2.strokeWidth = vars.thickness || 5;
			path2.fill = 'none';

			group.addChild(path2);
		}

		group.addChild(path1);

		operand.update = function() {
			var scale = operand.iconSize[0] / operand.origSize[0];
			path1.stroke = operand.iconColor;
			if(vars.type === 'plus') {
				path2.stroke = operand.iconColor;
			}
			group.transform = 'rotate(' + operand.degrees + ',' + operand.iconSize[0] * 0.5 + ',' + operand.iconSize[1] * 0.5 + ') scale(' + scale + ')';
			operand.left = operand.x;
			operand.top = operand.y;
			operand.width = operand.iconSize[0];
			operand.height = operand.iconSize[1];
		};

		operand.addChild(group);
		operand.update();

		var proxy = makeProxy(operand);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.email = function(vars) {
		var email = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(email, [15,9]);

		email.id = vars.id || 'emailIcon';

		if(vars.type === 'pixel') {
			/*pixel-looking email icon*/
			pline.d = 'M 15,9 V 0 h -0.93 v 1 h -0.945 v 1 h -0.93 V 3 H 11.264 V 4 H 10.312 V 5 H 9.375 V 5.992 H 8.438 V 6.993 H 6.562 V 5.991 H 5.625 V 5 H 4.688 V 4 H 3.742 V 3 H 2.812 V 2 H 1.875 V 1 H 0.93 V 0 H 0 V 9 H 15 z M 0.93,6.993 h 0.945 v -1 H 2.813 V 5 h 0.93 v 0.992 h -0.93 v 1 H 1.875 V 8 H 0.93 V 6.993 z M 2.812,1 v 1 h 0.93 V 3 H 4.687 V 4 H 5.625 V 5 H 6.563 V 5.992 H 8.438 V 5 H 9.376 V 4 h 0.938 V 3 h 0.952 V 2 h 0.93 V 1 h 0.93 V 0 H 1.875 v 1 h 0.937 z m 8.453,4 h 0.93 v 0.992 h 0.93 v 1 H 14.07 V 8 H 13.125 V 6.993 h -0.93 v -1 h -0.93 V 5 z';
		} else {
			/*vector-looking email icon*/
			pline.d = 'M 10.7,3.65 15,0.1 V 9 H 0 V 0 L 4.65,3.45 0.85,7.6 1.3,8.05 5.2,3.85 7.8,5.9 10.15,4.1 13.65,7.952 14.15,7.502 10.7,3.65 z M 1.45,0 h 12.3 L 7.8,4.8 1.45,0 z';
		}
		pline.fill = '#000000';

		group.addChild(pline);

		email.update = function() {
			pline.fill = email.iconColor;
			email.left = email.x;
			email.top = email.y;
			email.width = email.iconSize[0];
			email.height = email.iconSize[1];
		};

		email.addChild(group);
		email.update();

		var proxy = makeProxy(email);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.fullscreen = function(vars) {
		var fullscreen = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(fullscreen, [15,9]);

		fullscreen.id = vars.id || 'fullscreen';

		pline.d = 'M 0,0 H 15 V 9 H 0 v -9 z m 2,2 v 5 h 11 v -5 H 2 z';
		pline.fill = '#000000';

		group.addChild(pline);

		fullscreen.update = function() {
			pline.fill = fullscreen.iconColor;
			fullscreen.left = fullscreen.x;
			fullscreen.top = fullscreen.y;
			fullscreen.width = fullscreen.iconSize[0];
			fullscreen.height = fullscreen.iconSize[1];
		};

		fullscreen.addChild(group);
		fullscreen.update();

		var proxy = makeProxy(fullscreen);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.cart = function(vars) {
		var cart = graphics.draw('svg'),
			pline = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(cart, [14,10]);

		cart.id = vars.id || 'cart';

		pline.d = 'M 1.248,8.985 H 3.254 V 6.989 H 1.248 V 8.985 z M 0,0 1.05,5.992 h 9.235 L 11.271,0.966 13.248,0.968 13.256,0 H 0 z m 3.25,3.981 h -1 v -1 h 1 v 1 z M 3.25,2 h -1 V 1 h 1 v 1 z m 2,2 h -1 V 3 h 1 v 1 z m 0,-2 h -1 V 1 h 1 v 1 z m 2,1.979 h -1 v -1 h 1 v 1 z M 7.25,2 h -1 V 1 h 1 v 1 z m 2,1.979 h -1 v -1 h 1 v 1 z M 9.25,2 h -1 V 1 h 1 V 2 z M 7.252,9.007 H 9.25 V 7.002 H 7.252 v 2.005 z';
		pline.fill = '#000000';
		pline.fillRule = 'evenodd';

		group.addChild(pline);

		cart.update = function() {
			pline.fill = cart.iconColor;
			cart.left = cart.x;
			cart.top = cart.y;
			cart.width = cart.iconSize[0];
			cart.height = cart.iconSize[1];
		};

		cart.addChild(group);
		cart.update();

		var proxy = makeProxy(cart);
		applyListeners(group.element, proxy);
		return proxy;
	};

	this.play = function(vars) {
		var play = graphics.draw('svg'),
			pline = graphics.draw('path'),
			pline2 = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(play);

		play.id = vars.id || 'play';

		pline.d = 'M 14.574848,7.5003379 C 14.614786,10.312119 12.786261,13.020154 10.174051,14.05261 7.608189,15.134064 4.4404037,14.499841 2.4967742,12.503226 0.50018873,10.559608 -0.13405639,7.3918425 0.94741176,4.8259513 1.9798358,2.2137621 4.6878719,0.38523961 7.4996761,0.42513792 10.31148,0.38513962 13.019517,2.2137621 14.051941,4.8259513 c 0.346511,0.8461641 0.523713,1.7602254 0.522907,2.6743866 z';
		pline.fill = '#000000';
		pline.fillOpacity = 0.8;
		pline.stroke = '#FFFFFF';
		pline.strokeWidth = 1;
		pline2.d = 'M 4.7095802,11.804205 C 7.3447057,10.346467 9.9798314,8.888779 12.614957,7.4310409 9.9798314,5.9845022 7.3447057,4.5380135 4.7095802,3.0914749 c 0,2.9042268 0,5.8085037 0,8.7127301 z';
		pline2.fill = '#FFFFFF';
		pline2.stroke = 'none';

		group.addChild(pline);
		group.addChild(pline2);

		play.update = function() {
			var scale = play.iconSize[0] / play.origSize[0];
			pline.stroke = play.iconColor;
			pline2.fill = play.iconColor;
			group.transform = 'rotate(' + play.degrees + ',' + play.iconSize[0] * 0.5 + ',' + play.iconSize[1] * 0.5 + ') scale(' + scale + ')';
			play.left = play.x;
			play.top = play.y;
			play.width = play.iconSize[0];
			play.height = play.iconSize[1];
		};

		play.addChild(group);
		play.update();

		var proxy = makeProxy(play);
		applyListeners(group.element, proxy);
		return proxy;
	};
	this.loader = function(vars) {
		var loader = graphics.draw('svg'),
			path1 = graphics.draw('path'),
			path2 = graphics.draw('path'),
			path3 = graphics.draw('path'),
			path4 = graphics.draw('path'),
			path5 = graphics.draw('path'),
			path6 = graphics.draw('path'),
			path7 = graphics.draw('path'),
			path8 = graphics.draw('path'),
			path9 = graphics.draw('path'),
			path10 = graphics.draw('path'),
			path11 = graphics.draw('path'),
			path12 = graphics.draw('path'),
			group = graphics.draw('g');

		attachVars(loader, [24,24]);

		loader.id = vars.id || 'loader';

		path1.d = 'm 11.978674,17.9727 0,6.0089';
		path1.fill = 'none';
		path1.strokeOpacity = 1;
		path1.strokeWidth = 2;
		path1.stroke = '#000000';

		path2.d = 'm 11.978674,0.0007 0,6.0089';
		path2.fill = 'none';
		path2.strokeOpacity = 0.91;
		path2.strokeWidth = 2;
		path2.stroke = '#000000';

		path3.d = 'm -0.01180219,11.9911 6.00884529,0';
		path3.fill = 'none';
		path3.strokeOpacity = 0.83;
		path3.strokeWidth = 2;
		path3.stroke = '#000000';

		path4.d = 'm 17.960208,11.9911 6.008942,10e-5';
		path4.fill = 'none';
		path4.strokeOpacity = 0.74;
		path4.strokeWidth = 2;
		path4.stroke = '#000000';

		path5.d = 'm 5.9834399,1.6071 3.0044451,5.2038';
		path5.fill = 'none';
		path5.strokeOpacity = 0.66;
		path5.strokeWidth = 2;
		path5.stroke = '#000000';

		path6.d = 'm 14.969458,17.1713 3.00445,5.2039';
		path6.fill = 'none';
		path6.strokeOpacity = 0.58;
		path6.strokeWidth = 2;
		path6.stroke = '#000000';

		path7.d = 'm 1.5946239,5.9959 5.2038258,3.0044';
		path7.fill = 'none';
		path7.strokeOpacity = 0.49;
		path7.strokeWidth = 2;
		path7.stroke = '#000000';

		path8.d = 'm 17.158844,14.9819 5.20388,3.0045';
		path8.fill = 'none';
		path8.strokeOpacity = 0.41;
		path8.strokeWidth = 2;
		path8.stroke = '#000000';

		path9.d = 'm 1.594592,17.9863 5.2038126,-3.0044';
		path9.fill = 'none';
		path9.strokeOpacity = 0.33;
		path9.strokeWidth = 2;
		path9.stroke = '#000000';

		path10.d = 'm 17.158809,9.0003 5.203947,-3.0043';
		path10.fill = 'none';
		path10.strokeOpacity = 0.24;
		path10.strokeWidth = 2;
		path10.stroke = '#000000';

		path11.d = 'm 5.9833702,22.3751 3.0044339,-5.2037';
		path11.fill = 'none';
		path11.strokeOpacity = 0.16;
		path11.strokeWidth = 2;
		path11.stroke = '#000000';

		path12.d = 'm 14.969378,6.8109 3.0046,-5.2037';
		path12.fill = 'none';
		path12.strokeOpacity = 0.08;
		path12.strokeWidth = 2;
		path12.stroke = '#000000';

		group.addChild(path1);
		group.addChild(path2);
		group.addChild(path3);
		group.addChild(path4);
		group.addChild(path5);
		group.addChild(path6);
		group.addChild(path7);
		group.addChild(path8);
		group.addChild(path9);
		group.addChild(path10);
		group.addChild(path11);
		group.addChild(path12);

		loader.update = function() {
			var scale = loader.iconSize[0] / loader.origSize[0];
			path1.stroke = loader.iconColor;
			path2.stroke = loader.iconColor;
			path3.stroke = loader.iconColor;
			path4.stroke = loader.iconColor;
			path5.stroke = loader.iconColor;
			path6.stroke = loader.iconColor;
			path7.stroke = loader.iconColor;
			path8.stroke = loader.iconColor;
			path9.stroke = loader.iconColor;
			path10.stroke = loader.iconColor;
			path11.stroke = loader.iconColor;
			path12.stroke = loader.iconColor;
			group.transform = 'rotate(' + loader.degrees + ',' + loader.iconSize[0] * 0.5 + ',' + loader.iconSize[1] * 0.5 + ') scale(' + scale + ')';
			loader.left = loader.x;
			loader.top = loader.y;
			loader.width = loader.iconSize[0];
			loader.height = loader.iconSize[1];
		};

		loader.addChild(group);
		loader.update();

		var proxy = makeProxy(loader);
		applyListeners(group.element, proxy);
		return proxy;
	};

	function applyListeners(obj, proxy) {
		obj.addEventListener(TOUCH_START, proxy.event, false);
		obj.addEventListener(TOUCH_MOVE, proxy.event, false);
		obj.addEventListener(TOUCH_END, proxy.event, false);
		obj.addEventListener(TOUCH_CANCEL, proxy.event, false);
		obj.addEventListener(MOUSE_OVER, proxy.event, false);
		obj.addEventListener(MOUSE_DOWN, proxy.event, false);
		obj.addEventListener(MOUSE_MOVE, proxy.event, false);
		obj.addEventListener(MOUSE_UP, proxy.event, false);
		obj.addEventListener(MOUSE_OUT, proxy.event, false);
		obj.addEventListener(CLICK, proxy.event, false);
		obj.addEventListener(LOAD, proxy.event, false);
		obj.addEventListener(ERROR, proxy.event, false);
	}

	function attachVars(svg, size) {
		if(!size) {
			var size = [15,15];
		}
		svg.iconColor = '#000000';
		svg.iconSize = size;
		svg.origSize = size;
		svg.x = 0;
		svg.y = 0;
		svg.degrees = 0;
		svg.width = size[0];
		svg.height = size[1];
		svg.left = svg.x;
		svg.top = svg.y;
	}

	function makeProxy(svg) {
		var _elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
			proxy = {
			element: svg.element,
			listeners: Event.listeners,
			dispatchEvent: Event.dispatchEvent,
			addEventListener: function(type, callback, vars) {
				var l = _elementEvents.length;
				while(l--) {
					if(type === _elementEvents[l]) {
						svg.element.addEventListener(type, this.event, false);
					}
				}
				Event.addEventListener(type, callback, vars);
			},
				removeEventListener: function(type, callback, vars) {
					var l = _elementEvents.length;
					while(l--) {
						if(type === _elementEvents[l]) {
							svg.element.removeEventListener(type, this.event, false);
						}
					}
					Event.removeEventListener(type, callback, vars);
				},
			event: function(e) {
				if(e.target.width && !proxy.width) {
					proxy.width = e.target.width;
				}
				if(e.target.height && !proxy.height) {
					proxy.height = e.target.height;
				}
				var event = {target: proxy, pageX:e.pageX, pageY:e.pageY, clientX:e.clientX, clientY:e.clientY};
				proxy.dispatchEvent({type:e.type, target:proxy, data:event});
			},
			get size() { return svg.iconSize },
			set size(value) {
				svg.iconSize = value;
				svg.update();
			},
			get width() { return svg.width },
			set width(value) {
				svg.width = value;
				svg.update();
			},
			get height() { return svg.height },
			set height(value) {
				svg.height = value;
				svg.update();
			},
			get color() { return svg.iconColor },
			set color(value) {
				svg.iconColor = value;
				svg.update();
			},
			get x() { return svg.x },
			set x(value) {
				svg.x = value;
				svg.update();
			},
			get y() { return svg.y },
			set y(value) {
				svg.y = value;
				svg.update();
			},
			get rotate() { return svg.degrees },
			set rotate(value) {
				svg.degrees = value;
				svg.update();
			}
		};

		return proxy;
	}

}
