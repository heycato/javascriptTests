var trace = console.log.bind ? console.log.bind(console) : function(){console.log(arguments);};

function getThumbQuery(width, height, file) {
    var x = RETINA ? 2 : 1;
    return Address.base + 'media/thumb/?w=' + (width * x) + '&h=' + (height * x) + '&f=' + file;
}

function findInArray(str, arr) {
    var i = arr.length;
    while(i--) {
        if(str == arr[i]) {
            return true;
        }
    }
    return false;
}

function parseColor(c) {
	var color = [];
	if (c && c.charAt(0) === "#") {
		c = parseInt(c.substr(1), 16);
		color = [(c >> 16) & 255, (c >> 8) & 255, c & 255];
	} else if(c && c.charAt(0) === "r") {
		c = c.replace(/[rgba()]/ig,'').split(',');
		color = [Number(c[0]), Number(c[1]), Number(c[2]), Number(c[3])];
	}
	return color;
}

function percentToPixels(value, range) {
    if(value.search('%') > -1) {
        value = value.replace('%', '');
        value = (Number(value) * 0.01) * range;
        return Math.round(value);
    } else if(value.search('px') > -1) {
        value = value.replace('px', '');
        return Number(value);
    } else {
        return Number(value);
    }
}

function getExt(file) {
    return file.split('.').pop();
}


function cssToJs(str) {
    return str.replace(/(-[a-z])/g, function($1) {
        return $1.replace('-','').toUpperCase();
    });
}

function jsToCss(str) {
    return str.replace(/([A-Z])/g, function($1) {
        return $1.replace($1,'-'+$1.toLowerCase());
    });
}

Math.range = function(minNum, maxNum, factor) {
	return Math.round(((Math.random() * (maxNum - minNum)) + minNum) * factor) / factor;
};