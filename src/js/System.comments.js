function System() {

	function _map(key, klass, single) {							// stores references to classes or objects mapped to the system

		if(_system.master) {									// checks if this system has a reference to main system
			if(single && _system.master.records[key]) {			// if klass is singleton and registered on main system, don't register it
				throw "'" + key + "' already registered";
			}
		}
		if(_system.records[key]) {								// if klass is already in this system, don't register it
			throw "'" + key + "' already registered";
		}
		if(typeof klass === 'function') {
			_system.records[key] = {							// records array holds all references to mapped classes
				name: key,										// don't need this here, will probably remove
				instance: undefined,							// prop holds instance of klass if singleton
				func: function(c, vars) {						// the function that is called when instantiating
					vars = vars || {};
					var l = _system.imports.length;
					while(l--) {								// loop through imports array and append all imports to vars object
						if(_system.imports[l].target === key || _system.imports[l].target === 'global') {
							vars[_system.imports[l].source] = _system.imports[l].func;
						}
					}
					return new klass(vars);						// returns new instance of klass with injections
				},
				single: single									// if(single) then klass is a singleton
			};
		} else if(typeof klass === 'object') {					// same as class but maps an object as singleton. func returns the instance;
			_system.records[key] = {
				name: key,
				object: klass,
				func: function(c) {
					return this.object;
				},
				single: true
			};
		}
	}

	function _resolve(key, vars) {								// first function run on instantiation attempt
		if(!_system.records[key]) {								// check if class is registered
			throw "'" + key + "' not registered";
		}
		if(_system.records[key].single) {						// check if class is a singleton
			if(!_system.records[key].instance) {				// if is singleton, but no instance has been created
				_system.records[key].instance = _instantiate(_system.records[key], vars);
			}													// create an instance and put it in the instance property of the record
			return _system.records[key].instance;				// and return the instance
		}
		return _instantiate(_system.records[key], vars);		// if not singleton, instantiate class as new
	}

	function _instantiate(record, vars) {						// runs func property of record
		return record.func(_system, vars);						// returns func (line 25)
	}

	var _system = {												// return object of system class

		_isSubsystem: false,									// is set true when sub system is mapped
		records: {},											// records of all mapped classes and objects
		subSystems: {},											// records of all mapped sub systems
		listeners: [],											// sub ordinate classes / view events array
		imports: [],											// records of all class injections
		call: _resolve,											// public reference to begin instantiation
		handlers: [],											// all system events array
		trash: [],												// messing around with caching trashed instances - don't need

		mapClass: function(key, klass) {						// public reference to _map function
			_map(key, klass, false);							// mapClass sets single param to false
		},

		mapSingleton: function(key, klass) {					// public reference to _map function
			_map(key, klass, true);								// mapSingleton set single param to true
		},

		mapObject: function(key, obj) {							// same as mapSingleton - probably don't need
			_map(key, obj, true);
		},

		mapImport: function(sourceKey, targetKey, importName) {	// public function maps imports
			var record,
				call,
				_targetKey = targetKey || 'global';				// 'global' target means all classes in system get sourceKey imported
			if(_system.records[sourceKey]) {					// if sourceKey is in this system
				record = _system.records[sourceKey];			// set record to reference sourceKey location
				call = _system.call;							// use this system's call function
			} else if(_system.master) {							// if import is in the master system
				if(_system.master.records[sourceKey]) {
					record = _system.master.records[sourceKey]; // set record to reference sourceKey location on master system
					call = _system.master.call;					// use master system's call function
				}
			} else {
				record = undefined;								// if not in this system or master system set record to undefined
			}
			if(!record) {
				throw "'" + sourceKey + "' has not been mapped";// no record found throw error.
			}
			if(!_system.records[_targetKey]) {					// if targetKey param not found in system
				if(_targetKey !== 'global') {					// and targetKey is not 'global;
					throw "'" + _targetKey + "' has not been mapped"; // throw error
				}
			}
			var importKey = importName || sourceKey;			// if no importName param specified, use sourceKey
			_system.imports.push({								// push to imports array
				source: importKey,
				target: _targetKey,
				func: function(vars) {							// import func is what is called on an injection ( ie vars.Sprite() )
					return call(sourceKey, vars);				// call is reference to _resolve which begins instantiation process
				}
			});
		},

		mapSystem: function(key, system) {
			if(_system.subSystems[key]) {
				throw "'" + key + "' already registered";
			}
			_system.subSystems[key] = system;
			_system.subSystems[key].handlers = _system.listeners;		//subsystem handlers points to master systems' listeners
			system.master = _system;
			system._isSubsystem = true;
		},

		removeInstance: function(key) {									// removes instance from storage in system
			if(_system.records[key].single && _system.records[key].instance) {
				_system.trash.push(_system.records[key].instance);		// store removed instances in trash array
				delete _system.records[key].instance;					// delete instance
				if(_system.trash.length > 10) {							// cache only 10 instances in trash
					_system.trash.splice(0 , 1);						// remove oldest instance
				}														// all trash stuffs not needed... just playing around with it
			} else {
				throw "'" + key + "' not removed";						// if no instance throw error
			}
		},

		unmap: function(key) {											// remove a record of a mapped class from system
			if(!_system.records[key]) {
				throw "'" + key + "' not registered";					// if can't find key, throw error
			}
			_system.trash.push(_system.records[key]);					// more trash stuff
			delete _system.records[ key ];								// delete record
		}

	return _system;

}