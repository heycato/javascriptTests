function System() {

	function _map(key, klass, single) {
		if(_system.records[key]) {
			throw "'" + key + "' already registered";
		}
		if(typeof klass === 'function') {
			_system.records[key] = {
				name: key,
				dependencies: {
					Event: {
						addEventListener: _system.addEventListener,
						removeEventListener: _system.removeEventListener,
						dispatchEvent: _system.dispatchEvent
					}
				},
				instance: undefined,
				func: function(c, vars) {
					return new klass(_system.records[key].dependencies, vars);
				},
				single: single	
			};
		} else if(typeof klass === 'object') {
			_system.records[key] = {
				name: key,
				object: klass,
				func: function(c) {
					this.object.addEventListener = _system.addEventListener;
					this.object.removeEventListener = _system.removeEventListener;
					this.object.dispatchEvent = _system.dispatchEvent;
					return this.object;
				},
				single: true
			};
		}
		_system[key] = function(vars) {
			return _resolve(key, vars);
		};
	}

	function _resolve(key, vars) {
		if(!_system.records[key]) {
			throw "'" + key + "' not registered";
		}
		if(_system.records[key].single) {
			if(!_system.records[key].inst) {
				_system.records[key].inst = _instantiate(_system.records[key], vars);
			}
			return _system.records[key].inst;
		}
		return _instantiate(_system.records[key], vars);
	}

	function _instantiate(record, vars) {
		return record.func(_system, vars);
	}

	var _system = {

		records: {},
		systemLinks: {},
		call: _resolve,
		listeners: [],
		trash: [],

		mapClass: function(key, klass) {
			_map(key, klass, false);
		},

		mapSingleton: function(key, klass) {
			_map(key, klass, true);
		},

		mapObject: function(key, obj) {
			_map(key, obj, true);
		},

		mapImport: function(sourceKey, targetKey, outletName) {
			if(!_system.records[sourceKey]) {
				throw "'" + sourceKey + "' has not been mapped";
			}
			if(!_system.records[targetKey]) {
				throw "'" + targetKey + "' has not been mapped";
			}
			var outletKey = outletName || sourceKey;
			_system.records[targetKey].dependencies[outletKey] = _system.records[sourceKey].func;
		},

		mapSystem: function(key, system) {
			if(!_system.systemlinks[key]) {
				throw "'" + key + "' not registered";
			}
			_system.systemLinks[key] = system;
			system.systemLinks.parent = _system;
		},

		remove: function(key) {
			if(_system.records[key].single && _system.records[key].inst) {
				_system.trash.push(_system.records[key].inst);
				delete _system.records[key].inst;
			} else {
				throw "'" + key + "' not removed";
			}
		},

		unmap: function(key) {
			if(!_system.records[key]) {
				throw "'" + key + "' not registered";
			}
			_system.trash.push(_system.records[key]);
			delete _system.records[ key ];
		},

		dispatch: function(event, registry) {
			var i = registry.length,
				target = event.target || _system;
			while(i --) {
				if(registry[i] && registry[i].type === event.type && (target === registry[i].vars.proxy || target === registry[i].vars.target)) {
					registry[i].callback.apply(registry[i].vars.target, [event.data || event]);
				}
			}
		},

		addEventListener: function(type, callback, vars) {
			vars = vars || {};
			vars.target = vars.target || _system;
			_system.listeners.push({type:type, callback:callback, vars:vars});
		},

		removeEventListener: function(type, callback, vars) {
			var i = _system.listeners.length,
				target = vars.target || _system;
			while(i --) {
				if(_system.listeners[i] && _system.listeners[i].type === type && _system.listeners[i].callback === callback && _system.listeners[i].vars.target === target) {
					_system.listeners.splice(i, 1);
				}
			}
		},

		dispatchEvent: function(event) {
			_system.dispatch(event, _system.listeners);
		},

		notifySystem: function(key, event) {
			_system.dispatch(event, _system.systemlinks[key].listeners);
		}
	};
	
	return _system;
}