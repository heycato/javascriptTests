function Element(imports) {
	imports = imports || {};
	var el = document.createElement(imports.type || 'div');
	el.style.position = imports.position || 'absolute';

	return new imports.MakeProxy({element:el});
}