function UICheckBox(vars) {

	vars = vars || {};

	var _elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
		_width = 16,
		_height = 16,
		_x = 0,
		_y = 0,
		_colorTrue = theme.bgColorSelected || 'rgb(0, 127, 255)',
		_colorFalse = theme.bgColorDarker || 'rgb(221, 221, 221)',
		_value = false,
		_groupId = vars.groupId || '',
		_color = _value ? _colorTrue: _colorFalse,
		_backgroundColor = _colorFalse;

	var ui = document.createElementNS("http://www.w3.org/2000/svg", vars.type || 'svg'),
		bg = document.createElementNS("http://www.w3.org/2000/svg", 'rect'),
		fg = document.createElementNS("http://www.w3.org/2000/svg", 'rect');

	bg.setAttribute('width', _width);
	bg.setAttribute('height', _height);
	bg.setAttribute('x', 0);
	bg.setAttribute('y', 0);
	bg.setAttribute('fill', _colorFalse);

	fg.setAttribute('width', 14);
	fg.setAttribute('height', 14);
	fg.setAttribute('x', 1);
	fg.setAttribute('y', 1);
	fg.setAttribute('fill', _color);

	ui.appendChild(bg);
	ui.appendChild(fg);

	ui.setAttribute('width', 16);
	ui.setAttribute('height', 16);
	update();

	function update() {
		if(vars.type === 'g') {
			ui.setAttribute('transform', 'translate(' + _x + ',' + _y + ')');
			ui.setAttribute('width', _width);
			ui.setAttribute('height', _height);
		} else {
			ui.setAttribute('style', 'position: absolute; left:' + _x + 'px; top:' + _y + 'px;');
			ui.setAttribute('width', _width + 'px');
			ui.setAttribute('height', _height + 'px');
		}
		bg.setAttribute('fill', _backgroundColor);
		fg.setAttribute('fill', _color);
	}

	function action(e) {
		if(!e.target.value) {
			e.target.value = true;
		} else {
			e.target.value = false;
		}
		if(e.target.groupId === '') {
			Tween.to(e.target, 0.1, {color: e.target.value ? _colorTrue : _colorFalse});
			Event.dispatchEvent({type:'change', payload: e.target});
		} else {
			Event.dispatchEvent({type: e.target.groupId, payload: e.target});
		}
	}

	var proxy = {
		element: ui,
		action: action,
		children: [],
		listeners: Event.listeners,
		dispatchEvent: Event.dispatchEvent,
		addEventListener:  function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.addEventListener(type, this.event, false);
				}
			}
			Event.addEventListener.apply(this, [type, callback, vars]);
		},
		removeEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.removeEventListener(type, this.event, false);
				}
			}
			Event.removeEventListener.apply(this, [type, callback, vars]);
		},
		addChild: function(child) {
			if(!this.element[child.element]) {
				this.element.appendChild(child.element);
			}
			child.parent = this;
			this[child.id] = child;
			this.children.push(child);
		},
		removeChild: function(child) {
			if(child.element.parentNode) {
				this.element.removeChild(child.element);
			}
			delete this[child.id];
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					child = undefined;
					this.children.splice(i, 1);
				}
			}
		},
		event: function(e) {
			if(e.target.width && !proxy.width) {
				proxy.width = e.target.width;
			}
			if(e.target.height && !proxy.height) {
				proxy.height = e.target.height;
			}
			var event = {
				type: e.type,
				target:e.target.parentNode.proxy,
				currentTarget:proxy,
				pageX:e.pageX,
				pageY:e.pageY,
				keyCode: e.keyCode,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - proxy.stageX,
				mouseY: e.clientY - proxy.stageY,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.touches) event.touches = e.touches;
			proxy.dispatchEvent({type:event.type, target:proxy, data:event});
		},
		get backgroundColor() { return _backgroundColor },
		set backgroundColor(value) {
			_backgroundColor = value;
			update();
		},
		get groupId() { return _groupId },
		set groupId(value) {
			_groupId = value;
		},
		get color() { return _color },
		set color(value) {
			_color = value;
			update();
		},
		get x() { return _x },
		set x(value) {
			_x = value;
			update();
		},
		get y() { return _y },
		set y(value) {
			_y = value;
			update();
		},
		get width() { return _width },
		set width(value) {
			_width = value;
			update();
		},
		get height() { return _height },
		set height(value) {
			_height = value;
			update();
		},
		get value() { return _value },
		set value(value) {
			_value = value;
		}
	};
	ui.proxy = proxy;
	proxy.style = ui.style;

	if(vars.type !== 'g') {
		proxy.addEventListener(CLICK, proxy.action, {target:proxy});
	}

	return proxy;
}
