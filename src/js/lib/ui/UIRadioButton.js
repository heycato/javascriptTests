function UIRadioButton(vars) {
	vars = vars || {};

	var Sprite = vars.Sprite || Sprite,
		radioButton = vars.UICheckBox || UICheckBox,
		Event = vars.Event() || Event,
		componentProxy = vars.UIComponentProxy || UIComponentProxy,
		ui = Sprite();

	ui.value = undefined;
	ui.spacer = 10;
	ui.totalButtons = 0;
	ui.buttons = [];
	ui.colorStateTrue = 'rgb(0, 127, 255)';
	ui.colorStateFalse = 'rgb(221, 221, 221)';
	ui.uiColor = 'rgb(255, 255, 255)';
	ui.backgroundColor = ui.colorStateFalse;

	ui.addButton = function(label) {
		var container = Sprite(),
			btn = radioButton(),
			txtLabel = Sprite();
		btn.removeEventListener(CLICK, btn.action, {target:btn});
		btn.addEventListener(CLICK, ui.action, {target:btn});
		btn.svg.colorStateTrue = ui.colorStateTrue;
		btn.svg.colorStateFalse = ui.colorStateFalse;
		btn.svg.update();
		btn.id = label;
		txtLabel.text = label || '';
		if(ui.totalButtons === 0) {
			btn.x = 0;
		} else {
			btn.x = ((btn.width + 60) * ui.totalButtons);
		}
		btn.y = 5;
		txtLabel.x = btn.x + btn.width + ui.spacer;
		txtLabel.y = 5;
		container.addChild(btn);
		container.addChild(txtLabel);
		ui.addChild(container);
		ui.totalButtons ++;
		ui.buttons.push(btn);
	};

	ui.action = function(e) {
		var l = ui.buttons.length;
		while(l--) {
			if(ui.buttons[l] === e.target) {
				e.target.action();
			} else {
				ui.buttons[l].value = false;
			}
		}
		console.log(e);
		ui.value = e.target.id + e.target.value;
		Event.notify({type:'change', value:e});
	};

	var proxy = componentProxy({svg:ui});
	proxy.addButton = ui.addButton;

	return proxy;
}
