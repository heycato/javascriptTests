function UIToggleSwitch(vars) {
	vars = vars || {};

	var _width = 38,
		_height = 20,
		_x = 0,
		_y = 0,
		_colorBgTrue = vars.theme.bgColorSelected || 'rgb(0, 127, 255)',
		_colorBgFalse = vars.theme.bgColorDarker || 'rgb(221, 221, 221)',
		_colorFg = vars.theme.bgColorLighter || 'rgb(255, 255, 255)',
		_value = false,
		_color = _value ? _colorBgTrue: _colorBgFalse,
		_fgX = _value ? (38 - 16 - 2) : 2,
		proxy = {},
		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
		ui = document.createElementNS("http://www.w3.org/2000/svg", vars.type || 'svg'),
		bg = document.createElementNS("http://www.w3.org/2000/svg", 'rect'),
		fg = document.createElementNS("http://www.w3.org/2000/svg", 'rect');

	bg.setAttribute('width', 38);
	bg.setAttribute('height', 20);
	bg.setAttribute('x', 0);
	bg.setAttribute('y', 0);

	fg.setAttribute('width', 16);
	fg.setAttribute('height', 16);
	fg.setAttribute('y', 2);
	fg.setAttribute('fill', _colorFg);

	function update() {
		ui.setAttribute('style', 'position: absolute; width:' + _width + 'px; height:' + _height + 'px; left:' + _x + 'px; top:' + _y + 'px;');
		bg.setAttribute('fill', _color);
		fg.setAttribute('x', _fgX);
	}

	function action(e) {
		if(!e.target.value) {
			Tween.to(e.target, 0.1, {color:_colorBgTrue});
			Tween.to(e.target, 0.1, {fgX:38 - 16 - 2});
			e.target.value = true;
		} else if(e.target.value) {
			Tween.to(e.target, 0.1, {color:_colorBgFalse});
			Tween.to(e.target, 0.1, {fgX:2});
			e.target.value = false;
		}
		Event.dispatchEvent({type:'change', payload: e.target});
	}

	ui.appendChild(bg);
	ui.appendChild(fg);

	proxy = {
		element: ui,
		action: action,
		listeners: Event.listeners,
		dispatchEvent: Event.dispatchEvent,
		addEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.addEventListener(type, this.event, false);
				}
			}
			Event.addEventListener(type, callback, vars);
		},
		removeEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.removeEventListener(type, this.event, false);
				}
			}
			Event.addEventListener(type, callback, vars);
		},
		event: function(e) {
			if(e.target.width && !proxy.width) {
				proxy.width = e.target.width;
			}
			if(e.target.height && !proxy.height) {
				proxy.height = e.target.height;
			}
			var event = {
				type: e.type,
				target:e.target.parentNode.proxy,
				currentTarget:proxy,
				pageX:e.pageX,
				pageY:e.pageY,
				keyCode: e.keyCode,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - proxy.stageX,
				mouseY: e.clientY - proxy.stageY,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.touches) event.touches = e.touches;
			proxy.dispatchEvent({type:event.type, target:proxy, data:event});
		},
		get value() {
			return _value;
		},
		set value(value) {
			_value = value;
			update();
		},
		get x() {
			return _x;
		},
		set x(value) {
			_x = value;
			update();
		},
		get y() {
			return _y;
		},
		set y(value) {
			_y = value;
			update();
		},
		get color() {
			return _color;
		},
		set color(value) {
			_color = value;
			update();
		},
		get fgX() {
			return _fgX;
		},
		set fgX(value) {
			_fgX = value;
			update();
		}
	};

	ui.proxy = proxy;
	proxy.style = ui.style;

	proxy.addEventListener(CLICK, proxy.action, {target:proxy});

	return proxy;
}
