function UINumberSelector(vars) {
	vars = vars || {};

	var graphics = new Graphics(),
		ui = new Sprite({selectable:true}),
		uiNumBox = new Sprite({type:'input', selectable:true}),
		uiLeftArw = graphics.draw('svg'),
		lArwPath = graphics.draw('path'),
		uiRightArw = graphics.draw('svg'),
		rArwPath = graphics.draw('path'),
		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
		_width = 90,
		_height = 20,
		_x = 0,
		_y = 0,
		_colorTrue = vars.theme.bgColorSelected || 'rgb(0, 127, 255)',
		_colorFalse = vars.theme.bgColorDarker || 'rgb(221, 221, 221)',
		_value = 0,
		_radio = false,
		_arwColor = vars.theme.fontColor || 'rgb(180, 180, 180)',
		_backgroundColor = _colorFalse;

	uiNumBox.width = 30;
	uiNumBox.height = 20;
	uiNumBox.x = 30;
	uiNumBox.y = 0;
	uiNumBox.element.value = _value;
	uiNumBox.backgroundColor = _colorFalse;
	uiNumBox.fontColor = _colorTrue;
	uiNumBox.textAlign = 'center';
	uiNumBox.fontSize = 10;
	uiNumBox.border = 'none';
	uiNumBox.selectable = true;

	lArwPath.d = 'm 6,10 -4.537,-4.708 4.537,-4.708';
	lArwPath.strokeWidth = 2;
	lArwPath.stroke = _arwColor;
	lArwPath.fill = 'none';

	uiLeftArw.width = 10;
	uiLeftArw.height = 14;
	uiLeftArw.x = 0;
	uiLeftArw.y = 4;
	uiLeftArw.addChild(lArwPath);

	rArwPath.d = 'm 1,10 4.536,-4.536 -4.536,-4.6';
	rArwPath.strokeWidth = 2;
	rArwPath.stroke = _arwColor;
	rArwPath.fill = 'none';

	uiRightArw.width = 10;
	uiRightArw.height = 14;
	uiRightArw.x = 82;
	uiRightArw.y = 4;
	uiRightArw.addChild(rArwPath);

	ui.width = 90;
	ui.height = 20;
	ui.x = 0;
	ui.y = 0;
	ui.uiNumBox = uiNumBox;
	ui.uiLeftArw = uiLeftArw;
	ui.uiRightArw = uiRightArw;

	uiNumBox.addEventListener(BLUR, updateValue, {target:uiNumBox});
	uiLeftArw.addEventListener(CLICK, uiDecrementValue, {target:uiLeftArw});
	uiRightArw.addEventListener(CLICK, uiIncrementValue, {target:uiRightArw});

	function updateValue() {
		_value = Number(uiNumBox.element.value);
		Event.dispatchEvent({type:'change', payload: proxy});
		update();
	}

	function uiIncrementValue() {
		_value ++;
		Event.dispatchEvent({type:'change', payload: proxy});
		update();
	}

	function uiDecrementValue() {
		_value --;
		update();
		Event.dispatchEvent({type:'change', payload: proxy});
	}

	function update() {
		ui.x = _x;
		ui.y = _y;
		uiNumBox.element.value = _value;
	}


	ui.addChild(uiLeftArw);
	ui.addChild(uiNumBox);
	ui.addChild(uiRightArw);

	var proxy = {
		element: ui.element,
		listeners: Event.listeners,
		dispatchEvent: Event.dispatchEvent,
		addEventListener:  function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.addEventListener(type, this.event, false);
				}
			}
			Event.addEventListener.apply(this, [type, callback, vars]);
		},
		removeEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.removeEventListener(type, this.event, false);
				}
			}
			Event.removeEventListener.apply(this, [type, callback, vars]);
		},
		event: function(e) {
			if(e.target.width && !proxy.width) {
				proxy.width = e.target.width;
			}
			if(e.target.height && !proxy.height) {
				proxy.height = e.target.height;
			}
			var event = {
				type: e.type,
				target:e.target.parentNode.proxy,
				currentTarget:proxy,
				pageX:e.pageX,
				pageY:e.pageY,
				keyCode: e.keyCode,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - proxy.stageX,
				mouseY: e.clientY - proxy.stageY,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.touches) event.touches = e.touches;
			proxy.dispatchEvent({type:event.type, target:proxy, data:event});
		},
		get backgroundColor() { return _backgroundColor },
		set backgroundColor(value) {
			_backgroundColor = value;
			update();
		},
		get color() { return _color },
		set color(value) {
			_color = value;
			update();
		},
		get x() { return _x },
		set x(value) {
			_x = value;
			update();
		},
		get y() { return _y },
		set y(value) {
			_y = value;
			update();
		},
		get value() { return _value },
		set value(value) {
			_value = value;
			update();
		}
	};
	ui.proxy = proxy;
	proxy.style = ui.style;

	return proxy;
}
