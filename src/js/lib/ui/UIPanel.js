function UIPanel(vars) {

	var sprite = vars.Sprite,
		stage = vars.Stage(),
		container = sprite();

	var _x = vars.x || 0,
		_y = vars.y ||0,
		_width = vars.width || 0,
		_height = vars.height || 0,
		_state = vars.state || 'open',
		_type = vars.type || 'panel',
		_title = vars.title || 'Title',
		_buttonText = _type === 'accordion' ? '▸' : '×';

	var self = {
		get x() {
			return _x;
		},
		set x(value) {
			container.x = value;
			_x = value;
		},
		get y() {
			return _y;
		},
		set y(value) {
			container.y = value;
			_y = value;
		},
		get width() {
			return _width;
		},
		set width(value) {
			container.width = value;
			_width = value;
		},
		get height() {
			return _height;
		},
		set height(value) {
			container.height = value;
			_height = value;
		},
		get type() {
			return _type;
		},
		set type(value) {
			_type = value;
		},
		get state() {
			return _state;
		},
		set state(value) {
			// do some stuff
			_state = value;
		},
		get title() {
			return _title;
		},
		set title(value) {
			self.label.text = value;
			_title = value;
		}
	};

	self.isOpen = false;

	function init() {
		self.element = container.element;
		self.hitTestPoint = function(x, y){
			return container.hitTestPoint(x, y);
		};
		self.hitTestAbs = function(x, y){
			return container.hitTestAbs(x, y);
		};
		/*container.backgroundColor = vars.theme.bgColor;*/
		container.x = _x;
		container.y = _y;
		container.width = vars.width;
		container.height = vars.height;
		self.open();

		buildTitleBar();

		// title bar
		// label
		// close button
		// collapse arrow
		// dragable
		// zindex management
		// content area
		// scrollbar?
		// resizeable panel?
		// docking behavior
	}

	self.open = function() {
		console.log('panel open');
	};

	self.close = function() {
		console.log('panel closed');
	};

	function buildTitleBar() {
		self.titleBar = sprite();
		self.titleBar.width = vars.width;
		self.titleBar.height = vars.theme.titleBarHeight;
		self.titleBar.backgroundColor = vars.theme.bgColorDarker;
		container.addChild(self.titleBar);
		buildLabel();
		buildTitleButton();
		buildContentArea();

		self.titleBar.addEventListener(MOUSE_DOWN, startDrag);
		self.titleBar.addEventListener(TOUCH_START, startDrag);
	}

	function buildLabel() {
		self.label = sprite();
		self.label.text = _title;
		self.label.fontColor = vars.theme.fontColor;
		self.label.fontFamily = vars.theme.fontFamily;
		self.label.fontSize = vars.theme.fontSize;
		self.label.height = vars.theme.fontSize;
		self.label.selectable = false;
		self.label.textTransform = vars.theme.titleTextTransform;
		self.label.x = 10;
		self.label.y = (self.titleBar.height - self.label.height) * 0.5;
		self.titleBar.addChild(self.label);
	}

	function buildTitleButton() {
		self.button = sprite();
		self.button.text = _buttonText;
		self.button.fontColor = vars.theme.fontColor;
		self.button.fontFamily = vars.theme.fontFamily;
		self.button.fontSize = vars.theme.fontSize;
		self.button.width = vars.theme.fontSize;
		self.button.height = vars.theme.fontSize;
		self.button.selectable = false;
		self.button.textTransform = vars.theme.titleTextTransform;
		self.button.x = vars.width - 7 - self.button.width;
		self.button.y = (self.titleBar.height - self.button.height) * 0.5 - 1;
		self.titleBar.addChild(self.button);

		self.button.addEventListener(MOUSE_OVER, buttonOver, {target:self.button});
		self.button.addEventListener(MOUSE_OUT, buttonOut, {target:self.button});
		self.button.addEventListener(CLICK, buttonAction, {target:self.button});
		self.button.addEventListener(TOUCH_END, buttonAction, {target:self.button});
	}

	function buildContentArea() {
		self.contentArea = sprite();
		self.contentArea.y = self.titleBar.height;
		self.contentArea.width = vars.width;
		self.contentArea.height =  vars.height;
		self.contentArea.backgroundColor = vars.theme.bgColor;
		container.addChild(self.contentArea);

		updateContainerSize();
	}

	function updateContainerSize(){
		container.width = self.contentArea.width;
		container.height = self.contentArea.y + self.contentArea.height;
		self.width = container.width;
		self.height = container.height;
	}

	function buttonOver(e) {
		Tween.to(this, vars.theme.transition, {fontColor:vars.theme.fontColorHover});
	}

	function buttonOut(e) {
		Tween.to(this, vars.theme.transition, {fontColor:vars.theme.fontColor});
	}

	function buttonAction(e) {
		if(self.type === 'accordion') {
			console.log('button action accordion');
		} else if(self.type === 'panel') {
			self.close();
		}
	}

	function startDrag(e) {
		stage.dragElement = container;
		stage.dragElement.startX = getX(e) - stage.dragElement.x;
		stage.dragElement.startY = getY(e) - stage.dragElement.y;
		stage.addEventListener(MOUSE_UP, stopDrag);
		stage.addEventListener(TOUCH_END, stopDrag);
		stage.addEventListener(MOUSE_MOVE, onMove);
		stage.addEventListener(TOUCH_MOVE, onMove);
	}

	function onMove(e) {
		stage.dragElement.x = (getX(e) - stage.dragElement.startX);
		stage.dragElement.y = (getY(e) - stage.dragElement.startY);
	}

	function stopDrag() {
		stage.removeEventListener(MOUSE_MOVE, onMove);
		stage.removeEventListener(TOUCH_MOVE, onMove);
		stage.removeEventListener(MOUSE_UP, stopDrag);
		stage.removeEventListener(TOUCH_END, stopDrag);
	}

	function getX(e) {
		if (touchDevice) {
			return e.touches[0].pageX;
		} else {
			return e.clientX;
		}
	}

	function getY(e) {
		if (touchDevice) {
			return e.touches[0].pageY;
		} else {
			return e.clientY;
		}
	}

	self.open = function() {
		if(self.type === 'accordion') {
			console.log('button open action accordion');
		} else if(self.type === 'panel') {
			container.display = 'block';
			Tween.to(container, vars.theme.transition, {alpha:1});
		}
	};

	self.close = function() {
		if(self.type === 'accordion') {
			console.log('button close action accordion');
		} else if(self.type === 'panel') {
			Tween.to(container, vars.theme.transition, {alpha:0, onComplete:function(){
				container.display = 'none';
			}});
		}
	};

	init.call(self);

	return self;
}



