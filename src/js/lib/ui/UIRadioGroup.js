function UIRadioGroup(vars) {

	vars = vars || {};

	var graphics = new Graphics(),
		_proxy = {},
 		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
		_backgroundColor = 'rgb(221, 221, 221)',
		_color = 'rgb(0, 127, 255)',
		_orientation = 'horizontal',
		_width,
		_height,
		_x = 0,
		_y = 0,
		_id = vars.id,
		_labelGap = 10,
		_gap = 20,
		_fontFamily = 'Helvetica',
		_fontSize = 12,
		_fontColor = '#555555',
		_value = false,
		_currentSelection,
		_offsetWidth = _gap + 16 + _gap + 50;

	var ui = graphics.draw('svg');

	function addButton(label) {

		var btn = new UICheckBox({type:'g', groupId: _id}),
			txt = graphics.draw('text');

		btn.id = label;
		btn.addEventListener(CLICK, btn.action, {target:btn});

		txt.text = label;
		txt.color = '#555555';
		txt.x = 16 + _labelGap;
		txt.fontSize = _fontSize;

		btn.txt = txt;

		txt.fontFamily = _fontFamily;

		txt.y = 12;

		btn.width = 16;
		btn.height = 16;
		ui.width = 400;
		ui.height = 400;

		btn.addChild(txt);
		ui.addChild(btn);

		_proxy.buttons.push(btn);
	}

	function stackOrientation() {
		switch(_orientation) {
			case 'horizontal':
				setBtnX();
				break;
			case 'vertical':
				setBtnY();
				break;
			default:
				throw 'UIRadioGroup orientation property must be horizontal or vertical';
				break;
		}
	}

	function setBtnX() {
		var len = _proxy.buttons.length,
			i = 0,
			prevOffsetWidth = 0,
			width,
			maxHeight = 0;
		for(i; i < len; i ++) {
			var btn = _proxy.buttons[i],
				txt = btn.txt;
			txt.bbox = txt.getBBox;
			btn.groupId = _id;
			txt.x = btn.width + _labelGap;
			width = btn.width + _labelGap + txt.bbox.width + _gap;
			btn.x = prevOffsetWidth;

			prevOffsetWidth += width;
			if(btn.height > maxHeight) {
				maxHeight = btn.height;
			}
		}
		_width = prevOffsetWidth;
		ui.width = _width;
		ui.height = maxHeight;
	}

	function setBtnY() {
		var len = _proxy.buttons.length,
			i = 0,
			prevOffset = 0,
			height,
			width,
			maxWidth = 0;
		for(i; i < len; i ++) {
			var btn = _proxy.buttons[i];
			txt = btn.txt;
			txt.bbox = txt.getBBox;
			btn.groupId = _id;
			txt.x = btn.width + _labelGap;
			height = btn.height + _gap;
			width = btn.width + _labelGap + txt.bbox.width + _gap;
			btn.y = prevOffset;

			prevOffset += height;
			if(width > maxWidth) {
				maxWidth = width;
			}
		}
		_height = prevOffset;
		ui.height = _height;
		ui.width = maxWidth;
	}

	function update() {
		var l = _proxy.buttons.length,
			i = 0;
		ui.x = _x;
		ui.y = _y;
		for(i; i<l; i++) {
			if(_proxy.buttons[i]) {
				var btn = _proxy.buttons[i],
					txt = btn.txt,
					width;
				btn.groupId = _id;
				txt.bbox = txt.getBBox;
				txt.fontSize = _fontSize;
				txt.fontFamily = _fontFamily;
				txt.color = _fontColor;
				width = txt.bbox.width;

				stackOrientation();
			}
		}
		colorChange(_value);
	}

	function checkState(e) {
		_currentSelection = e.payload.id;
		colorChange(_currentSelection);
		_proxy.value = _currentSelection;
		Event.dispatchEvent({type:'change', payload: _proxy});
	}

	function colorChange(value) {
		var i = 0,
			j = 0,
			l = _proxy.buttons.length;
		for(i; i < l; i++) {
			if(_proxy.buttons[i].id === value) {
				Tween.to(_proxy.buttons[i], 0.1, {color:'rgb(0, 127, 255)'});
			} else {
				Tween.to(_proxy.buttons[i], 0.1, {color:'rgb(221, 221, 221)'});
			}
		}
	}

	_proxy = {
		element: ui.element,
		buttons: [],
		addButton: addButton,
		update: update,
		children: [],
		listeners: Event.listeners,
		dispatchEvent: Event.dispatchEvent,
		addEventListener:  function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.addEventListener(type, this.event, false);
				}
			}
			Event.addEventListener(type, callback, vars);
		},
		removeEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					this.element.removeEventListener(type, this.event, false);
				}
			}
			Event.removeEventListener(type, callback, vars);
		},
		event: function(e) {
			var event = {target: _proxy, pageX:e.pageX, pageY:e.pageY, clientX:e.clientX, clientY:e.clientY};
			_proxy.dispatchEvent({type:e.type, target:_proxy, data:event});
		},
		addChild: function(child) {
			if(!this.element[child.element]) {
				this.element.appendChild(child.element);
			}
			child.parent = this;
			this[child.id] = child;
			this.children.push(child);
		},
		get id() { return _id },
		set id(value) {
			_id = value;
			update();
		},
		get x() { return _x },
		set x(value) {
			_x = value;
			update();
		},
		get fontFamily() { return _fontFamily },
		set fontFamily(value) {
			_fontFamily = value;
			update();
		},
		get fontSize() { return _fontSize },
		set fontSize(value) {
			_fontSize = value;
			update();
		},
		get fontColor() { return _fontColor },
		set fontColor(value) {
			_fontColor = value;
			update();
		},
		get y() { return _y },
		set y(value) {
			_y = value;
			update()
		},
		get gap() { return _gap },
		set gap(value) {
			_gap = value;
			update();
		},
		get labelGap() { return _labelGap },
		set labelGap(value) {
			_labelGap = value;
			update();
		},
		get value() { return _value },
		set value(value) {
			_value = value;
			update();
		},
		get orientation() { return _orientation },
		set orientation(value) {
			_orientation = value;
			update();
		}
	};

	_proxy.addEventListener(vars.id, checkState);
	stackOrientation();

	return _proxy;
}
