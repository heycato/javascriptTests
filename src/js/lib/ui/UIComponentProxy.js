function UIComponentProxy(vars) {
	vars = vars || {};

	var Event = vars.Event() || Event,
		svg = vars.svg,
		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
		proxy = {
			svg: svg,
			element: svg.element,
			action: function(e) {
				svg.action(e);
			},
			listeners: Event.listeners,
			dispatchEvent: Event.dispatchEvent,
			addEventListener: function(type, callback, vars) {
				var l = _elementEvents.length;
				while(l--) {
					if(type === _elementEvents[l]) {
						svg.element.addEventListener(type, this.event, false);
					}
				}
				Event.addEventListener(type, callback, vars);
			},
			removeEventListener: function(type, callback, vars) {
				var l = _elementEvents.length;
				while(l--) {
					if(type === _elementEvents[l]) {
						svg.element.removeEventListener(type, this.event, false);
					}
				}
				Event.removeEventListener(type, callback, vars);
			},
			event: function(e) {
				if(!_selectable) {
					e.preventDefault();
				}
				if(e.target.width && !proxy.width) {
					proxy.width = e.target.width;
				}
				if(e.target.height && !proxy.height) {
					proxy.height = e.target.height;
				}
				var event = {target: proxy, pageX:e.pageX, pageY:e.pageY, clientX:e.clientX, clientY:e.clientY};
				proxy.dispatchEvent({type:e.type, target:proxy, data:event});
			},
			get id() { return svg.id },
			set id(value) {
				svg.id = value;
			},
			get size() { return svg.uiSize },
			set size(value) {
				svg.uiSize = value;
				svg.update();
			},
			get width() { return svg.width },
			set width(value) {
				svg.width = value;
			},
			get height() { return svg.height },
			set height(value) {
				svg.height = value;
			},
			get color() { return svg.uiColor },
			set color(value) {
				svg.uiColor = value;
				svg.update();
			},
			get x() { return svg.x },
			set x(value) {
				svg.x = value;
			},
			get y() { return svg.y },
			set y(value) {
				svg.y = value;
			},
			get value() { return svg.value },
			set value(value) {
				svg.value = value;
				svg.update();
			}
		};

	return proxy;
}
