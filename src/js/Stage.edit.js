function Stage(vars) {

	vars = vars || {};

	var Event = vars.Event() || Event;

	var _selectable = false,
		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR];

	var stage = {
		dragElement: {},
		children: [],

		listeners: Event.listeners,
		dispatchEvent: Event.dispatchEvent,
		addEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					window.addEventListener(type, this.event, false);
				}
			}
			Event.addEventListener(type, callback, vars);
		},
		removeEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					window.removeEventListener(type, this.event, false);
				}
			}
			Event.removeEventListener(type, callback, vars);
		},

		addChild: function(child) {
			if(!child.element.parentNode) {
				document.body.appendChild(child.element);
			}
			child.parent = this;
			this[child.id] = child;
			this.children.push(child);
		},

		removeChild: function(child) {
			if(child.element.parentNode) {
				document.body.removeChild(child.element);
			}
			delete this[child.id];
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					this.children.splice(i, 1);
				}
			}
		},

		getDomElements: function() {
			var o = document.getElementsByTagName('*');
			var i = o.length;
			while(i--) {
				if(o[i].id) {
					var child = {};
					child.element = o[i];
					this.addChild(child);
				}
			}
		},

		domContentLoaded: function() {
			if(!_selectable) {
				document.body.style.overflow = 'hidden';
			}
//			this.getDomElements();
			this.dispatchEvent({type:'load', target:this});
		},

		documentReadyListener: function() {
			if(typeof document.onreadystatechange === 'object') {
				document.onreadystatechange = function() {
					if(document.readyState === 'complete') {
						stage.domContentLoaded();
					}
				};
			} else {
				window.onload = function() {
					stage.domContentLoaded();
				};
			}
		},

		globalToLocal: function (point) {
			return {x:point.x, y:point.y};
		},

		localToGlobal: function (point) {
			return {x:point.x, y:point.y};
		},


		event: function(e) {
			var event = {
				type: e.type,
				target:e.target.proxy,
				currentTarget:stage,
				x:e.x,
				y:e.y,
				pageX:e.pageX,
				pageY:e.pageY,
				screenX:e.screenX,
				screenY:e.screenY,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - stage.stageX,
				mouseY: e.clientY - stage.stageY,
				deltaX: -e.wheelDeltaX || -e.wheelDelta * 2 || e.deltaX || e.detail || 0,
				deltaY: -e.wheelDeltaY || -e.wheelDelta * 2 || e.deltaY || e.detail || 0,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.touches) event.touches = e.touches;

			if(e.type === "wheel" || e.type === "MozMousePixelScroll") {
				event.type = MOUSE_WHEEL;
			}

			stage.dispatchEvent({type:event.type, target:stage, data:event});
		},

		get getChildren() {
			return children;
		},
		get images() {
			return document.images;
		},
		get forms() {
			return document.forms;
		},
		get links() {
			return document.links;
		},
		get anchors() {
			return document.anchors;
		},
		get scripts() {
			return document.scripts;
		},
		get plugins() {
			return document.plugins;
		},
		get embeds() {
			return document.embeds;
		},
		get width() {
			return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		},
		get height() {
			return window.innerHeight || document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
		},
		get stageX() {
			return 0;
		},
		get stageY() {
			return 0;
		},
		get selectable() {
			return _selectable;
		},
		set selectable(value) {
			_selectable = value;
		}
	};

	stage.proxy = stage;

	if("onmousewheel" in document) {
		window.addEventListener(MOUSE_WHEEL, stage.event, false);
	} else if("onwheel" in document) {
		window.addEventListener("wheel", stage.event, false);
	} else if("MozMousePixelScroll" in document) {
		window.addEventListener("MozMousePixelScroll", stage.event, false);
	}

	stage.documentReadyListener();

	return stage;
}

function addChild(child) {
	stage.addChild(child);
}
