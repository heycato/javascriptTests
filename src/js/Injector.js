function Injector() {

	var _registry = {},
		_imports = [];

	function _buildInjections(target, args) {
		var injections = args || {};
		var l = _imports.length;
		while(l--) {
			if(_imports[l].target === target || _imports[l].target === 'global') {
				injections[_.imports[l].source] = _.imports[l].func;
			}
		}
		return injections;
	}

	function _resolve(key, args) {
		if(!_registry[key]) {
			throw "'" + key + "' not registered";
		}
		if(_registry[key].single) {
			if(!_registry[key].instance) {
				_registry[key].instance = _instantiate(_registry[key], args);
			}
			return _registry[key].instance;
		}
		return _instantiate(_registry[key], args);
	}

	function _instantiate(record, args) {
		return record.func(this, args);
	}

	function _register(name, func, single) {
		if(!this.registry[name]) {
			this.registry[name] = {
				func: func,
				instance: undefined,
				single: single,
				execute: function(c, args) {
					return func(_buildInjections(name, args));
				}
			}
		}
	}

	function _inject(sourceKey, targetKey, importName) {
		var record,
			call,
			_targetKey = targetKey || 'global';
		if(_registry[sourceKey]) {
			record = _registry[sourceKey];
			call = _resolve;
		} else {
			record = undefined;
		}
		if(!record) {
			throw "'" + sourceKey + "' has not been mapped";
		}
		if(!_registry[_targetKey]) {
			if(_targetKey !== 'global') {
				throw "'" + _targetKey + "' has not been mapped";
			}
		}
		var importKey = importName || sourceKey;
		this.imports.push({
			source: importKey,
			target: _targetKey,
			func: function(vars) {
				return _resolve(sourceKey, vars);
			}
		});
	}

	return {
		registry: _registry,
		imports: _imports,
		call: _resolve,
		register: _register,
		inject: _inject
	}
}