var Scrolls = {

	arr: [],

	register: function(scroll) {
		this.arr.push(scroll);
	},

	unRegister: function(scroll) {
		var i = this.arr.length;
		while(i--) {
			if(scroll === this.arr[i]) {
				this.arr.splice(i, 1);
			}
		}
	},

	resize: function() {
		if(this.arr.length > 0) {
			var i = this.arr.length;
			while(i--) {
				this.arr[i].resize();
			}
		}
	}
};

function Scroll(obj, mask, vars) {

	var _percent = 0,
		_position = 0,
		_scrollDist = 0;

	var scroll = {
		obj: obj,
		mask: mask,
		types: vars.types || ['bar','wheel','touch'],
		axis: vars.axis || 'y',
		align: vars.align || 'center',
		margin: vars.margin || 0,
		maxTime: vars.maxTime || 100,
		minTime: vars.minTime || 1,
		color: vars.color || '#00FF00',
		hover: vars.hover || '#FF0000',
		alpha: vars.alpha || 0.35,
		side: vars.side || 'right',
		offsetX: vars.offsetX || 0,
		offsetY: vars.offsetY || 0,
		width: vars.width || 7,
		get percent(){
			return _percent;
		},
		set percent(value){
			value = value < 0 ? 0 : value;
			value = value > 1 ? 1 : value;
			_percent = value;
		},
		get position(){
			return _position;
		},
		set position(value){
			_position = value;
		},
		get scrollDist(){
			return _scrollDist;
		},
		set scrollDist(value){
			_scrollDist = value;
		}
	};

	scroll.perpAxis = vars.axis === 'x' ? 'y' : 'x';
	scroll.dim = vars.axis === 'x' ? 'width' : 'height';
	scroll.perpDim = vars.axis === 'x' ? 'height' : 'width';

	scroll.alignment = function() {
		switch(scroll.align) {
			case 'middle':
			case 'center':
				return 0.5;
			case 'right':
			case 'bottom':
				return 1;
		}
		return 0;
	};

	scroll.update = function(speed) {
		if(this.obj[this.dim] < mask[this.dim]) {
			_percent = scroll.alignment();
			speed = 0.35;
		}

		_scrollDist = (this.margin + this.obj[this.dim] + this.margin) - this.mask[this.dim];
		_position = -(_scrollDist *_percent) + this.margin;
		var tweenVars = {};
		tweenVars[vars.axis] = Mth.round(_position);
		Tween.to(obj, speed, tweenVars);

		if(scroll.bar) {
			scroll.bar.moveHandle();
		}
	};

	scroll.resize = function() {
		if(scroll.bar) {
			scroll.bar.resize();
		}
		scroll.update(0.35);
	};

	function buildBehaviors() {
		var i = vars.types.length;
		while(i--) {
			switch (vars.types[i]) {
				case 'bar':
					scroll.bar = new ScrollBar(scroll);
					break;
				case 'wheel':
					scroll.wheel = new ScrollWheel(scroll);
					break;
				case 'hover':
					scroll.hover = new ScrollHover(scroll);
					break;
				case 'touch':
					scroll.touch = new ScrollTouch(scroll);
					break;
			}
		}
	}

	buildBehaviors();
	scroll.resize();
	scroll.update();
	Scrolls.register(scroll);

	return scroll;
}