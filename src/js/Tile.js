function Tile(vars) {

	var	items = [],
		colCount = 0,
		currentX = 0,
		currentY = 0,
		maxWidth = 0,
		maxHeight = 0,
		columns = 0;

	this.axis = vars.axis || 'y';
	this.perpLength = vars.perpLength || 300;
	this.align = vars.align || 'left';
	this.wrap = vars.wrap || true;
	this.gap = vars.gap || 0;
	this.width = 0;
	this.height = 0;

	this.incrHorizontal = function(i) {
		var height = items[i].height,
			width = items[i].width;
		if(this.wrap) {
			if(currentY + height < this.perpLength - height - this.gap) {
				currentY += height + this.gap;
				colCount++;
			} else {
				currentY = 0;
				currentX += width + this.gap;
			}
		} else {
			currentX += width + this.gap;
		}
	};

	this.incrVertical = function(i) {
		var height = items[i].height,
			width = items[i].width;
		if(this.wrap) {
			if(currentX + width < this.perpLength - width - this.gap) {
				currentX += width + this.gap;
				colCount++;
			} else {
				currentX = 0;
				currentY += height + this.gap;
			}
		} else {
			currentY += height + this.gap;
		}
	};

	this.getItemsAbove = function(i) {

		var thumbsAbove = Math.floor(i / columns),
			height = 0,
			j = 0;

		for(j; j < thumbsAbove; j++) {
			var curThumbAbove = columns * (j + 1);
			if(this.axis === 'y') {
				height += items[i - curThumbAbove].height + this.gap;
			} else {
				height += items[i - curThumbAbove].width + this.gap;
			}
		}

		return height;
	};

	this.layoutItems = function() {
		var i = 0,
			length = items.length;
		currentX = 0;
		currentY = 0;
		maxWidth = 0;
		maxHeight = 0;
		colCount = 0;

		columns = this.getColumns();

		for(i; i < length; i++) {

			if(this.axis === 'y') {
				currentY = this.getItemsAbove(i);
			} else {
				currentX = this.getItemsAbove(i);
			}

			if(this.align === 'center' && !this.wrap) {
				items[i].x = (this.perpLength - items[i].width) * 0.5;
			} else if(this.align === 'right' && !this.wrap) {
				items[i].x = (this.perpLength - items[i].width);
			} else {
				items[i].x = currentX;
			}

			if(this.align === 'middle' && !this.wrap) {
				items[i].y = (this.perpLength - items[i].height) * 0.5;
			} else if(this.align === 'bottom' && !this.wrap) {
				items[i].y = (this.perpLength - items[i].height);
			} else {
				items[i].y = currentY;
			}

			if(currentX >= maxWidth) {
				maxWidth = currentX + items[i].width;
			}

			if(currentY >= maxHeight) {
				maxHeight = currentY + items[i].height;
			}

			if(this.axis === 'y' && colCount === 0) {
				maxWidth = items[i].width;
			} else if(this.axis === 'x' && colCount === 0) {
				maxHeight = items[i].height;
			}

			this.width = maxWidth;
			this.height = maxHeight;

			switch (this.axis) {
				case 'x':
					this.incrHorizontal(i);
					break;
				case 'y':
					this.incrVertical(i);
					break;
			}
		}
	};

	this.getColumns = function() {
		var dim = this.axis === 'y' ? 'width' : 'height';
		return Math.floor(((this.perpLength + this.gap) - 1) / (items[0][dim] + this.gap)) || 1;
	};

	this.addItem = function(width, height) {
		items.push({width:width, height:height, x:0, y:0});
	};

	this.setSize = function(index, width, height) {
		items[index].width = width;
		items[index].height = height;
	};

	this.getPosition = function(index) {
		return items[index];
	};

	this.getBounds = function() {
		return {width:this.width, height:this.height};
	};
}