var Event = {

	listeners: [],

	addEventListener: function(type, callback, vars) {
		vars = vars || {};
		vars.target = vars.target || this;
		this.listeners.push({type:type, callback:callback, vars:vars});
	},

	removeEventListener: function(type, callback, target) {
		var i = this.listeners.length,
			target = target || this;
		while(i --) {
			if(this.listeners[i] && this.listeners[i].type === type && this.listeners[i].callback === callback && this.listeners[i].vars.target === target) {
				this.listeners.splice(i, 1);
			}
		}
	},

	dispatchEvent: function(event) {
		var i = this.listeners.length;
		while(i --) {
			if(this.listeners[i] && this.listeners[i].type === event.type && (event.target === this.listeners[i].vars.proxy || event.target === this.listeners[i].vars.target)) {
				this.listeners[i].callback.apply(this.listeners[i].vars.target, [event.data || event]);
			}
		}
	}
};

var globalEvents = Event;