function Align(vars) {

	/* {hRange:0, vRange:0, width:0, height:0, hAlign:'left', vAlign:'top', hOffset:0, vOffset:0} */

	var i;

	for(i in vars) {
		if(vars.hasOwnProperty(i)) {
			this[i] = vars[i] || 0;
		}
	}

	this.calc = function() {

		switch(this.hAlign) {
			case 'left':
				this.x = this.hOffset;
				break;
			case 'center':
				this.x = ((this.hRange - this.width) * 0.5) + this.hOffset;
				break;
			case 'right':
				this.x = this.hRange - this.width - this.hOffset;
				break;
		}

		switch(this.vAlign) {
			case 'top':
				this.y = this.vOffset;
				break;
			case 'center':
				this.y = ((this.vRange - this.height) * 0.5) + this.vOffset;
				break;
			case 'bottom':
				this.y = (this.vRange - this.height) - this.vOffset;
				break;
		}
	};

	this.calc();
}