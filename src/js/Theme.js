function Theme() {

	var LightSkin = {

		bgColor:"#EEEEEE",
		bgColorLight:"#F9F9F9",
		bgColorLighter:"#FFFFFF",
		bgColorDark:"#E9E9E9",
		bgColorDarker:"#DDDDDD",
		bgColorSelected:"#0099FF",
		bgColorHover:"#0099FF",

		fontColor:"#373737",
		fontColorLight:"#444444",
		fontColorLighter:"#595959",
		fontColorDark:"#111111",
		fontColorDarker:"#000000",
		fontColorSelected:"#0099FF",
		fontColorHover:"#0099FF",

		fontFamily:"Lato-Regular",
		fontFamilyThin:"Lato-Hairline",
		fontFamilyLight:"Lato-Light",
		fontFamilyBold:"Lato-Bold",

		fontSize:12,
		fontSizeSmall:10,
		fontSizeLarge:14,
		fontSizeXLarge:24,

		transition: 0.35,
		transitionSlow:0.85,
		transitionFast:0.15,

		titleBarHeight:30,
		titleTextTransform:'uppercase',
		btnHeight:35,
		gap:1,
		shadow:"0px 4px 12px rgba(0,0,0,0.15)"

	};

	var DarkSkin = {};

	return LightSkin;
}


