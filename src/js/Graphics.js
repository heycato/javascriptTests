function Graphics(vars) {

	vars = vars || {};
	var gfx = this;

	this.children = [];

	this.draw = function(name) {
		var _elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR];

		var _id, _class, _transform, _style, _width, _height, _r, _rx, _ry, _x, _y, _x1, _y1, _x2, _y2, _cx, _cy,
			_fill, _fillOpacity, _fillRule, _strokeLineJoin, _strokeMiterLimit, _strokeOpacity, _strokeWidth, _stroke,
			_strokeDashArray, _strokeDashOffset, _strokeLineCap, _fontFamily, _fontSize, _fontSizeAdjust, _fontStretch,
			_fontStyle, _fontVariant, _fontWeight, _letterSpacing, _wordSpacing, _d, _points, _viewBox, _visibility,
			_position, _left, _top, _bgColor, _degrees, _text, _alpha, _display, _zIndex, _result, _in1, _in2, _mode,
			_dx, _dy, _stdDeviation, _type, _filter, _values,
			tmp = {};

		tmp[name] = document.createElementNS("http://www.w3.org/2000/svg", name);
		tmp[name].children = [];

		tmp[name].addChild = addChild;

		if(name === 'svg') {
			gfx.children.push(tmp[name]);
			tmp[name].style.position = 'absolute';
		}

		var proxy = {
			element: tmp[name],
			children: [],
			listeners: Event.listeners,
			dispatchEvent: Event.dispatchEvent,
			addEventListener:  function(type, callback, vars) {
				var l = _elementEvents.length;
				while(l--) {
					if(type === _elementEvents[l]) {
						tmp[name].addEventListener(type, this.event, false);
					}
				}
				Event.addEventListener(type, callback, vars);
			},
			removeEventListener: function(type, callback, vars) {
				var l = _elementEvents.length;
				while(l--) {
					if(type === _elementEvents[l]) {
						tmp[name].removeEventListener(type, this.event, false);
					}
				}
				Event.removeEventListener(type, callback, vars);
			},
			event: function(e) {
				if(e.target.width && !proxy.width) {
					proxy.width = e.target.width;
				}
				if(e.target.height && !proxy.height) {
					proxy.height = e.target.height;
				}
				var event = {target: proxy, pageX:e.pageX, pageY:e.pageY, clientX:e.clientX, clientY:e.clientY};
				proxy.dispatchEvent({type:e.type, target:proxy, data:event});
			},
			addChild: function(child) {
				if(!tmp[name][child.element]) {
					tmp[name].appendChild(child.element);
				}
				child.parent = this;
				this[child.id] = child;
				this.children.push(child);
			},
			hitTestPoint: function(xPoint, yPoint) {
				return xPoint >= this.stageX && xPoint <= this.stageX + _width && yPoint >= this.stageY && yPoint <= this.stageY + _height;
			},
			get id() { return _id },
			set id(value) {
				_id = value;
				tmp[name].setAttribute('id', value);
			},
			get collection() { return _class },
			set collection(value) {
				_class = value;
				tmp[name].setAttribute('class', value);
			},
			get transform() { return _transform },
			set transform(value) {
				_transform = value;
				tmp[name].setAttribute('transform', value);
			},
			get style() { return _style },
			set style(value) {
				_style = value;
				tmp[name].setAttribute('style', value);
			},
			get width() { return _width },
			set width(value) {
				_width = value;
				tmp[name].setAttribute('width', value);
			},
			get height() { return _height },
			set height(value) {
				_height = value;
				tmp[name].setAttribute('height', value);
			},
			get x() {
				return _x;
			},
			set x(value) {
				_x = value;
				tmp[name].style.left = value + 'px';
				tmp[name].setAttribute('x', value);
			},
			get y() { return _y },
			set y(value) {
				_y = value;
				tmp[name].style.top = value + 'px';
				tmp[name].setAttribute('y', value);
			},
			get x1() { return _x1 },
			set x1(value) {
				_x1 = value;
				tmp[name].setAttribute('x1', value);
			},
			get y1() { return _y1 },
			set y1(value) {
				_y1 = value;
				tmp[name].setAttribute('y1', value);
			},
			get x2() { return _x2 },
			set x2(value) {
				_x2 = value;
				tmp[name].setAttribute('x2', value);
			},
			get y2() { return _y2 },
			set y2(value) {
				_y2 = value;
				tmp[name].setAttribute('y2', value);
			},
			get r() { return _r },
			set r(value) {
				_r = value;
				tmp[name].setAttribute('r', value);
			},
			get rx() { return _rx },
			set rx(value) {
				_rx = value;
				tmp[name].setAttribute('rx', value);
			},
			get ry() { return _ry },
			set ry(value) {
				_ry = value;
				tmp[name].setAttribute('ry', value);
			},
			get cx() { return _cx },
			set cx(value) {
				_cx = value;
				tmp[name].setAttribute('cx', value);
			},
			get cy() { return _cy },
			set cy(value) {
				_cy = value;
				tmp[name].setAttribute('cy', value);
			},
			get color() { return _fill },
			set color(value) {
				_fill = value;
				tmp[name].setAttribute('fill', value);
			},
			get fill() { return _fill },
			set fill(value) {
				_fill = value;
				tmp[name].setAttribute('fill', value);
			},
			get fillOpacity() { return _fillOpacity },
			set fillOpacity(value) {
				_fillOpacity = value;
				tmp[name].setAttribute('fill-opacity', value);
			},
			get fillRule() { return _fillRule },
			set fillRule(value) {
				_fillRule = value;
				tmp[name].setAttribute('fill-rule', value);
			},
			get stroke() { return _stroke },
			set stroke(value) {
				_stroke = value;
				tmp[name].setAttribute('stroke', value);
			},
			get strokeOpacity() { return _strokeOpacity },
			set strokeOpacity(value) {
				_strokeOpacity = value;
				tmp[name].setAttribute('stroke-opacity', value);
			},
			get strokeWidth() { return _strokeWidth },
			set strokeWidth(value) {
				_strokeWidth = value;
				tmp[name].setAttribute('stroke-width', value);
			},
			get strokeDashArray() { return _strokeDashArray },
			set strokeDashArray(value) {
				_strokeDashArray = value;
				tmp[name].setAttribute('stroke-dasharray', value);
			},
			get strokeDashOffset() { return _strokeDashOffset },
			set strokeDashOffset(value) {
				_strokeDashOffset = value;
				tmp[name].setAttribute('stroke-dashoffset', value);
			},
			get strokeLineCap() { return _strokeLineCap },
			set strokeLineCap(value) {
				_strokeLineCap = value;
				tmp[name].setAttribute('stroke-linecap', value);
			},
			get strokeLineJoin() { return _strokeLineJoin },
			set strokeLineJoin(value) {
				_strokeLineJoin = value;
				tmp[name].setAttribute('stroke-linejoin', value);
			},
			get strokeMiterLimit() { return _strokeMiterLimit },
			set strokeMiterLimit(value) {
				_strokeMiterLimit = value;
				tmp[name].setAttribute('stroke-miterlimit', value);
			},
			get fontFamily() { return _fontFamily },
			set fontFamily(value) {
				_fontFamily = value;
				tmp[name].setAttribute('font-family', value);
			},
			get fontSize() { return _fontSize },
			set fontSize(value) {
				_fontSize = value;
				tmp[name].setAttribute('font-size', value);
			},
			get fontSizeAdjust() { return _fontSizeAdjust },
			set fontSizeAdjust(value) {
				_fontSizeAdjust = value;
				tmp[name].setAttribute('font-size-adjust', value);
			},
			get fontStretch() { return _fontStretch },
			set fontStretch(value) {
				_fontStretch = value;
				tmp[name].setAttribute('font-stretch', value);
			},
			get fontStyle() { return _fontStyle },
			set fontStyle(value) {
				_fontStyle = value;
				tmp[name].setAttribute('font-style', value);
			},
			get fontVariant() { return _fontVariant },
			set fontVariant(value) {
				_fontVariant = value;
				tmp[name].setAttribute('font-variant', value);
			},
			get fontWeight() { return _fontWeight },
			set fontWeight(value) {
				_fontWeight = value;
				tmp[name].setAttribute('font-weight', value);
			},
			get letterSpacing() { return _letterSpacing },
			set letterSpacing(value) {
				_letterSpacing = value;
				tmp[name].setAttribute('letter-spacing', value);
			},
			get wordSpacing() { return _wordSpacing },
			set wordSpacing(value) {
				_wordSpacing = value;
				tmp[name].setAttribute('word-spacing', value);
			},
			get d() { return _d },
			set d(value) {
				_d = value;
				tmp[name].setAttribute('d', value);
			},
			get points() { return _points },
			set points(value) {
				_points = value;
				tmp[name].setAttribute('points', value);
			},
			get viewBox() { return _viewBox },
			set viewBox(value) {
				_viewBox = value;
				tmp[name].setAttribute('viewBox', value);
			},
			get visibility() { return _visibility },
			set visibility(value) {
				_visibility = value;
				tmp[name].setAttribute('visibility', value);
			},
			get position() { return _position },
			set position(value) {
				_position = value || 'absolute';
				tmp[name].style.position = _position;
			},
			get left() { return _left },
			set left(value) {
				_left = value || 0;
				tmp[name].style.left = _left + 'px';
			},
			get top() { return _top },
			set top(value) {
				_top = value || 0;
				tmp[name].style.top = _top + 'px';
			},
			get backgroundColor() { return _bgColor },
			set backgroundColor(value) {
				_bgColor = value;
				tmp[name].style.backgroundColor = value;
			},
			get rotate() { return _degrees },
			set rotate(value) {
				_degrees = value;
				tmp[name].setAttribute('transform', 'rotate(' + value + ')');
			},
			get text() { return _text },
			set text(value) {
				_text = value;
				tmp[name].textContent = value;
			},
			get alpha() { return _alpha },
			set alpha(value) {
				_alpha = value;
				tmp[name].style.opacity = value;
			},
			get opacity() { return _alpha },
			set opacity(value) {
				_alpha = value;
				tmp[name].style.opacity = value;
			},
			get display() { return _display },
			set display(value) {
				_display = value;
				tmp[name].style.display = value;
			},
			get zIndex() { return _zIndex },
			set zIndex(value) {
				_zIndex = value;
				tmp[name].style.zIndex = value;
			},
			get stageX() {
				if(this.parent) {
					return this.parent.stageX + (this.x || this.left);
				}
				return (this.x || this.left);
			},
			get stageY() {
				if(this.parent) {
					return this.parent.stageY + (this.y || this.top);
				}
				return (this.y || this.top);
			},
			get getBBox() { return tmp[name].getBBox(); },
			// defs, filters, offset, blend, gaussian blur
			get result() { return _result },
			set result(value) {
				_result = value;
				tmp[name].setAttribute('result', value );
			},
			//SourceGraphic | SourceAlpha | BackgroundImage | BackgroundAlpha | FillPaint | StrokePaint
			get in1() { return _in1 },
			set in1(value) {
				_in1 = value;
				tmp[name].setAttribute('in', value );
			},
			//SourceGraphic | SourceAlpha | BackgroundImage | BackgroundAlpha | FillPaint | StrokePaint
			get in2() { return _in2 },
			set in2(value) {
				_in2 = value;
				tmp[name].setAttribute('in2', value );
			},
			//indicates a shift along the x-axis on the position of an element or its content
			get dx() { return _dx },
			set dx(value) {
				_dx = value;
				tmp[name].setAttribute('dx', value );
			},
			//indicates a shift along the y-axis on the position of an element or its content
			get dy() { return _dy },
			set dy(value) {
				_dy = value;
				tmp[name].setAttribute('dy', value );
			},
			//<feBlend> normal | multiply | screen | darken | lighten
			get mode() { return _mode },
			set mode(value) {
				_mode = value;
				tmp[name].setAttribute('mode', value );
			},
			//defines the standard deviation for the blur operation
			get stdDeviation() { return _stdDeviation },
			set stdDeviation(value) {
				_stdDeviation = value;
				tmp[name].setAttribute('stdDeviation', value );
			},
			//<animateTransform> translate | scale | rotate | skewX | skewY
			//<feColorMatrix> matrix | saturate | hueRotate | luminanceToAlpha
			//<feFuncR>, <feFuncG>, <feFuncB>, <feFuncA> identity | table | discrete | linear | gamma
			//<feTurbulence> fractalNoise | turbulence
			get type() { return _type },
			set type(value) {
				_type = value;
				tmp[name].setAttribute('type', value );
			},
			// uri / none / inherit
			get filter() { return _filter },
			set filter(value) {
				_filter = value;
				tmp[name].setAttribute('filter', value );
			},
			// value depends on type of animation element or <feColorMatrix>
			get values() { return _values },
			set values(value) {
				_values = value;
				tmp[name].setAttribute('values', value );
			}
		};

		return proxy;
	};
}
