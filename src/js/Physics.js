var Physics = {
	acceleration: function(v, u, t) {
		//acceleration = ∆velocity / ∆time
		// v = final velocity, u = initial velocity, t = time
		return (v - u) / t;
	},

	kineticEnergy: function(m, v) {
		// KE = kinetic energy (joules)
		//m = mass, v = velocity
		return 0.5 * m * (v * v);
	},

	friction: function(µ, N) {
		// µ = friction coefficient, N = normal force
		return µ * N;
	},

	momentum: function(m, v) {
		// momentum = mass(m) * velocity(v)
		return m * v;
	},

	force: function(m, a) {
		return m * a;
	},

	velocity: function(t, v, F, m) {
		if(v) {
			return this.acceleration(v, 0, t) * t;		// velocity from average acceleration over time
		} else if(F && m) {
			return (F * t) / m;	// velocity from Force on Mass over time
		} else {
			return false;
		}
	},
// FLUID DENSITY EXPRESSED AS RHO
	RHO: {
		ACETYLENE: 0.0017,
		ALCOHOL: 0.82,
		ALUMINUM: 2.72,
		BRASS: 8.48,
		CADMIUM: 8.57,
		CARBON_DIOXIDE: 0.00198,
		CARBON_MONOXIDE: 0.00126,
		CAST_IRON: 7.20,
		CHROMIUM: 7.03,
		COPPER: 8.79,
		DRY_AIR: 0.0013,
		HYDROGEN: 0.00009,
		LEAD: 11.35,
		MERCURY: 13.95,
		NICKEL: 8.73,
		NITROGEN: 0.00125,
		NYLON: 1.12,
		OXYGEN: 0.00143,
		PARAFFIN: 0.80,
		PETROL: 0.72,
		PVC: 1.36,
		RUBBER: 0.96,
		SEA_WATER: 1.02,
		STEEL: 7.82,
		TIN: 7.28,
		WATER: 1.00,
		ZINC: 7.12
	},
// COEFFICIENT OF DRAG (CD) - SURFACE AREA OF A PROJECTED FACE
	CD: {
		SPHERE: 0.47,
		HALF_SPHERE: 0.42,
		CONE: 0.5,
		ANGLED_CUBE: 0.8,
		LONG_CYLINDER: 0.9,
		SHORT_CYLINDER: 1.15,
		WATER_DROP: 0.04,
		HALF_WATER_DROP: 0.09
	},
// Acceleration due to gravity on earth m/s^2
	GRAVITY: 9.81,

	FR: {
		STEEL: 0.8,
		BRASS: 0.35,
		CASTIRON: 0.4,
		BRONZE: 0.16,
		GRAPHITE: 0.1,
		TEFLON:	0.04,
		ALUMINUM: 1.35,
		COPPER: 1,
		IRON: 1,
		SILVER: 1.4,
		GLASS: 0.9,
		DIAMOND: 0.1,
		NYLON: 0.15,
		RUBBER: 2,
		WOOD_CLEAN: 0.25,
		WOOD_WET: 0.2
	}

};

