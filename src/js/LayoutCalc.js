var layoutCalcs = {

	logoView:{
		x:function(_logoWidth) {
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.logoAlignHorizontal) {
					case 'left':
						return LAYOUT_MODEL.logoOffsetX;
						break;
					case 'center':
						return ((stage.width - _logoWidth) * 0.5) + LAYOUT_MODEL.logoOffsetX;
						break;
					case 'right':
						return stage.width - _logoWidth - LAYOUT_MODEL.logoOffsetX;
						break;
				}
			}
			return 0;
		},
		y:function(_logoHeight) {
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.logoAlignVertical) {
					case 'top':
						return LAYOUT_MODEL.logoOffsetY;
						break;
					case 'center':
						return ((stage.height - _logoHeight) * 0.5) + LAYOUT_MODEL.logoOffsetY;
						break;
					case 'bottom':
						return stage.height - _logoHeight - LAYOUT_MODEL.logoOffsetY;
						break;
				}
			}
			return 0;
		}
	},

	menuView:{
		x:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			} else if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				return stage.width - menuView.width() - LAYOUT_MODEL.sitePaddingLeft;
			}
			return LAYOUT_MODEL.sitePaddingLeft;
		},
		y:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			}
			return LAYOUT_MODEL.sitePaddingTop;
		},
		width:function() {
			if(USER_AGENT === POD) {
				return stage.width;
			} else if(USER_AGENT === PAD) {
				return LAYOUT_MODEL.menuWidth;
			} else {
				return LAYOUT_MODEL.menuWidth;
			}
		},
		height:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return stage.height;
			}
			return (stage.height - (LAYOUT_MODEL.sitePaddingTop + LAYOUT_MODEL.sitePaddingBottom));
		}
	},

	mediaView:{
		x:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			} else if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				return LAYOUT_MODEL.sitePaddingLeft;
			}
			return LAYOUT_MODEL.sitePaddingLeft + LAYOUT_MODEL.menuWidth - LAYOUT_MODEL.menuOffsetX;
		},
		y:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			}
			return LAYOUT_MODEL.sitePaddingTop;
		},
		width:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return stage.width;
			} else if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				return (stage.width - (LAYOUT_MODEL.sitePaddingLeft + LAYOUT_MODEL.sitePaddingRight + layoutCalcs.menuView.width() + LAYOUT_MODEL.menuOffsetX));
			}
			return (stage.width - (LAYOUT_MODEL.sitePaddingLeft + LAYOUT_MODEL.sitePaddingRight + layoutCalcs.menuView.width() - LAYOUT_MODEL.menuOffsetX));
		},
		height:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return stage.height;
			}
			return (stage.height - (LAYOUT_MODEL.sitePaddingTop + LAYOUT_MODEL.sitePaddingBottom));
		}
	}

};
