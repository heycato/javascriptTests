function System() {

	var _master = undefined,
		_Event = new EventDispatcher();

	function _map(key, klass, single) {

		if(_system.master) {
			if(single && _system.master.records[key]) {
				throw "'" + key + "' already registered";
			}
		}
		if(_system.records[key]) {
			throw "'" + key + "' already registered";
		}
		if(typeof klass === 'function') {
			_system.records[key] = {
				name: key,
				instance: undefined,
				func: function(c, vars) {
					vars = vars || {};
					var l = _system.imports.length;
					while(l--) {
						if(_system.imports[l].target === key || _system.imports[l].target === 'global') {
							vars[_system.imports[l].source] = _system.imports[l].func;
						}
					}
					return new klass(vars);
				},
				single: single	
			};
		} else if(typeof klass === 'object') {
			_system.records[key] = {
				name: key,
				object: klass,
				func: function(c) {
					return this.object;
				},
				single: true
			};
		}
	}

	function _resolve(key, vars) {
		if(!_system.records[key]) {
			throw "'" + key + "' not registered";
		}
		if(_system.records[key].single) {
			if(!_system.records[key].instance) {
				_system.records[key].instance = _instantiate(_system.records[key], vars);
			}
			return _system.records[key].instance;
		}
		return _instantiate(_system.records[key], vars);
	}

	function _instantiate(record, vars) {
		return record.func(_system, vars);
	}

	var _system = {

		records: {},
		subSystems: [],
		call: _resolve,
		listeners: [],
		imports: [],
		trash: [],
		get master() { return _master },
		set master(system) {
			_master = system;
		},

		addEventListener: _Event.addEventListener,
		removeEventListener: _Event.removeEventListener,
		dispatchEvent: function(event) {
			_Event.dispatchEvent(event);
			if(_system.subSystems.length > 0) {
				var i = _system.subSystems.length;
				while(i --) {
					_system.subSystems[i].dispatchEvent(event);
				}
			}
		},

		notify: function(event) {
			if(_master) {
				_master.dispatchEvent(event);
			} else {
				throw 'no master system to notify';
			}
		},

		mapClass: function(key, klass) {
			_map(key, klass, false);
		},

		mapSingleton: function(key, klass) {
			_map(key, klass, true);
		},

		mapObject: function(key, obj) {
			_map(key, obj, true);
		},

		mapImport: function(sourceKey, targetKey, importName) {
			var record,
				call,
				_targetKey = targetKey || 'global';
			if(_system.records[sourceKey]) {						// check if already mapped to _system
				record = _system.records[sourceKey];
				call = _resolve;
			} else if(_master) {								// check if already mapped to _system.master
				if(_system.master.records[sourceKey]) {
					record = _master.records[sourceKey];
					call = _master.call;
				}
			} else {
				record = undefined;
			}
			if(!record) {
				throw "'" + sourceKey + "' has not been mapped";
			}
			if(!_system.records[_targetKey]) {
				if(_targetKey !== 'global') {
					throw "'" + _targetKey + "' has not been mapped";
				}
			}
			var importKey = importName || sourceKey;
			_system.imports.push({
				source: importKey,
				target: _targetKey,
				func: function(vars) {
					return call(sourceKey, vars);
				}
			});
		},

		mapSystem: function(key, system) {
			if(_system.subSystems[key]) {
				throw "'" + key + "' already registered";
			}
			_system.subSystems[key] = system;
			_system.subSystems[key].handlers = _system.listeners;		//subsystem handlers points to master systems' listeners
			system.master = _system;
		},

		removeInstance: function(key) {
			if(_system.records[key].single && _system.records[key].instance) {
				_system.trash.push(_system.records[key].instance);
				delete _system.records[key].instance;
				if(_system.trash.length > 10) {
					_system.trash.splice(0 , 1);
				}
			} else {
				throw "'" + key + "' not removed";
			}
		},

		unmap: function(key) {
			if(!_system.records[key]) {
				throw "'" + key + "' not registered";
			}
			_system.trash.push(_system.records[key]);
			delete _system.records[ key ];
		}

	};

	_map('Event', _Event, true);
	_system.mapImport('Event');

	return _system;

}