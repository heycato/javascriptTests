(function(scope) {
	"use strict";
	var dijon = {
		VERSION:'0.5.3'
	};
	dijon.System = function() {
		this._mappings = {};
		this._outlets = {};
		this._handlers = {};
		this.strictInjections = true;
		this.autoMapOutlets = false;
		this.postInjectionHook = 'setup';
	};
	dijon.System.prototype = {
		_createAndSetupInstance:function(key, Clazz) {
			var instance = new Clazz();
			this.injectInto( instance, key );
			return instance;
		},
		_retrieveFromCacheOrCreate:function(key, overrideRules) {
			if(typeof overrideRules === 'undefined') {
				overrideRules = false;
			}
			var output;
			if(this._mappings.hasOwnProperty(key)) {
				var config = this._mappings[key];
				if(!overrideRules && config.isSingleton) {
					if(config.object == null) {
						config.object = this._createAndSetupInstance(key, config.clazz);
					}
					output = config.object;
				} else {
					if(config.clazz) {
						output = this._createAndSetupInstance(key, config.clazz);
					} else {
						output = config.object;
					}
				}
			} else {
				throw new Error(1000);
			}
			return output;
		},
		mapOutlet:function(sourceKey, targetKey, outletName) {
			if(typeof sourceKey === 'undefined') {
				throw new Error(1010);
			}
			targetKey = targetKey || "global";
			outletName = outletName || sourceKey;
			if(!this._outlets.hasOwnProperty(targetKey)) {
				this._outlets[targetKey] = {};
			}
			this._outlets[targetKey][outletName] = sourceKey;
			return this;
		},
		getObject:function(key) {
			if(typeof key === 'undefined') {
				throw new Error(1020);
			}
			return this._retrieveFromCacheOrCreate(key);
		},
		mapValue:function(key, useValue) {
			if(typeof key === 'undefined') {
				throw new Error(1030);
			}
			this._mappings[key] = {
				clazz:null,
				object:useValue,
				isSingleton:true
			};
			if(this.autoMapOutlets) {
				this.mapOutlet(key);
			}
			if(this.hasMapping(key)) {
				this.injectInto(useValue, key);
			}
			return this;
		},
		hasMapping:function(key) {
			if(typeof key === 'undefined') {
				throw new Error(1040);
			}
			return this._mappings.hasOwnProperty(key);
		},
		mapClass:function(key, clazz) {
			if(typeof key === 'undefined') {
				throw new Error(1050);
			}
			if(typeof clazz === 'undefined') {
				throw new Error(1051);
			}
			this._mappings[key] = {
				clazz:clazz,
				object:null,
				isSingleton:false
			};
			if(this.autoMapOutlets) {
				this.mapOutlet(key);
			}
			return this;
		},
		mapSingleton:function(key, clazz) {
			if(typeof key === 'undefined') {
				throw new Error( 1060 );
			}
			if(typeof clazz === 'undefined') {
				throw new Error(1061);
			}
			this._mappings[key] = {
				clazz:clazz,
				object:null,
				isSingleton:true
			};
			if(this.autoMapOutlets) {
				this.mapOutlet(key);
			}
			return this;
		},
		instantiate:function(key) {
			if(typeof key === 'undefined') {
				throw new Error(1070);
			}
			return this._retrieveFromCacheOrCreate(key, true);
		},
		injectInto:function(instance, key) {
			if(typeof instance === 'undefined') {
				throw new Error(1080);
			}
			if(typeof instance === 'object') {
				var o = [];
				if(this._outlets.hasOwnProperty('global')) {
					o.push(this._outlets['global']);			//this.autoMapOutlets = true
				}
				if(typeof key !== 'undefined' && this._outlets.hasOwnProperty(key)) {
					o.push(this._outlets[key]); // list of dependencies mapped to key
				}
				for(var i in o) {
					var l = o [i]; // every import for key
					for(var outlet in l) {
						var source = l[outlet];
						if(!this.strictInjections || outlet in instance) {
							instance[outlet] = this.getObject(source);
						}
					}
				}
				if("setup" in instance) {
					instance.setup.call(instance);
				}
			}
			return this;
		},
		unmap:function(key) {
			if(typeof key === 'undefined') {
				throw new Error(1090);
			}
			delete this._mappings[key];
			return this;
		},
		unmapOutlet:function(target, outlet) {
			if(typeof target === 'undefined') {
				throw new Error(1100);
			}
			if(typeof outlet === 'undefined') {
				throw new Error(1101);
			}
			delete this._outlets[target][outlet];
			return this;
		},
		mapHandler:function(eventName, key, handler, oneShot, passEvent) {
			if(typeof eventName === 'undefined') {
				throw new Error(1110);
			}
			key = key || 'global';
			handler = handler || eventName;
			if(typeof oneShot === 'undefined') {
				oneShot = false;
			}
			if(typeof passEvent === 'undefined') {
				passEvent = false;
			}
			if(!this._handlers.hasOwnProperty(eventName)) {
				this._handlers[ eventName ] = {};
			}
			if(!this._handlers[eventName].hasOwnProperty(key)) {
				this._handlers[eventName][key] = [];
			}
			this._handlers[eventName][key].push({
				handler:handler,
				oneShot:oneShot,
				passEvent:passEvent
			});
			return this;
		},
		unmapHandler:function(eventName, key, handler) {
			if(typeof eventName === 'undefined') {
				throw new Error(1120);
			}
			key = key || 'global';
			handler = handler || eventName;
			if(this._handlers.hasOwnProperty(eventName) && this._handlers[eventName].hasOwnProperty(key)) {
				var handlers = this._handlers[eventName][key];
				for(var i in handlers) {
					var config = handlers[i];
					if(config.handler === handler) {
						handlers.splice(i, 1);
						break;
					}
				}
			}
			return this;
		},
		notify:function(eventName) {
			if(typeof eventName === 'undefined') {
				throw new Error(1130);
			}
			var argsWithEvent = Array.prototype.slice.call(arguments),
				argsClean = argsWithEvent.slice(1);
			if(this._handlers.hasOwnProperty(eventName)) {
				var handlers = this._handlers[eventName];
				for(var key in handlers) {
					var configs = handlers[key],
						instance;
					if(key !== 'global') {
						instance = this.getObject(key);
					}
					var toBeDeleted = [],
						i, n;
					for(i = 0, n = configs.length ; i < n ; i++) {
						var handler,
							config = configs[i];
						if(instance && typeof config.handler === "string") {
							handler = instance[config.handler];
						} else {
							handler = config.handler;
						}
						if(config.oneShot) {
							toBeDeleted.unshift(i);
						}
						if(config.passEvent) {
							handler.apply(instance, argsWithEvent);
						} else {
							handler.apply(instance, argsClean);
						}
					}
					for(i = 0, n = toBeDeleted.length ; i < n ; i++) {
						configs.splice(toBeDeleted[i], 1);
					}
				}
			}
			return this;
		}
	};
	scope.dijon = dijon;
}(this));

