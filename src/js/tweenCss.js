function Tween(target, duration, property) {

	var propertyIndex = 0,
		transforms = [],
		transitions = [],
		styles = {},
		tweenEnd = duration * 1000,
		timer,
		curve = {
			linear: 'cubic-bezier(0,0,1,1)',
			ease: 'cubic-bezier(0.25,0.1,0.25,1)',
			easeIn: 'cubic-bezier(0.42,0,1,1)',
			easeOut: 'cubic-bezier(0,0,0.58,1)',
			easeInOut: 'cubic-bezier(0.42,0,0.58,1)'
		},
		easing = property['ease'] ? setEase(property['ease']) : curve.ease,
		delay = property['delay'] || 0;

	for(var key in property) {
		if(property.hasOwnProperty(key)) {

			// if key needs conversion
			var prop = convertKey(key),
				value = property[key];

			// skip specials keys
			if(/^onComplete|onCompleteScope|onCompleteParams|ease|delay$/.test(prop)) {
				continue;

			// transform keys
			} else if(/^x|y|rotate|scale/.test(prop)) {
				switch(prop) {
					case 'x':
						transforms.push('translateX(' + value + 'px)');
						break;
					case 'y':
						transforms.push('translateY(' + value + 'px)');
						break;
					case 'rotate':
						transforms.push('rotate(' + value + 'deg)');
						break;
					case 'scale':
						transforms.push('scale(' + value + ')');
						break;
					default:
						break;
				}

			// styles needing 'px' appended
			} else if(/^width|height|paddingTop|paddingRight|paddingBottom|paddingLeft|marginTop|marginRight|marginBottom|marginLeft/.test(prop)) {
				transitions.push(prop + ' ' + duration + 's ' + easing + ' ' + delay + 's');
				styles[prop] = value + 'px';
			} else {
				if(prop === 'backgroundColor') {
					var ffBg = 'background-color';
				}
			// all other style keys
				transitions.push((ffBg || prop) + ' ' + duration + 's ' + easing + ' ' + delay + 's');
				styles[prop] = value;
			}
			propertyIndex ++;
		}
	}

	function buildString(strings, separator) {
		var l = strings.length,
			value = '';
		while(l --) {
			if(l < 1) {
				value += strings[l];
			} else {
				value += strings[l] + separator;
			}
		}
		return value;
	}

	function buildStyles() {
		for(var prop in styles) {
			target.style[prop] = styles[prop];
		}
	}

	function fireTweens() {
		var transform = window.webkitURL ? '-webkit-transform' : 'transform',
			transition = window.webkitURL ? '-webkit-transition' : 'transition';
		clearTimeout(timer);
		timer = setTimeout(complete, tweenEnd);
		target.style[transition] = buildString(transitions, ', ');
		target.style[transition] += ', ' + transform + ' ' + duration + 's ' + easing + ' ' + delay + 's';
		target.style[transform] = buildString(transforms, ' ');
		buildStyles();
	}

	function convertKey(propKey) {
		switch(propKey) {
			case 'backgroundColor':
				return window.webkitURL ? 'background-color' : 'backgroundColor';
				break;
			default:
				return propKey;
				break;
		}
	}

	function setEasing(propValue) {
		switch(propValue) {
			case 'linear':
				return curve.linear;
				break;
			case 'easeIn':
				return curve.easeIn;
				break;
			case 'easeOut':
				return curve.easeOut;
				break;
			case 'easeInOut':
				return curve.easeInOut;
				break;
			default:
				return curve.ease;
				break;
		}
	}

	function complete() {
		property['onComplete'].apply(property['onCompleteScope'] || target, property['onCompleteParams']);
	}

	fireTweens();

}

