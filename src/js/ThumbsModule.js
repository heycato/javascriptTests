/*

 {
 "titleFontFamily":"Arial",
 "horizontalMargin":0,
 "defaultOn":true,
 "masonry":false,
 "dock":"displace",
 "verticalMargin":0,
 "type":"strip",
 "side":"bottom",
 "titleFontSize":0,
 "titleFontColor":"#ff0000",
 "gap":0,
 "size":"medium"
 }

 */

function ThumbsModule(vars) {

	"use strict";

	var self = this,
		container,
		mask,
		grid,
		axis = vars.settings.axis === 'horizontal' ? 'x' : 'y',
		open = false,
		resizing = false,
		loading = false,
		sequential = false,
		thumbArr = [],
		index = 0,
		sizes = {small: {width: 100, height: 66}, medium: {width: 200, height: 133}, large: {width: 300, height: 200}},
		thumbsStyle,
		toggle,
		tile = new Tile({
			marginHorizontal: vars.settings.horizontalMargin,
			marginVertical: vars.settings.verticalMargin,
			gap: vars.settings.gap
		});

	this.build = function() {
		container = this.container = new Sprite({collection:'thumbs'});
		if(vars.settings.dock === 'overlay' || vars.settings.type === 'fill') {
			var c1 = parseColor(vars.settings.overlayColor);
			this.container.backgroundColor = 'rgba(' + c1[0] + ',' + c1[1] + ',' + c1[2] + ',' + vars.settings.overlayAlpha + ')';
		}
		this.container.id = 'thumbs';
		this.container.alpha = 0;

		mask = new Sprite();
		mask.id = 'mask';
		mask.overflow = 'hidden';
		container.addChild(mask);

		grid = new Sprite();
		grid.id = 'grid';
		mask.addChild(grid);

		makeHolders(grid);
		highlight();

		toggle = new Toggle({
			target: this,
			on: this.hide,
			off: this.show
		});

		defineThumbsStyle();

		this.resize();

//		if(PAD) {
//
//			this.scroll = new Scroll(grid, mask, {
//				types:['touch'],
//				axis:axis,
//				margin:vars.settings.verticalMargin
//			});
//
//		} else if((vars.settings.scrollType === 'scrollbar' || PAD) && vars.settings.type === 'fill') {

			this.scroll = new Scroll(grid, mask, {
				types:['hover','touch'],
				axis:axis,
				color:vars.settings.scrollbarColor,
				hover:vars.settings.scrollbarHover,
				side:vars.settings.scrollbarAlignment,
				offsetX:vars.settings.scrollbarHorizontalOffset,
				offsetY:vars.settings.scrollbarVerticalOffset,
				margin:vars.settings.verticalMargin,
				width:8
			});

//		} else if(vars.settings.scrollType === 'mouse cursor' || vars.settings.type === 'strip') {
//
//			this.scroll = new Scroll(grid, mask, {types:['hover','touch'], axis:axis});

//			this.scroll = new Scroll(grid, mask, {
//				types:['bar','wheel','touch'],
//				axis:axis,
//				color:vars.settings.scrollbarColor,
//				hover:vars.settings.scrollbarHover,
//				side:vars.settings.scrollbarAlignment,
//				offsetX:vars.settings.scrollbarHorizontalOffset,
//				offsetY:vars.settings.scrollbarVerticalOffset,
//				margin:vars.settings.verticalMargin,
//				width:8
//			});

//		}

		this.show();
	};

	function makeHolders(grid) {
		var items = vars.mediaItems,
			length = items.length,
			i = 0;
		for(i; i < length; i++) {
			var holder = new Sprite({id:i}),
				width = sizes[vars.settings.size].width,
				height = sizes[vars.settings.size].height;
			holder.width = width;
			holder.height = height;
			holder.overflow = 'hidden';
			holder.filename = items[i].thumb;
			holder.index = i;

			if(vars.settings.type === 'fill' && !vars.settings.masonry) {
				var c1 = parseColor(vars.settings.overlayColor);
				holder.backgroundColor = "rgba(" + c1[0] + "," + c1[1] + "," + c1[2] + "," + vars.settings.holderBgAlpha + ")";
			}

			var hit = new Sprite();
			hit.id = 'hit';
			hit.zIndex = 3;
			hit.alpha = 0;
			switch(vars.settings.type) {
				case 'fill':
					hit.width = width - 2;
					hit.height = height - 2;
					hit.border = '1px solid';
					hit.borderColor = vars.settings.hoverColor;
					break;
				case 'strip':
					hit.width = width;
					hit.height = height;
					hit.backgroundColor = vars.settings.hoverColor;
					break;
			}
			holder.addChild(hit);

			tile.addItem(width, height);
			grid.addChild(holder);
			thumbArr.push(holder);

			hit.addEventListener('mouseover', onMouseOver);
			hit.addEventListener('mouseout', onMouseOut);
			hit.addEventListener('click', select);
			hit.addEventListener('touchend', select);
		}
	}

	function defineThumbsStyle() {
		switch(vars.settings.type) {
			case 'fill':
				thumbsStyle = new ThumbsFill({
					container: container,
					mask: mask,
					items: thumbArr,
					side: vars.settings.side,
					sizes: sizes,
					size: vars.settings.size,
					horizontalMargin: vars.settings.horizontalMargin,
					verticalMargin: vars.settings.verticalMargin,
					masonry: vars.settings.masonry,
					tile: tile
				});
				break;
			case 'strip':
				thumbsStyle = new ThumbsStrip({
					container: container,
					mask: mask,
					items: thumbArr,
					side: vars.settings.side,
					sizes: sizes,
					size: vars.settings.size,
					horizontalMargin: vars.settings.horizontalMargin,
					verticalMargin: vars.settings.verticalMargin,
					tile: tile
				});
				break;
		}
	}

	function onMouseOver(e) {
		switch(vars.settings.type) {
			case 'fill':
				Tween.to(e.target, 0.7, {alpha:1, borderColor:vars.settings.hoverColor});
				break;
			case 'strip':
				Tween.to(e.target, 0.7, {alpha:0.3, backgroundColor:vars.settings.hoverColor});
				break;
		}
	}

	function onMouseOut(e) {
		if((Site.curImgNum - 1) !== e.target.parent.index) {
			Tween.to(e.target, 0.25, {alpha:0, borderColor:vars.settings.overlayColor});
		} else {
			switch(vars.settings.type) {
				case 'fill':
					Tween.to(e.target, 0.42, {alpha:1, borderColor:vars.settings.selectedColor});
					break;
				case 'strip':
					Tween.to(e.target, 0.7, {alpha:0.3, backgroundColor:vars.settings.selectedColor});
					break;
			}
		}
	}

	function select(e) {
		var mediaNum = Number(e.target.parent.index) + 1;
		Address.setPath(Site.curPath + "/" + mediaNum);
	}

	function highlight() {
		if(Site.curImgNum > 0) {
			switch(vars.settings.type) {
				case 'fill':
					Tween.to(thumbArr[Site.curImgNum - 1].hit, 0.3, {alpha:1, borderColor:vars.settings.selectedColor});
					break;
				case 'strip':
					Tween.to(thumbArr[Site.curImgNum - 1].hit, 0.3, {alpha:0.3, backgroundColor:vars.settings.selectedColor});
					break;
			}
		}
	}

	this.hide = function() {
		Tween.to(container, 0.35, {alpha:0, onComplete:function() {
			this.display = 'none';
		}});
		toggle.state = false;
	};

	this.show = function() {
		container.display = 'block';
		Tween.to(container, 0.35, {alpha:1});
		toggle.state = true;
	};

	this.toggle = function() {
		toggle.flip();
	};

	this.resize = function() {
		thumbsStyle.resize();
	};


	this.build();
}


function Dock() {

}


function ThumbsFill(vars) {

	var self = this,
		container = vars.container,
		mask = vars.mask,
		side = vars.side,
		sizes = vars.sizes,
		size = vars.size,
		items = vars.items,
		masonry = vars.masonry,
		tile = vars.tile;

	tile.axis = 'y';
	tile.align = 'left';
	tile.wrap = true;

	function arrange() {
		tile.perpLength = stage.width - (vars.horizontalMargin * 2);
		tile.layoutItems();
		var length = items.length,
			i = 0;
		for(i; i < length; i++) {
			var position = tile.getPosition(i);
			/*if(items[length - 1].loaded && !items[i].positioning) {
			 items[i].positioning = true;
			 Tween.to(items[i], 1, {x:position.x, y:position.y, onComplete:positionComplete});
			 } else if(!items[i].positioning) {*/
			items[i].x = position.x;
			items[i].y = position.y;
			/*}*/
			if(!items[i].loading && !items[i].loaded) {
				var thumb = new Bitmap();
				thumb.id = 'img';
				if(masonry) {
					thumb.src = getThumb(sizes[size].width, 600, items[i].filename);
				} else {
					thumb.src = getThumb(sizes[size].width, sizes[size].height, items[i].filename);
				}
				thumb.zIndex = 2;
				thumb.alpha = 0;
				items[i].loading = true;
				thumb.addEventListener('load', self.thumbLoaded);
				items[i].addChild(thumb);
			}
		}
	}

	function positionComplete() {
		this.positioning = false;
	}

	this.resize = function() {
		container.width = stage.width;
		container.height = stage.height;
		container.left = mediaViewLeft;
		container.top = sitePaddingTop;
		mask.width = container.width;
		mask.height = container.height;
		arrange();
		var bounds = tile.getBounds();
		mask.grid.width = bounds.width;
		mask.grid.height = bounds.height;
		var align = new Align({hRange:stage.width, vRange:stage.height, width:bounds.width, height:bounds.height, hAlign:'center', vAlign:'top', hOffset:0, vOffset:vars.verticalMargin});
		mask.grid.x = align.x;
		mask.grid.y = align.y;
	};

	this.thumbLoaded = function() {
		this.parent.loaded = true;
		this.parent.loading = false;
		var width = this.width;
		var height = this.height;
		var ratio = width / height;
		if(RETINA) {
			this.width = width * 0.5;
			this.height = height * 0.5;
		} else {
			this.width = width;
			this.height = height;
		}
		if(masonry) {
			this.width = sizes[size].width;
			this.height = sizes[size].width / ratio;
			this.parent.height = this.height;
			this.parent.hit.width = this.width - 2;
			this.parent.hit.height = this.height - 2;
		} else {
			var align = new Align({hRange:this.parent.width, vRange:this.parent.height, width:this.width, height:this.height, hAlign:'center', vAlign:'center', hOffset:0, vOffset:0});
			this.left = align.x;
			this.top = align.y;
		}
		tile.setSize(this.parent.index, this.parent.width, this.parent.height);
		self.resize();
		Tween.to(this, 1, {alpha:1});
	};

}

function ThumbsStrip(vars) {

	var self = this,
		container = vars.container,
		mask = vars.mask,
		side = vars.side,
		sizes = vars.sizes,
		size = vars.size,
		items = vars.items,
		tile = vars.tile;

	tile.align = 'bottom';
	tile.wrap = true;

	function arrange() {
		tile.layoutItems();
		var length = items.length,
			i = 0;
		for(i; i < length; i++) {
			var position = tile.getPosition(i);
			items[i].left = position.x;
			items[i].top = position.y;
			if(!items[i].loading && !items[i].loaded) {
				var thumb = new Bitmap();
				thumb.id = 'img';
				thumb.src = getThumb(sizes[size].width, sizes[size].height, items[i].filename);
				thumb.parent = items[i];
				thumb.alpha = 0;
				items[i].loading = true;
				thumb.addEventListener('load', self.thumbLoaded);
				items[i].addChild(thumb);
			}
		}
	}

	function top() {
		tile.axis = 'x';
		tile.perpLength = sizes[size].height;
		container.width = stage.width;
		container.height = sizes[size].height;
		container.left = mediaViewLeft;
		container.top = sitePaddingTop;
		mask.width = container.width;
		mask.height = container.height;
	}

	function right() {
		tile.axis = 'y';
		tile.perpLength = sizes[size].width;
		container.width = sizes[size].width;
		container.height = stage.height;
		container.left = mediaViewLeft + stage.width - sizes[size].width;
		container.top = sitePaddingTop;
		mask.width = container.width;
		mask.height = container.height;
	}

	function bottom() {
		tile.axis = 'x';
		tile.perpLength = sizes[size].height;
		container.width = stage.width;
		container.height = sizes[size].height;
		container.left = mediaViewLeft;
		container.top = sitePaddingTop + stage.height - sizes[size].height;
		mask.width = container.width;
		mask.height = container.height;
	}

	function left() {
		tile.axis = 'y';
		tile.perpLength = sizes[size].width;
		container.width = sizes[size].width;
		container.height = stage.height;
		container.left = mediaViewLeft;
		container.top = sitePaddingTop;
		mask.width = container.width;
		mask.height = container.height;
	}

	this.resize = function() {
		switch(side) {
			case 'top':
				top();
				break;
			case 'right':
				right();
				break;
			case 'bottom':
				bottom();
				break;
			case 'left':
				left();
				break;
		}
		arrange();
		var bounds = tile.getBounds();
		mask.grid.width = bounds.width;
		mask.grid.height = bounds.height;
		var center = 0;
		if(tile.axis === 'vertical') {
			center = (stage.height - bounds.height) * 0.5;
//			mask.grid.top = center;
//			Tween.to(mask.grid, 1, {top:center});
		} else {
			center = (stage.width - bounds.width) * 0.5;
//			mask.grid.left = center;
//			Tween.to(mask.grid, 1, {left:center});
		}
	};

	this.thumbLoaded = function() {
		this.parent.loaded = true;
		this.parent.loading = false;

		var width = this.width;
		var height = this.height;
		var ratio = width / height;

		if(RETINA) {
			this.width = width * 0.5;
			this.height = height * 0.5;
		} else {
			this.width = width;
			this.height = height;
		}

		if(side === 'top' || side === 'bottom') {
			this.height = sizes[size].height;
			this.width = sizes[size].height * ratio;
		} else {
			this.width = sizes[size].width;
			this.height = sizes[size].width / ratio;
		}

		this.parent.width = this.width;
		this.parent.hit.width = this.width;
		this.parent.hit.height = this.height;
		tile.setSize(this.parent.index, this.width, this.height);
		self.resize();
		Tween.to(this, 1, {alpha:1});
	};
}
