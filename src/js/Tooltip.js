var Tooltip = function(args) {

	this.allElems = [];
	this.tipTargets = [];
	this.tipArw = "";
	this.tipDiv = "";
	this.txtSize = 12 + 'px';
	this.txtColor = '#373737';
	this.bgColor = 'rgba(255,255,255,0.75)';
	this.cornerRad = 0;

	for(var key in args) {
        if(args.hasOwnProperty(key)) {
            this[key] = args[key];
        }
    }

	this.init = function() {
		this.tipDiv = document.getElementById('tooltip');
		this.tipArw = document.getElementById('tip-arrow');
		this.allElems = document.getElementsByTagName('*');
		for(var i = 0; i < this.allElems.length; i += 1) {
			if(this.allElems[i].getAttribute('data-tip-text') && this.allElems[i].getAttribute('data-tip-text') !== ' ') {
				this.tipTargets.push(this.allElems[i]);
			}
		}
		this.hoverTarget();
	};

	this.setStyles = function() {
		this.tipDiv.style.fontSize = this.txtSize;
		this.tipDiv.getElementsByTagName('p')[0].style.paddingTop = (parseInt(this.txtSize, 10) * 0.65) + 'px';
		this.tipDiv.getElementsByTagName('p')[0].style.paddingBottom = (parseInt(this.txtSize, 10) * 0.05) + 'px';
		this.tipDiv.style.color = this.txtColor;
		this.tipDiv.style.borderRadius = this.cornerRad;
		this.tipDiv.style.backgroundColor = this.bgColor;
		this.tipArw.style.borderBottomColor = this.bgColor;
	};

	var getStyle = function(obj, prop) {
		try {
            return (window.getComputedStyle(obj, null).getPropertyValue(prop));
        } catch(e) {
            //console.log(obj,prop);
            return false;
        }
	};

	var setXPos = function(ths, x) {
		if((ths.tipX - ths.arwPoint - parseInt(ths.cornerRad, 10)) < 0) {
			ths.tipArw.style.left = parseInt(ths.cornerRad, 10) - ths.tipPad + 'px';
			ths.tipDiv.style.left = x - (ths.arwPoint + parseInt(ths.cornerRad, 10)) + 'px';
		} else if((ths.tipX + ths.arwPoint + parseInt(ths.cornerRad, 10)) > ths.tipWidth) {
			ths.tipArw.style.left = (ths.tipWidth - (ths.tipPad + ths.arwWidth + parseInt(ths.cornerRad, 10))) + 'px';
			ths.tipDiv.style.left = (x - ths.tipWidth) + ths.arwPoint + parseInt(ths.cornerRad, 10) + 'px';
		} else {
			ths.tipArw.style.left = ths.tipX - ths.tipPad - ths.arwPoint + 'px';
			ths.tipDiv.style.left = x - ths.tipX + 'px';
		}
	};

    var setYPos = function(ths, x, y) {
        ths.xPerc = x / ths.winWidth;
        ths.tipX = Math.round(ths.tipWidth * ths.xPerc);
        if(y < ths.hAxis) {
            ths.tipArw.setAttribute('class', 'up-arrow');
            ths.tipArw.style.borderBottomColor = ths.bgColor;
            ths.tipDiv.style.top = y + ths.tipPad + ths.tipOffset + 'px';
            ths.tipArw.style.bottom = ths.tipHeight - ths.tipPad + 'px';
        } else {
            ths.tipArw.setAttribute('class', 'down-arrow');
            ths.tipArw.style.borderTopColor = ths.bgColor;
            ths.tipDiv.style.top = y - ths.tipHeight - ths.tipPad - ths.tipOffset + 'px';
            setXPos(ths, x);
            ths.tipArw.style.bottom = (ths.arwHeight + ths.tipPad) * -1 + 'px';
        }
    };

    var move = function(e) {
        setXPos(this.instance, e.pageX);
        setYPos(this.instance, e.pageX, e.pageY);
    };

	var hide = function() {
		var ths = this.instance;
		if(ths.isShowing) {
			ths.tipDiv.style.display = 'none';
			ths.isShowing = false;
		}
		this.removeEventListener('mousemove', move, false);
		this.removeEventListener('mouseout', hide, false);
	};

	var show = function(e) {
		var ths = this.instance;
		setXPos(ths, e.pageX);
		setYPos(ths, e.pageX, e.pageY);
		if(!ths.isShowing) {
			ths.setStyles();
			ths.tipText = this.getAttribute('data-tip-text');
			ths.tipDiv.getElementsByTagName('p')[0].innerHTML = ths.tipText;
			ths.tipDiv.style.display = 'block';
			ths.tipPad = parseInt(getStyle(ths.tipDiv, "padding"), 10);
			ths.arwWidth = parseInt(getStyle(ths.tipArw, "width"), 10);
			ths.arwHeight = parseInt(getStyle(ths.tipArw, "height"), 10);
			ths.tipWidth = parseInt(getStyle(ths.tipDiv, "width"), 10);
			ths.tipHeight = parseInt(getStyle(ths.tipDiv, "height"), 10);
			ths.winWidth = window.innerWidth;
			ths.winHeight = window.innerHeight;
			ths.hAxis = ths.winHeight * 0.5;
			ths.arwPoint = (ths.arwWidth / 2);
			ths.tipOffset = (ths.arwHeight * 2);
			ths.isShowing = true;
		}
		this.addEventListener('mousemove', move, false);
		this.addEventListener('mouseout', hide, false);
	};

    this.hoverTarget = function() {
        if(this.tipTargets.length > 0) {
            for(var i = 0; i < this.tipTargets.length; i += 1) {
                this.tipTargets[i].addEventListener('mouseover', show, false);
            }
        }
    };
};