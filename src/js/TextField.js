function TextField(vars) {
	var textField = vars.Sprite({type:'span'});
	textField.selectable = true;
	textField.fontFamily = 'sans-serif';
    return textField;
}