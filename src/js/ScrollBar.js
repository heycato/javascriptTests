function ScrollBar(scroll) {

	var obj = scroll.obj,
		mask = scroll.mask,
		parent = mask.parent,
		dragging = false,
		startPoint = 0;

	var backer = parent.backer = new Sprite();
	backer.alpha = scroll.alpha;
	backer.backgroundColor = scroll.color;
	parent.addChild(backer);

	var handle = parent.handle = new Sprite();
	handle.backgroundColor = scroll.color;
	parent.addChild(handle);

	handle.addEventListener(MOUSE_OVER, onMouseOver);
	handle.addEventListener(MOUSE_OUT, onMouseOut);
	handle.addEventListener(MOUSE_DOWN, onMouseDown);

	this.moveHandle = function() {
		handle[scroll.axis] = ((backer[scroll.dim] - handle[scroll.dim]) * scroll.percent) + backer[scroll.axis];
	};

	this.resize = function() {
		backer[scroll.perpDim] = scroll.width;
		backer[scroll.dim] = mask[scroll.dim];
		backer[scroll.axis] = mask[scroll.axis] + getOffset();

		handle[scroll.perpDim] = scroll.width;
		var handleSize = (mask[scroll.dim] / obj[scroll.dim]) * mask[scroll.dim];
		if(handleSize > backer[scroll.dim]) {
			Tween.to(backer, 1, {alpha:0});
			Tween.to(handle, 1, {alpha:0});
			handle[scroll.dim] = backer[scroll.dim];
		} else {
			Tween.to(backer, 1, {alpha:scroll.alpha});
			Tween.to(handle, 1, {alpha:1});
			handle[scroll.dim] = handleSize;
		}

		if(scroll.side === 'left' || scroll.side === 'top') {
			backer[scroll.perpAxis] = mask[scroll.perpAxis] - scroll.width + getPerpOffset();
			handle[scroll.perpAxis] = mask[scroll.perpAxis] - scroll.width + getPerpOffset();
		} else {
			backer[scroll.perpAxis] = mask[scroll.perpAxis] + mask[scroll.perpDim] + getPerpOffset();
			handle[scroll.perpAxis] = mask[scroll.perpAxis] + mask[scroll.perpDim] + getPerpOffset();
		}

		this.moveHandle();
	};

	function onMouseOver(e) {
		Tween.to(handle, 0.35, {backgroundColor:scroll.hover});
	}

	function onMouseOut(e) {
		if(!dragging) {
			Tween.to(handle, 0.35, {backgroundColor:scroll.color});
		}
	}

	function onMouseDown(e) {
		Tween.to(handle, 0.35, {backgroundColor:scroll.hover});
		startPoint = getPoint(e) - backer[scroll.axis] - handle[scroll.axis];
		stage.addEventListener(MOUSE_UP, onMouseUp, false);
		stage.addEventListener(MOUSE_MOVE, onMouseMove, false);
	}

	function onMouseMove(e) {
		dragging = true;
		moveDist = getPoint(e) - startPoint - backer[scroll.axis];
		scroll.percent = (moveDist - backer[scroll.axis]) / (backer[scroll.dim] - handle[scroll.dim]);
		scroll.update(0.35);
	}

	function onMouseUp(event) {
		dragging = false;
		Tween.to(handle, 0.35, {backgroundColor:scroll.color});
		stage.removeEventListener(MOUSE_MOVE, onMouseMove);
		stage.removeEventListener(MOUSE_UP, onMouseUp);
	}

	function getPoint(e) {
		if(scroll.axis === 'x') {
			return e.clientX;
		}
		return e.clientY;
	}

	function getOffset() {
		if(scroll.axis === 'x') {
			return scroll.offsetX;
		}
		return scroll.offsetY;
	}

	function getPerpOffset() {
		if(scroll.axis === 'y') {
			return scroll.offsetX;
		}
		return scroll.offsetY;
	}

}