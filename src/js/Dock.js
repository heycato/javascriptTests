var Docks = {

	arr: [],

	register: function(dock) {
		this.arr.push(dock);
	},

	unRegister: function(dock) {
		var i = this.arr.length;
		while(i--) {
			if(dock === this.arr[i]) {
				this.arr.splice(i, 1);
			}
		}
	},

	resize: function() {
		if(this.arr.length > 0) {
			var i = this.arr.length;
			while(i--) {
				this.arr[i].resize();
			}
		}
	}
};

function Dock(obj, mask, vars) {

	var side = vars.side || 'left',
		axis = side === 'top' || side === 'bottom' ? 'y' : 'x',
		pAxis = axis === 'x' ? 'y' : 'x',
		dim = axis === 'x' ? 'width' : 'height',
		pDim = axis === 'x' ? 'height' : 'width',
		align = vars.align || 0,
		margin = vars.margin || 0,
		reveal = vars.reveal || 0;

	this.show = function() {
		var tweenVars = {};
		tweenVars[axis] = showAxis();
		Tween.to(obj, 0.35, tweenVars);
		toggle.state = true;
	};

	this.hide = function() {
		var tweenVars = {};
		tweenVars[axis] = hideAxis();
		Tween.to(obj, 0.35, tweenVars);
		toggle.state = false;
	};

	this.toggle = function() {
		toggle.flip();
	};

	this.toggleState = function() {
		return toggle.state;
	};

	this.resize = function() {
		obj[axis] = toggle.state ? showAxis() : hideAxis();
		obj[pAxis] = perpAxis();
	};

	function showAxis() {
		if(side === 'left' || side === 'top') {
			return margin;
		}
		return mask[dim] - obj[dim] - margin;
	}

	function hideAxis() {
		if(side === 'left' || side === 'top') {
			return - obj[dim] + reveal;
		}
		return mask[dim] - reveal;
	}

	function perpAxis() {
		switch(align) {
			case 'top':
			case 'left':
				return 0;
			case 'right':
			case 'bottom':
				return mask[pDim] - obj[pDim];
		}
		return (mask[pDim] - obj[pDim]) * 0.5;
	}

	var toggle = new Toggle({
		target: this,
		on: this.show,
		off: this.hide
	});

	toggle.state = true;
	Docks.register(this);

	this.resize();
}