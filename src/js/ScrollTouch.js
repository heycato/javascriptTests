function ScrollTouch(scroll) {

	var startPoint = 0,
		startPercent = 0;

	scroll.mask.addEventListener(TOUCH_START, onStart);

	function onStart(e) {
		startPoint = getPoint(e);
		startPercent = scroll.percent;
		scroll.mask.addEventListener(TOUCH_END, onEnd);
		scroll.mask.addEventListener(TOUCH_MOVE, onMove);
	}

	function onMove(e) {
		moveDist = getPoint(e) - startPoint;
		scroll.percent = -(moveDist / (scroll.scrollDist)) + startPercent;
		scroll.percent = scroll.percent < 0 ? 0 : scroll.percent;
		scroll.percent = scroll.percent > 1 ? 1 : scroll.percent;

		scroll.update(0);
	}

	function getPoint(e) {
		if(scroll.axis === 'x') {
			return touchDevice ? event.touches[0].pageX : event.clientX;
		}
		return touchDevice ? event.touches[0].pageY : event.clientY;
	}

	function onEnd(e) {
		scroll.mask.removeEventListener(TOUCH_END, onEnd);
		scroll.mask.removeEventListener(TOUCH_MOVE, onMove);
	}

}