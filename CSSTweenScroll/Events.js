function EventSandbox() {

	function findInArray(needle, haystack) {
		var i = haystack.length;
		while(i--) {
			if(needle.callback === haystack[i].callback && needle.target === haystack[i].target) {
				return true;
			}
		}
		return false;
	}

	return {
		listeners: {},
		addEventListener: function(type, callback) {
			if(!this.listeners[type]) this.listeners[type] = [];
			var newListener = {type:type, callback:callback, target:this};
			if(!findInArray(newListener, this.listeners[type])) {
				this.listeners[type].push(newListener);
			}

		},
		removeEventListener: function(type, callback) {
			if(!this.listeners[type]) {
				throw 'removeEventListener: type not registered';
			}
			if(!callback || typeof callback !== 'function') {
				delete this.listeners[type];
			} else {
				var i = 0,
					l = this.listeners[type].length;
				for(;i < l; i ++) {
					if(this.listeners[type][i] && this.listeners[type][i].callback === callback && this.listeners[type][i].target === this) {
						this.listeners[type].splice(i, 1);
					}
				}
			}
		},
		dispatchEvent: function(type, data) {
			if(this.listeners[type]) {
				for(var i = 0, l = this.listeners[type].length; i < l; i ++) {
					if(this.listeners[type][i] && !this.listeners[type][i].callback) {
						console.error('VALID CALLBACK NOT FOUND ON LISTENER: ', this.listeners[type][i]);
						break;
					}
					if(this.listeners[type][i] && this.listeners[type][i].target === this) {
						this.listeners[type][i].callback.apply(this.listeners[type][i].target, [data]);
					}
				}
			}
		}
	}
}

var globalEvents = new EventSandbox();