function isSection(id) {
	return Number(id) < 10000 && id !== '';
}

function isMediaItem(id) {
	return Number(id) > 9999;
}

function getObjById(obj, id) {
	var i;
	id = Number(id);
	for(i in obj) {
		if(obj[i].id === id) {
			return obj[i];
		}
	}
	return false;
}

function getSectionById(id) {
	return getObjById(SECTIONS_MODEL, Number(id));
}

function getMediaById(id) {
	return getObjById(MEDIA_MODEL, Number(id));
}

function getThumbQuery(width, height, file) {
	var x = RETINA ? 2 : 1;
	return REWRITE_BASE + MEDIA_THUMB + '?w=' + (width * x) + '&h=' + (height * x) + '&f=' + encodeURIComponent(file);
}

function getObjectLength(obj) {
	var length = 0, key;
	for (key in obj) if (obj.hasOwnProperty(key)) length++;
	return length;
}

function csvToArray(value) {
	if(value) {
		if(value === '') {
			return [];
		} else if(typeof value === 'number') {
			return [value];
		} else if(typeof value === 'boolean') {
			return [value];
		} else if(value.toString().indexOf(',') > -1) {
			return value.split(',');
		} else {
			return [value];
		}
	}
	return [];
}

function setTimer(callback, delay, params, scope) {
	scope = scope || arguments.callee.caller || this;
	setTimeout(function() {
		if(params) {
			callback.apply(scope, params);
		} else {
			callback.call(scope);
		}
	}, delay);
}

function uniquePush(arr, item) {
	var i = 0,
		length = arr.length;
	for(;i<length;i++){
		if(arr.indexOf(item) === -1) {
			arr.push(item);
		}
	}
	return arr;
}

function findInArray(needle, haystack) {
	var i = haystack.length;
	while(i--) {
		if(needle === haystack[i]) {
			return true;
		}
	}
	return false;
}

function hasClass(o, cls) {
	return new RegExp('(^|\\s)' + cls + '(\\s|$)').test(o.className);
}

function parseColor(c) {
	var color = [];
	if (c && c.charAt(0) === "#") {
		c = parseInt(c.substr(1), 16);
		color = [(c >> 16) & 255, (c >> 8) & 255, c & 255];
	} else if(c && c.charAt(0) === "r") {
		c = c.replace(/[rgba()]/ig,'').split(',');
		color = [Number(c[0]), Number(c[1]), Number(c[2]), Number(c[3])];
	}
	return color;
}

function getLoaderColor(bgColor) {
	var avg, tint, c;
	c = parseColor(bgColor);
	avg = (c[0] + c[1] + c[2]) * 0.33;
	tint = avg < 128 ? '#FFFFFF' : '#000000';
	return tint;
}

function hexToRgba(hex, alpha) {
	hex = hex.charAt(0) === "#" ? parseInt(hex.substr(1), 16) : hex;
	return 'rgba(' + ((hex >> 16) & 255) + ',' + ((hex >> 8) & 255) + ',' + (hex & 255) + ',' + alpha + ')';
}

function getDocHeight() {
	return Math.max(Math.max(document.body.scrollHeight, document.documentElement.scrollHeight), Math.max(document.body.offsetHeight, document.documentElement.offsetHeight), Math.max(document.body.clientHeight, document.documentElement.clientHeight));
}

function percentToPixels(value, range) {
	if(value.search('%') > -1) {
		value = value.replace('%', '');
		value = (Number(value) * 0.01) * range;
		return Math.round(value);
	} else if(value.search('px') > -1) {
		value = value.replace('px', '');
		return Number(value);
	} else {
		return Number(value);
	}
}

function getExt(file) {
	return file.split('.').pop();
}

function getX(e) {
	if (TOUCH_DEVICE) {
		return e.touches[0].pageX;
	} else {
		return e.clientX;
	}
}

function getY(e) {
	if (TOUCH_DEVICE) {
		return e.touches[0].pageY;
	} else {
		return e.clientY;
	}
}

function hardwareAccel(el) {
	el.style.webkitTransform = 'translateZ(0)';
	el.style.webkitPerspective = '1000';
	el.style.webkitBackfaceVisibility = 'hidden';
}

function cssToJs(str) {
	return str.replace(/(-[a-z])/g, function($1) {
		return $1.replace('-','').toUpperCase();
	});
}

function jsToCss(str) {
	return str.replace(/([A-Z])/g, function($1) {
		return $1.replace($1,'-'+$1.toLowerCase());
	});
}