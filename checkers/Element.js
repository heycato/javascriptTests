function Element(type) {
	var self = (typeof type === 'object' && typeof type.nodeName === 'string') ? type : document.createElement(type ||'div'),
		tweenTimer,
		sysMsg;

	self.style.position = 'absolute';
	self.style.display = 'block';

	self._properties = {};

	self.$ = function(prop, value) {
		var val = PIXELVALUES[prop] ? value + 'px' : value;
		var testProp = prop.charAt(0).toUpperCase() + prop.slice(1);
		self._properties[prop] = value;
		if(typeof self.style[prop] === 'string') {
			self.style[prop] = val;
		} else if(typeof self.style['Webkit' + testProp] === 'string') {
			self.style['Webkit' + testProp] = val;
		}
	};

	self.tween = function(dur, props) {
		self.$('transition', 'all ' + dur + 's');
		for(var i in props) {
			if(!/delay|ease|css|onComplete|onCompleteScope|onCompleteParams|onUpdate|onUpdateScope|onUpdateParams/.test(i)) {
				self.$(i, props[i]);
			}
		}
		setTimeout(complete, dur * 1000);

		clearTimeout(tweenTimer);
		tweenTimer = setTimeout(function() {
			self.$('transition', '');
			if(sysMsg) sysMsg('tweenComplete', {element:self, props:props});
		}, dur * 1000);

		function complete() {
			if(props.onComplete) props.onComplete.apply(props.onCompleteScope || self, props.onCompleteParams || [false]);
		}
	};

	self.setSystemNotifier = function(value) {
		sysMsg = value;
	};

	return self;
}