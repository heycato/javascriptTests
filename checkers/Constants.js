/*
* requires Messenger.js to use MESSENGER constant
*/
var EVENT = {
		TWEEN: { COMPLETE: 'tweenComplete' },
		MOUSE: { CLICK: 'click', UP: 'mouseup', DOWN: 'mousedown', MOVE: 'mousemove', OVER: 'mouseover', OUT: 'mouseout' }
	},
	PIXELVALUES={backgroundPositionX:true,backgroundPositionY:true,backgroundRepeatX:true,backgroundRepeatY:true,baselineShift:true,borderBottomLeftRadius:true,borderBottomRightRadius:true,borderBottomWidth:true,borderImageWidth:true,borderLeftWidth:true,borderRadius:true,borderRightWidth:true,borderSpacing:true,borderTopLeftRadius:true,borderTopRightRadius:true,borderTopWidth:true,borderWidth:true,bottom:true,fontSize:true,height:true,left:true,letterSpacing:true,lineHeight:true,marginBottom:true,marginLeft:true,marginRight:true,marginTop:true,maxHeight:true,maxWidth:true,minHeight:true,minWidth:true,outlineOffset:true,outlineWidth:true,overflowX:true,overflowY:true,paddingBottom:true,paddingLeft:true,paddingRight:true,paddingTop:true,right:true,strokeWidth:true,textIndent:true,textLineThroughWidth:true,textOverlineWidth:true,textUnderlineWidth:true,top:true,width:true,wordSpacing:true},
	MESSENGER = Messenger ? new Messenger() : null,
	endofvarlist;