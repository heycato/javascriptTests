function Stage() {

	var _selectable = false,
		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, KEY_UP, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR];

	var stage = {

		children: [],
		dragElement: {},

		listeners: globalEvents.listeners,
		dispatchEvent: globalEvents.dispatchEvent,
		addEventListener: globalEvents.addEventListener,
		removeEventListener: globalEvents.removeEventListener,

		addChild: function(child) {
			if(!child.element.parentNode) {
				document.body.appendChild(child.element);
			}
			child.parent = this;
			this[child.id] = child;
			this.children.push(child);
			child.dispatchEvent({type:'childAddedToStage'});
		},

		addChildAt: function(child, index) {

		},

		removeChild: function(child) {
			if(child.element.parentNode) {
				document.body.removeChild(child.element);
			}
			delete this[child.id];
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					this.children.splice(i, 1);
				}
			}
		},

		domContentLoaded: function() {
			if(!_selectable) {
				document.body.style.overflow = 'hidden';
			}
			this.dispatchEvent({type:'load', target:this});
		},

		documentReadyListener: function() {
			if(typeof document.onreadystatechange === 'object' || document.readyState === 'loading') {
				document.onreadystatechange = function() {
					if(document.readyState === 'complete') {
						stage.domContentLoaded();
					}
				};
			} else {
				document.addEventListener("DOMContentLoaded", stage.domContentLoaded, false );
			}
		},

		globalToLocal: function (point) {
			return {x:point.x, y:point.y};
		},

		localToGlobal: function (point) {
			return {x:point.x, y:point.y};
		},

		event: function(e) {

			var event = {
				type:e.type,
				target:stage,
				currentTarget:stage,
				x:e.x,
				y:e.y,
				keyCode: e.keyCode,
				pageX:e.pageX,
				pageY:e.pageY,
				screenX:e.screenX,
				screenY:e.screenY,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - stage.stageX,
				mouseY: e.clientY - stage.stageY,
				deltaX: -e.wheelDeltaX || -e.wheelDelta * 2 || e.deltaX || e.detail || 0,
				deltaY: -e.wheelDeltaY || -e.wheelDelta * 2 || e.deltaY || e.detail || 0,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.touches) event.touches = e.touches;

			if(e.type === "wheel" || e.type === "MozMousePixelScroll") {
				event.type = MOUSE_WHEEL;
			}

			if(e.type === RESIZE || e.type === GESTURE_CHANGE || e.type === GESTURE_END || e.type === ORIENTATION_CHANGE) {
				_zoom = document.documentElement.clientWidth / window.innerWidth;
			}

			stage.dispatchEvent(event);
		},

		get getChildren() {
			return children;
		},
		get images() {
			return document.images;
		},
		get forms() {
			return document.forms;
		},
		get links() {
			return document.links;
		},
		get anchors() {
			return document.anchors;
		},
		get scripts() {
			return document.scripts;
		},
		get plugins() {
			return document.plugins;
		},
		get embeds() {
			return document.embeds;
		},
		get width() {
			return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		},
		get height() {
			return window.innerHeight || document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
		},
		get stageX() {
			return 0;
		},
		get stageY() {
			return 0;
		},
		get selectable() {
			return _selectable;
		},
		set selectable(value) {
			_selectable = value;
		}
	};

	stage.proxy = stage;

	window.addEventListener(TOUCH_START, stage.event, false);
	window.addEventListener(TOUCH_MOVE, stage.event, false);
	window.addEventListener(TOUCH_END, stage.event, false);
	window.addEventListener(TOUCH_CANCEL, stage.event, false);
	window.addEventListener(MOUSE_OVER, stage.event, false);
	window.addEventListener(MOUSE_DOWN, stage.event, false);
	window.addEventListener(MOUSE_MOVE, stage.event, false);
	window.addEventListener(MOUSE_UP, stage.event, false);
	window.addEventListener(MOUSE_OUT, stage.event, false);
	window.addEventListener(CLICK, stage.event, false);
	window.addEventListener(RESIZE, stage.event, false);
	window.addEventListener(ORIENTATION_CHANGE, stage.event, false);
	window.addEventListener(SCROLL, stage.event, false);
	window.addEventListener(GESTURE_START, stage.event, false);
	window.addEventListener(GESTURE_CHANGE, stage.event, false);
	window.addEventListener(GESTURE_END, stage.event, false);
	window.addEventListener(KEY_UP, stage.event, false);
	window.addEventListener(KEY_DOWN, stage.event, false);

	var resizeTimer;

	window.addEventListener('resize', function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			stage.dispatchEvent({type:RESIZE_END, data:e});
		}, 200);
	}, false);

	if("onmousewheel" in document) {
		window.addEventListener(MOUSE_WHEEL, stage.event, false);
	} else if("onwheel" in document) {
		window.addEventListener("wheel", stage.event, false);
	} else if("MozMousePixelScroll" in document) {
		window.addEventListener("MozMousePixelScroll", stage.event, false);
	}

	stage.documentReadyListener();

	return stage;
}

var stage = Stage.call(Stage);

function addChild(child) {
	stage.addChild(child);
}
