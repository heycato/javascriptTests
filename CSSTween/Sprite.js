function Sprite(vars) {

	var proxy = vars || {};
	proxy.events = proxy.events || globalEvents;

	var _xOffset, _yOffset, _dragRect;

	function drag(event){
		var obj = {x:event.clientX, y:event.clientY};
		var point = proxy.parent.globalToLocal(obj);
		if(_dragRect){
			var xMax = _dragRect.x + _dragRect.width;
			var yMax = _dragRect.y + _dragRect.height;
			if(point.y - _yOffset >= yMax) proxy.y = yMax;
			if(point.y <= _dragRect.y) proxy.y = _dragRect.y;
			if(point.x - _xOffset >= xMax) proxy.x = xMax;
			if(point.x <= _dragRect.x) proxy.x = _dragRect.x;
			if(point.x >= _dragRect.x + _xOffset && point.x <= xMax + _xOffset) proxy.x = point.x - _xOffset;
			if(point.y >= _dragRect.y + _yOffset && point.y <= yMax + _yOffset) proxy.y = point.y - _yOffset;
		}else{
			proxy.x = point.x - _xOffset;
			proxy.y = point.y - _yOffset;
		}
	}

	var sprite;

	if(proxy.element) {
		sprite = proxy.element;
	} else if(proxy.type === 'svg' || proxy.type === 'g' || proxy.type === 'circle' || proxy.type === 'ellipse' || proxy.type === 'line' || proxy.type === 'path' || proxy.type === 'polygon' || proxy.type === 'polyline' || proxy.type === 'rect') {
		sprite = document.createElementNS("http://www.w3.org/2000/svg", proxy.type);
	} else {
		sprite = document.createElement(proxy.type || 'div');
	}

	var style = sprite.style,
		_elementEvents = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, KEY_UP, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, LOAD, ERROR, FOCUS, BLUR],
		_alpha = proxy.alpha || 1,
		_display = proxy.display || 'block',
		_position = proxy.position || 'absolute',
		_x = proxy.left || proxy.x || 0,
		_y = proxy.top || proxy.y || 0,
		_width = proxy.width || 0,
		_height = proxy.height || 0,
		_src,
		_right,
		_bottom,
		_zIndex = 0,
		_cursor,
		_shadow,
		_visibility,
		_overflow,
		_background,
		_bgColor,
		_border,
		_borderTop,
		_borderRight,
		_borderBottom,
		_borderLeft,
		_borderColor,
		_borderRadius,
		_font,
		_fontColor,
		_fontSize,
		_fontFamily,
		_fontWeight,
		_text,
		_textAlign,
		_textDecoration,
		_textTransform,
		_letterSpacing,
		_lineHeight,
		_list,
		_margin,
		_marginTop,
		_marginRight,
		_marginBottom,
		_marginLeft,
		_padding,
		_paddingTop,
		_paddingRight,
		_paddingBottom,
		_paddingLeft,
		_parent,
		_selectable = proxy.selectable || false,
		_rotate,
		_cx,
		_cy,
		_x1,
		_y1,
		_x2,
		_y2,
		_r,
		_rx,
		_ry,
		_fill,
		_stroke,
		_strokeWidth;

	sprite.id = proxy.id || '';

	style.opacity = proxy.alpha || 1;
	style.display = proxy.display || 'block';
	style.position =  proxy.position || 'absolute';
	style['text-rendering'] = 'optimizelegibility';

	if(proxy.border) {
		style.border = proxy.border;
	}

	var template = {

		element: sprite,

		children: [],
		initQue: [],

		listeners: proxy.events.listeners,
		dispatchEvent: proxy.events.dispatchEvent,
		addEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					sprite.addEventListener(type, this.event, false);
				}
			}
			proxy.events.addEventListener.apply(this, [type, callback, vars]);
		},
		removeEventListener: function(type, callback, vars) {
			var l = _elementEvents.length;
			while(l--) {
				if(type === _elementEvents[l]) {
					sprite.removeEventListener(type, this.event, false);
				}
			}
			proxy.events.removeEventListener.apply(this, [type, callback, vars]);
		},

		addChild: function(child) {
			if(!sprite[child.element]) {
				sprite.appendChild(child.element);
			}
			child.parent = this;
			this[child.id] = child;
			this.children.push(child);
			child.dispatchEvent({type:'childAdded'});
		},

		removeChild: function(child) {
			if(child.element.parentNode) {
				sprite.removeChild(child.element);
			}
			delete this[child.id];
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					child = undefined;
					this.children.splice(i, 1);
				}
			}
		},

		getClass: function() {
			return sprite.className;
		},

		setClass: function(value) {
			sprite.setAttribute('class', value);
		},

		hasClass: function(cls) {
			return new RegExp('(^|\\s)' + cls + '(\\s|$)').test(sprite.className);
		},

		event: function(e) {
			if(!_selectable && !TOUCH_DEVICE) {
				e.preventDefault();
			}
			if(TOUCH_DEVICE && e.touches && e.touches.length === 1 && stage.zoom <= 1) {
				e.preventDefault();
			}
			if(e.target.width && !proxy.width) {
				template.width = e.target.width;
			}
			if(e.target.height && !proxy.height) {
				template.height = e.target.height;
			}
			var event = {
				type: e.type,
				target:template,
				currentTarget:template,
				keyCode: e.keyCode,
				pageX:e.pageX,
				pageY:e.pageY,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - template.stageX,
				mouseY: e.clientY - template.stageY,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.touches) event.touches = e.touches;
			template.dispatchEvent(event);
		},

		hitTestPoint: function(xPoint, yPoint) {
			return style.display !== 'none' && xPoint >= this.stageX && xPoint <= this.stageX + _width && yPoint >= this.stageY && yPoint <= this.stageY + _height;
		},

		hitTestAbs: function(xPoint, yPoint) {
			return style.display !== 'none' && xPoint >= this.absX && xPoint <= this.absX + _width && yPoint >= this.absY && yPoint <= this.absY + _height;
		},

		globalToLocal: function (point) {
			return {x:point.x - this.stageX, y:point.y - this.stageY};
		},

		localToGlobal: function (point) {
			return {x:point.x + this.stageX, y:point.y + this.stageY};
		},

		startDrag: function (offsets, rectangle) {
			_xOffset = offsets.x;
			_yOffset = offsets.y;
			_dragRect = rectangle;
			stage.addEventListener(MOUSE_MOVE, drag);
		},

		stopDrag: function () {
			stage.removeEventListener(MOUSE_MOVE, drag);
		},

		/* general */

		get id() {
			return sprite.id;
		},
		set id(value) {
			sprite.id = value;
		},

		get src() {
			return _src;
		},
		set src(value) {
			_src = value;
			sprite.src = value;
		},

		get text() {
			return _text;
		},
		set text(value) {
			_text = value;
			sprite.innerHTML = value;
		},

		get innerHTML() {
			return _text;
		},
		set innerHTML(value) {
			_text = value;
			sprite.innerHTML = value;
		},

		get alpha() {
			return _alpha;
		},
		set alpha(value) {
			_alpha = value;
			style.opacity = value;
		},

		get opacity() {
			return _alpha;
		},
		set opacity(value) {
			_alpha = value;
			style.opacity = value;
		},

		get display() {
			return _display;
		},
		set display(value) {
			_display = value;
			style.display = value;
		},

		get position() {
			return _position;
		},
		set position(value) {
			_position = value;
			style.position = value;
		},

		get top() {
			return _y;
		},
		set top(value) {
			_y = value;
			style.top = value + 'px';
		},

		get x() {
			return _x;
		},
		set x(value) {
			_x = value;
			if(proxy.type === 'rect') {
				sprite.setAttribute('x', value);
			} else {
				style.left = value + 'px';
			}
		},

		get y() {
			return _y;
		},
		set y(value) {
			_y = value;
			if(proxy.type === 'rect') {
				sprite.setAttribute('y', value);
			} else {
				style.top = value + 'px';
			}
		},

		get stageX() {
			if(this.parent) {
				return this.parent.stageX + this.x;
			}
			return this.x;
		},

		get stageY() {
			if(this.parent) {
				return this.parent.stageY + this.y;
			}
			return this.y;
		},

		get absX() {
			var x = 0;
			var el = sprite;
			if (el.offsetParent) {
				do { x += el.offsetLeft; } while (el = el.offsetParent);
			}
			return x;
		},

		get absY() {
			var y = 0;
			var el = sprite;
			if (el.offsetParent) {
				do { y += el.offsetTop; } while (el = el.offsetParent);
			}
			return y;
		},

		get right() {
			return _right;
		},
		set right(value) {
			_right = value;
			style.right = value + 'px';
		},

		get bottom() {
			return _bottom;
		},
		set bottom(value) {
			_bottom = value;
			style.bottom = value + 'px';
		},

		get left() {
			return _x;
		},
		set left(value) {
			_x = value;
			style.left = value + 'px';
		},

		get width() {
			_width = sprite.offsetWidth || _width;
			return _width;
		},
		set width(value) {
			_width = value;
			if(proxy.type === 'svg' || proxy.type === 'rect') {
				sprite.setAttribute('width', value);
			} else {
				style.width = value + 'px';
			}
		},

		get height() {
			_height = sprite.offsetHeight || _height;
			return _height;
		},
		set height(value) {
			_height = value;
			if(proxy.type === 'svg' || proxy.type === 'rect') {
				sprite.setAttribute('height', value);
			} else {
				style.height = value + 'px';
			}
		},

		get zIndex() {
			return _zIndex;
		},
		set zIndex(value) {
			_zIndex = value;
			style.zIndex = value;
		},

		get cursor() {
			return _cursor;
		},
		set cursor(value) {
			_cursor = value;
			style.cursor = value;
		},

		get shadow() {
			return _shadow;
		},
		set shadow(value) {
			_shadow = value;
			style.boxShadow = value;
		},

		get visibility() {
			return _visibility;
		},
		set visibility(value) {
			_visibility = value;
			style.visibility = value;
		},

		get overflow() {
			return _overflow;
		},
		set overflow(value) {
			_overflow = value;
			style.overflow = value;
		},


		/* background */

		get background() {
			return _background;
		},
		set background(value) {
			_background = value;
			style.background = value;
		},


		get backgroundColor() {
			return _bgColor;
		},
		set backgroundColor(value) {
			_bgColor = value;
			style.backgroundColor = value;
		},

		get parent(){
			return _parent;
		},
		set parent(value){
			_parent = value;
		},

		/* border */

		get border() {
			return _border;
		},
		set border(value) {
			_border = value;
			style.border = value;
		},
		get borderTop() {
			return _borderTop;
		},
		set borderTop(value) {
			_borderTop = value;
			style.borderTop = value;
		},
		get borderRight() {
			return _borderRight;
		},
		set borderRight(value) {
			_borderRight = value;
			style.borderRight = value;
		},
		get borderBottom() {
			return _borderBottom;
		},
		set borderBottom(value) {
			_borderBottom = value;
			style.borderBottom = value;
		},
		get borderLeft() {
			return _borderLeft;
		},
		set borderLeft(value) {
			_borderLeft = value;
			style.borderLeft = value;
		},

		get borderColor() {
			return _borderColor;
		},
		set borderColor(value) {
			_borderColor = value;
			style.borderColor = value;
		},

		get borderRadius() {
			return _borderRadius;
		},
		set borderRadius(value) {
			_borderRadius = value;
			style.borderRadius = value + 'px';
		},


		/* font */

		get font() {
			return _font;
		},
		set font(value) {
			_font = value;
			style.font = value;
		},

		get fontColor() {
			return _fontColor;
		},
		set fontColor(value) {
			_fontColor = value;
			style.color = value;
		},

		get fontSize() {
			return _fontSize;
		},
		set fontSize(value) {
			_fontSize = value;
			style.fontSize = value + 'px';
		},

		get fontFamily() {
			return _fontFamily;
		},
		set fontFamily(value) {
			_fontFamily = value;
			style.fontFamily = value;
		},

		get fontWeight() {
			return _fontWeight;
		},
		set fontWeight(value) {
			_fontWeight = value;
			style.fontWeight = value;
		},

		get textAlign() {
			return _textAlign;
		},
		set textAlign(value) {
			_textAlign = value;
			style.textAlign = value;
		},

		get textDecoration() {
			return _textDecoration;
		},
		set textDecoration(value) {
			_textDecoration = value;
			style.textDecoration = value;
		},

		get textTransform() {
			return _textTransform;
		},
		set textTransform(value) {
			_textTransform = value;
			style.textTransform = value;
		},

		get letterSpacing() {
			return _letterSpacing;
		},
		set letterSpacing(value) {
			_letterSpacing = value;
			style.letterSpacing = value;
		},

		get lineHeight() {
			return _lineHeight;
		},
		set lineHeight(value) {
			_lineHeight = value;
			style.lineHeight = value;
		},

		get list() {
			return _list;
		},
		set list(value) {
			_list = value;
			style.list = value;
		},


		/* margin */

		get margin() {
			return _margin;
		},
		set margin(value) {
			_margin = value;
			style.margin = value + 'px';
		},

		get marginTop() {
			return _marginTop;
		},
		set marginTop(value) {
			_marginTop = value;
			style.marginTop = value + 'px';
		},

		get marginRight() {
			return _marginRight;
		},
		set marginRight(value) {
			_marginRight = value;
			style.marginRight = value + 'px';
		},

		get marginBottom() {
			return _marginBottom;
		},
		set marginBottom(value) {
			_marginBottom = value;
			style.marginBottom = value + 'px';
		},

		get marginLeft() {
			return _marginLeft;
		},
		set marginLeft(value) {
			_marginLeft = value;
			style.marginLeft = value + 'px';
		},


		/* padding */

		get padding() {
			return _padding;
		},
		set padding(value) {
			_padding = value;
			style.padding = value + 'px';
		},

		get paddingTop() {
			return _paddingTop;
		},
		set paddingTop(value) {
			_paddingTop = value;
			style.paddingTop = value + 'px';
		},

		get paddingRight() {
			return _paddingRight;
		},
		set paddingRight(value) {
			_paddingRight = value;
			style.paddingRight = value + 'px';
		},

		get paddingBottom() {
			return _paddingBottom;
		},
		set paddingBottom(value) {
			_paddingBottom = value;
			style.paddingBottom = value + 'px';
		},

		get paddingLeft() {
			return _paddingLeft;
		},
		set paddingLeft(value) {
			_paddingLeft = value;
			style.paddingLeft = value + 'px';
		},
		get selectable() {
			return _selectable;
		},
		set selectable(value) {
			_selectable = value;
			if(!_selectable) {
				style.cursor = 'default';
				style['-webkit-touch-callout'] = 'none';
				style['-webkit-user-select'] = 'none';
				style['-khtml-user-select'] = 'none';
				style['-moz-user-select'] = 'none';
				style['-ms-touch-callout'] = 'none';
				style['user-select'] = 'none';
			} else {
				style.cursor = 'auto';
				style['-webkit-touch-callout'] = 'auto';
				style['-webkit-user-select'] = 'auto';
				style['-khtml-user-select'] = 'auto';
				style['-moz-user-select'] = 'auto';
				style['-ms-touch-callout'] = 'auto';
				style['user-select'] = 'auto';
			}
		},

		get rotate() {
			return _rotate;
		},
		set rotate(value) {
			_rotate = value;
			style['-webkit-transform'] = 'rotate('+value+'deg)';
			style['-moz-transform'] = 'rotate('+value+'deg)';
			style['-o-transform'] = 'rotate('+value+'deg)';
			style.transform = 'rotate('+value+'deg)';
		},

		get textWrap() {
			return _textWrap;
		},
		set textWrap(value) {
			_textWrap = value;
			style['white-space'] = value ? 'normal' : 'nowrap';
		},

		/*svg support*/

		get d() {
			return sprite.getAttribute('d');
		},
		set d(value) {
			sprite.setAttribute('d', value);
		},

		get points() {
			return sprite.getAttribute('points');
		},
		set points(value) {
			sprite.setAttribute('points', value);
		},

		get cx() {
			return _cx;
		},
		set cx(value) {
			_cx = value;
			sprite.setAttribute('cx', value);
		},

		get cy() {
			return _cy;
		},
		set cy(value) {
			_cy = value;
			sprite.setAttribute('cy', value);
		},

		get x1() {
			return _x1;
		},
		set x1(value) {
			_x1 = value;
			sprite.setAttribute('x1', value);
		},

		get y1() {
			return _y1;
		},
		set y1(value) {
			_y1 = value;
			sprite.setAttribute('y1', value);
		},

		get x2() {
			return _x2;
		},
		set x2(value) {
			_x2 = value;
			sprite.setAttribute('x2', value);
		},

		get y2() {
			return _y2;
		},
		set y2(value) {
			_y2 = value;
			sprite.setAttribute('y2', value);
		},

		get r() {
			return _r;
		},
		set r(value) {
			_r = value;
			sprite.setAttribute('r', value);
		},

		get rx() {
			return _rx;
		},
		set rx(value) {
			_rx = value;
			sprite.setAttribute('rx', value);
		},

		get ry() {
			return _ry;
		},
		set ry(value) {
			_ry = value;
			sprite.setAttribute('ry', value);
		},

		get fill() {
			return _fill;
		},
		set fill(value) {
			_fill = value;
			sprite.setAttribute('fill', value);
		},

		get stroke() {
			return _stroke;
		},
		set stroke(value) {
			_stroke = value;
			sprite.setAttribute('stroke', value);
		},

		get strokeWidth() {
			return _strokeWidth;
		},
		set strokeWidth(value) {
			_strokeWidth = value;
			sprite.setAttribute('stroke-width', value);
		}
	};

	if(!_selectable) {
		template.selectable = false;
	}

	if(proxy.id) {
		template.id = proxy.id;
	}

	if(proxy.collection) {
		template.setClass(proxy.collection);
	}

	if(!proxy.fontFamily) {
		template.fontFamily = 'Helvetica, Arial, sans-serif';
	}

	if(!proxy.fontWeight) {
		template.fontWeight = 'normal';
	}

	if(!proxy.fontSize) {
		template.fontSize = 14;
	}

	sprite.proxy = template;
	template.style = sprite.style;

	var i; for(i in proxy) {
		template[i] = proxy[i];
	}

	return template;

}