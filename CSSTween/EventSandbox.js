function EventSandbox() {

	this.listeners = [];

	this.addEventListener = function(type, callback, vars) {
		vars = vars || {};
		vars.target = vars.target || this;
		this.listeners.push({type:type, callback:callback, vars:vars});
	};

	this.removeEventListener = function(type, callback) {
		var i = this.listeners.length;
		while(i--) {
			if(this.listeners[i].type === type && this.listeners[i].callback === callback) {
				this.listeners.splice(i, 1);
			}
		}
	};

	this.dispatchEvent = function(event) {
		event.target = event.target || this;
		var i = this.listeners.length;
		while(i--) {
			if(this.listeners[i].type === event.type && (event.target === this.listeners[i].vars.proxy || event.target === this.listeners[i].vars.target)) {
				this.listeners[i].callback.apply(this.listeners[i].vars.target, [event]);
			}
		}
	};
}

var globalEvents = new EventSandbox();
