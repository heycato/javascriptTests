function hsv2rgb(h, s, v) {

	h /= 60;
	s *= 0.01;
	v *= 0.01;
	
	var i = Math.floor(h);
	var f = h - i;
	var m = v * (1 - s);
	var n = v * (1 - s * f);
	var k = v * (1 - s * (1 - f));
	var rgb;

	switch (i) {
		case 0:
			rgb = [v, k, m];
			break;
		case 1:
			rgb = [n, v, m];
			break;
		case 2:
			rgb = [m, v, k];
			break;
		case 3:
			rgb = [m, n, v];
			break;
		case 4:
			rgb = [k, m, v];
			break;
		case 5:
		case 6:
			rgb = [v, m, n];
		break;
	}
	
	return {
		r: rgb[0] * 255 |0,
		g: rgb[1] * 255 |0,
		b: rgb[2] * 255 |0
	}
}

function drawPalette(canvas, s, v) {
		
	var bmp, data, rgb, x, y, l, p, m, mm, c, f1, f2, wm,
		ctx = canvas.context, w = canvas.width, h = canvas.height, md = canvas.middle, //localize
		c0, c1, c2, c3, c4, c5, c6;

	c0 = hsv2rgb(0, 100, 100);
	c1 = hsv2rgb(60, 100, 100);
	c2 = hsv2rgb(120, 100, 100);
	c3 = hsv2rgb(180, 100, 100);
	c4 = hsv2rgb(240, 100, 100);
	c5 = hsv2rgb(300, 100, 100);

	// make horizontal gradient
	console.log('drawPalette w, h:', w, h);
	var grd = ctx.createLinearGradient(0, 0, w, 0);

	grd.addColorStop(0, 	 'rgb(' + c0.r + ',' + c0.g + ',' + c0.b + ')');
	grd.addColorStop(0.1667, 'rgb(' + c1.r + ',' + c1.g + ',' + c1.b + ')');
	grd.addColorStop(0.3333, 'rgb(' + c2.r + ',' + c2.g + ',' + c2.b + ')');
	grd.addColorStop(0.5, 	 'rgb(' + c3.r + ',' + c3.g + ',' + c3.b + ')');
	grd.addColorStop(0.6667, 'rgb(' + c4.r + ',' + c4.g + ',' + c4.b + ')');
	grd.addColorStop(0.8333, 'rgb(' + c5.r + ',' + c5.g + ',' + c5.b + ')');
	grd.addColorStop(1, 	 'rgb(' + c0.r + ',' + c0.g + ',' + c0.b + ')');

	ctx.fillStyle = grd;
	ctx.fillRect(0, 0, w, (h * 2));

	//make vertical white-to-color and color-to-black part
	/*bmp = ctx.getImageData(0, 0, w, h);
	data = bmp.data;

	mm = 255 / md;
	m = mm / 255;
	wm = w * 4;

	for (y = 0; y < md; y++) {
		
		f1 = y * m;
		f2 = (md - y) * mm;
		l = y * wm;
		
		for (x = 0; x < wm; x += 4) {
			p = l + x;

			data[p]		= f2 + data[p] * f1;
			data[p + 1] = f2 + data[p + 1] * f1;
			data[p + 2] = f2 + data[p + 2] * f1;
		}
	}

	for (y = md; y < h; y++) {
		
		f1 = (h - y) * m;
		l = y * wm;
		
		for (x = 0; x < wm; x += 4) {
			p = l + x;

			data[p]		= data[p] * f1;
			data[p + 1] = data[p + 1] * f1;
			data[p + 2] = data[p + 2] * f1;
		}
	}

	ctx.putImageData(bmp, 0, 0);*/
};