var canvasPool = [];

function newCanvas(vars) {
	/*trace(canvasPool.length + ' in the Canvas pool');*/
	if(canvasPool.length > 0) {
		var canvas = canvasPool.splice(0, 1)[0];
		var i; for(i in vars) {
			canvas[i] = vars[i];
		}
		/*trace('getting Canvas from the pool');*/
		return canvas;
	}
	/*trace('making Canvas');*/
	return new Canvas(vars);
}

function Canvas(vars) {
	vars = vars || {};
	vars.type = 'canvas';
	vars.className = 'Canvas';
	var canvas = new Sprite(vars);
	canvas.context = canvas.element.getContext('2d');
	return canvas;
}