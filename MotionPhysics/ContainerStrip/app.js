//BASED ON TUTORIAL FROM http://burakkanber.com/blog/modeling-physics-javascript-gravity-and-drag/

//Hub configuration

var utilDetail = new Hub._constructors.Detail(),
	appDetail = new Hub._constructors.Detail();

utilDetail.register('Sprite', function(c, vars) {
	return new Sprite(vars);
});
utilDetail.register('Event', function(c) {
	return Event;
}, true);
utilDetail.register('Mth', function(c) {
	return Mth;
}, true);
utilDetail.register('Physics', function(c) {
	return Physics;
}, true);

appDetail.register('init', function(c, vars) {
	return App({util: Hub.UTIL});
}, true);

utilDetail.make('UTIL');
appDetail.make('APP');

Hub.APP.init();

function App(imports) {
	var math = imports.util.Mth(),
		phys = imports.util.Physics(),
		event = imports.util.Event(),
		sprite = imports.util.Sprite,

		frameRate = 1 / 40,
		frameDelay = frameRate * 1000,
		loopTimer = false,
		startTimer = 0,
		endTimer = 0;

	var item = sprite();
	item.mass = 10;
	item.velocityX = 10;
	item.restitution = -0.1;
	item.width = 100;
	item.height = 70;
	item.x = 0;
	item.y = -1;
	item.border = '1px solid #333333';
	item.backgroundColor = '#888888';

	var strip = sprite();
	strip.µ = phys.FR.GLASS;
	strip.bounds = 2;
	strip.width = stage.width - strip.bounds * 2;
	strip.height = item.height;
	strip.x = 0;
	strip.y = 10;
	strip.border = '2px solid #000000';
	strip.backgroundColor = '#CCCCCC';

	var	Cd = phys.CD.ANGLED_CUBE, // coefficient of drag on a ball - dimensionless
		rho = phys.RHO.DRY_AIR, // fluid density - 0.0013 = "air" @ kg/m^3
		A = item.width * item.height / (10000), // frontal surface area of a ball (circle)
		ag = phys.GRAVITY, // acceleration of gravity on earth m/s^2
		mouse = {};

	function animate() {
		loopTimer = setInterval(loop, frameDelay);
	}

	function loop() {
		if(!mouse.isDown) {
			//var Fx = -0.5 * Cd * A * rho * item.velocityX * item.velocityX * item.velocityX / math.abs(item.velocityX);
			var Fx = -0.5 * Cd * A * strip.µ * item.velocityX * item.velocityX * item.velocityX / math.abs(item.velocityX);

			Fx = (isNaN(Fx) ? 0 : Fx);

			var ax = Fx / item.mass;

			item.velocityX += ax * frameRate;

			item.x += item.velocityX * frameRate * 100;

			if (item.x > stage.width - item.width) {
				// right wall
				item.velocityX *= item.restitution;
				item.x = stage.width - item.width;
			} else if(item.x < 0) {
				// left wall
				item.velocityX *= item.restitution;
				item.x = 0;
			} else {
				item.velocityX *= 0.99386875;
			}
		}
	}

	function mouseDown(e) {
		e.preventDefault();
		startTimer = Date.now();
		mouse.isDown = true;
		item.x = e.pageX - item.width * 0.5;
		item.x1 = item.x;
		stage.addEventListener(MOUSE_MOVE, mouseMove);
		stage.addEventListener(TOUCH_MOVE, mouseMove);
	}

	function mouseUp(e) {
		e.preventDefault();
		endTimer = Date.now();
		mouse.isDown = false;
		var xDist = e.pageX - item.x1,
			timeDelta = endTimer - startTimer,
			a = phys.acceleration(phys.velocity(timeDelta, xDist) , 0, timeDelta),
			F = phys.force(a, item.mass);
		item.velocityX = phys.velocity(timeDelta, F, item.mass);
		stage.removeEventListener(MOUSE_MOVE, mouseMove);
		stage.removeEventListener(TOUCH_MOVE, mouseMove);
	}

	function mouseMove(e) {
		e.preventDefault();
		item.x = e.pageX - item.width * 0.5;
	}

	stage.addEventListener(MOUSE_DOWN, mouseDown);
	stage.addEventListener(MOUSE_UP, mouseUp);
	stage.addEventListener(TOUCH_START, mouseDown);
	stage.addEventListener(TOUCH_END, mouseUp);

	strip.addChild(item);
	stage.addChild(strip);

	animate();
}
