<?php
$scriptPath = '';
$directories = glob('*' , GLOB_ONLYDIR);
echo "<!DOCTYPE html>\n";
echo "<html>\n";
echo "<head>\n";
echo "<meta charset='utf-8'>\n";
echo "<meta name=viewport content=width=device-width>\n";
echo "<link rel=stylesheet href=../../dir.css>\n";
echo "<title>CatoHouseDev Server</title>\n";
echo "</head>\n";
echo "<body>\n";
echo "<h1>Directories</h1>\n";
if(is_array($directories)) {
	$d = count($directories);
	for($i = 0; $i < $d; $i++) {
		echo "<a href='".$directories[$i]."'>".$directories[$i]."</a></br>";
	};
} else {
	echo "no directories found";
};
echo "<script src=".$scriptPath." ></script>\n";
echo "</body>\n";
echo "</html>\n";
