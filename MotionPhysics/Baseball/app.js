//BASED ON TUTORIAL FROM http://burakkanber.com/blog/modeling-physics-javascript-gravity-and-drag/

//Hub configuration

var utilDetail = new Hub._constructors.Detail(),
	appDetail = new Hub._constructors.Detail();

utilDetail.register('Sprite', function(c, vars) {
	return new Sprite(vars);
});
utilDetail.register('Event', function(c) {
	return Event;
}, true);
utilDetail.register('Mth', function(c) {
	return Mth;
}, true);
utilDetail.register('Physics', function(c) {
	return Physics;
}, true);

appDetail.register('init', function(c, vars) {
	return App({util: Hub.UTIL});
}, true);

utilDetail.make('UTIL');
appDetail.make('APP');

Hub.APP.init();

function App(imports) {
	var math = imports.util.Mth(),
		phys = imports.util.Physics(),
		event = imports.util.Event(),
		sprite = imports.util.Sprite,

		frameRate = 1 / 40,
		frameDelay = frameRate * 1000,
		loopTimer = false,
		startTimer = 0,
		endTimer = 0;

	var baseball = sprite();
	baseball.radius = 5;
	baseball.mass = 0.147;
	baseball.velocity = {x:10, y:0};
	baseball.restitution = -0.2;
	baseball.width = baseball.radius * 2;
	baseball.height = baseball.radius * 2;
	baseball.x = 0;
	baseball.y = 0;
	baseball.borderRadius = baseball.radius;
	baseball.backgroundColor = '#000000';

	var	µ = 0.001,	// surface friction coefficient - dimensionless
		Cd = phys.CD.SPHERE, // coefficient of drag on a ball - dimensionless
		rho = phys.RHO.DRY_AIR, // fluid density - 0.0013 = "air" @ kg/m^3
		A = math.PI * math.pow(baseball.radius, 2) / (10000), // frontal surface area of a ball (circle)
		ag = phys.GRAVITY, // acceleration of gravity on earth m/s^2
		mouse = {};

	function animate() {
		loopTimer = setInterval(loop, frameDelay);
	}

	function loop() {
		if(!mouse.isDown) {
			var Fx = -0.5 * Cd * A * rho * baseball.velocity.x * baseball.velocity.x * baseball.velocity.x / math.abs(baseball.velocity.x),
				Fy = -0.5 * Cd * A * rho * baseball.velocity.y * baseball.velocity.y * baseball.velocity.y / math.abs(baseball.velocity.y);

			Fx = (isNaN(Fx) ? 0 : Fx);
			Fy = (isNaN(Fy) ? 0 : Fy);

			var ax = Fx / baseball.mass,
				ay = ag + (Fy / baseball.mass);

			baseball.velocity.x += ax * frameRate;
			baseball.velocity.y += ay * frameRate;

			baseball.x += baseball.velocity.x * frameRate * 100;
			baseball.y += baseball.velocity.y * frameRate * 100;

			if (baseball.y > stage.height - baseball.width) {
				// floor
				baseball.velocity.y *= baseball.restitution;
				baseball.y = stage.height - baseball.width;
			}
			if (baseball.y < 0) {
				// ceiling
				baseball.velocity.y *= baseball.restitution;
				baseball.y = 0;
			}
			if (baseball.x > stage.width - baseball.width) {
				// right wall
				baseball.velocity.x *= baseball.restitution;
				baseball.x = stage.width - baseball.width;
			}
			if (baseball.x < 0) {
				// left wall
				baseball.velocity.x *= baseball.restitution;
				baseball.x = 0;
			}
			if (baseball.y === stage.height - baseball.height) {
				// floor friction
				baseball.velocity.x *= 0.99;
			}
		}
	}

	function mouseDown(e) {
		e.preventDefault();
		startTimer = Date.now();
		mouse.isDown = true;
		baseball.x = e.pageX - baseball.radius;
		baseball.y = e.pageY - baseball.radius;
		baseball.x1 = baseball.x;
		baseball.y1 = baseball.y;
		stage.addEventListener(MOUSE_MOVE, mouseMove);
		stage.addEventListener(TOUCH_MOVE, mouseMove);
	}

	function mouseUp(e) {
		e.preventDefault();
		endTimer = Date.now()
		mouse.isDown = false;
		var xDist = e.pageX - baseball.x1,
			yDist = e.pageY - baseball.y1,
			timeDelta = endTimer - startTimer,
			a = phys.acceleration(phys.velocity(timeDelta, xDist) , 0, timeDelta),
			F = phys.force(a, baseball.mass);
		baseball.velocity.x = phys.velocity(timeDelta, xDist) * frameRate;
		baseball.velocity.y = phys.velocity(timeDelta, yDist) * frameRate;
		stage.removeEventListener(MOUSE_MOVE, mouseMove);
		stage.removeEventListener(TOUCH_MOVE, mouseMove);
	}

	function mouseMove(e) {
		e.preventDefault();
		baseball.x = e.pageX - baseball.radius;
		baseball.y = e.pageY - baseball.radius;
	}

	stage.addEventListener(MOUSE_DOWN, mouseDown);
	stage.addEventListener(MOUSE_UP, mouseUp);
	stage.addEventListener(TOUCH_START, mouseDown);
	stage.addEventListener(TOUCH_END, mouseUp);
	stage.addChild(baseball);

	animate();
}
