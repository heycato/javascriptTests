function Tile(vars) {

	vars = vars || {};

	var items = [],
		colCount = 0,
		currentX = 0,
		currentY = 0,
		columns = 0;

	this.axis = vars.axis || 'y';
	this.perpLength = vars.perpLength || 300;
	this.align = vars.align || 'left';
	this.wrap = vars.wrap || true;
	this.gap = vars.gap || 0;
	this.width = 0;
	this.height = 0;

	this.incrHorizontal = function(i) {
		var height = items[i].height,
			width = items[i].width;
		if(this.wrap) {
			if(currentY + height < this.perpLength - height - this.gap) {
				currentY += height + this.gap;
				colCount++;
			} else {
				currentY = 0;
				currentX += width + this.gap;
			}
		} else {
			currentX += width + this.gap;
		}
	};

	this.incrVertical = function(i) {
		var height = items[i].height,
			width = items[i].width;
		if(this.wrap) {
			if(currentX + width < this.perpLength - width - this.gap) {
				currentX += width + this.gap;
				colCount++;
			} else {
				currentX = 0;
				currentY += height + this.gap;
			}
		} else {
			currentY += height + this.gap;
		}
	};

	this.getItemsAbove = function(i) {

		var thumbsAbove = Math.floor(i / columns),
			height = 0,
			j = 0;

		for(j; j < thumbsAbove; j++) {
			var curThumbAbove = columns * (j + 1);
			if(this.axis === 'y') {
				height += items[i - curThumbAbove].height + this.gap;
			} else {
				height += items[i - curThumbAbove].width + this.gap;
			}
		}

		return height;
	};

	this.layoutItems = function() {
		var i = 0,
			length = items.length,
			columnHeights = [],
			rowWidths = [],
			column,
			row,
			operator0,
			operator1;
		currentX = 0;
		currentY = 0;
		colCount = 0;
		columns = this.getColumns();

		for(i; i < length; i++) {

			if(this.axis === 'y') {
				currentY = this.getItemsAbove(i);
			} else {
				currentX = this.getItemsAbove(i);
			}

			if(this.align === 'center' && !this.wrap) {
				items[i].x = (this.perpLength - items[i].width) * 0.5;
			} else if(this.align === 'right' && !this.wrap) {
				items[i].x = (this.perpLength - items[i].width);
			} else {
				items[i].x = currentX;
			}

			if(this.align === 'middle' && !this.wrap) {
				items[i].y = (this.perpLength - items[i].height) * 0.5;
			} else if(this.align === 'bottom' && !this.wrap) {
				items[i].y = (this.perpLength - items[i].height);
			} else {
				items[i].y = currentY;
			}

			switch (this.axis) {
				case 'x':
					this.incrHorizontal(i);
					break;
				case 'y':
					this.incrVertical(i);
					break;
			}

			operator0 = this.axis === 'y' ? '%' : '/';
			operator1 = this.axis === 'y' ? '/' : '%';

			column = Math.floor(this.operators[operator0](i, this.getColumns()));
			columnHeights[column] = this.getMaxDimArray(columnHeights, column, items[i], "height", i);

			row = Math.floor(this.operators[operator1](i, this.getColumns()));
			rowWidths[row] = this.getMaxDimArray(rowWidths, row, items[i], "width", i);

		}
		this.height = Math.max.apply(Math, columnHeights) - this.gap;
		this.width = Math.max.apply(Math, rowWidths) - this.gap;
	};

	this.getColumns = function() {
		var dim = this.axis === 'y' ? 'width' : 'height';
		return Math.floor(((this.perpLength + this.gap) - 1) / (items[0][dim] + this.gap)) || 1;
	};

	this.addItem = function(width, height) {
		items.push({width:width, height:height, x:0, y:0});
	};

	this.setSize = function(index, width, height) {
		items[index].width = width;
		items[index].height = height;
	};

	this.getPosition = function(index) {
		return items[index];
	};

	this.getBounds = function() {
		return {width:this.width, height:this.height};
	};

	this.getMaxDimArray = function (arr, arrIndex, item, dim, index) {
		var lastDim = arr[arrIndex] ? arr[arrIndex] : 0;
		return lastDim + item[dim] + this.gap;
	};

	this.operators = {
		'%': function(a, b) { return a % b },
		'/': function(a, b) { return a / b }
	};

}