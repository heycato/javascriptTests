var bitmapPool = [];

function newBitmap(vars) {
	/*trace(bitmapPool.length + ' in the Bitmap pool');*/
	if(bitmapPool.length > 0) {
		var bitmap = bitmapPool.splice(0, 1)[0];
		var i; for(i in vars) {
			bitmap[i] = vars[i];
		}
		/*trace('getting Bitmap from the pool');*/
		return bitmap;
	}
	/*trace('making Bitmap');*/
	return new Bitmap(vars);
}

function Bitmap(vars) {
	vars = vars || {};
	vars.type = 'img';
	vars.className = 'Bitmap';
	return new Sprite(vars);
}