function EventSandbox() {

	var _listeners = {};

	function findInArray(needle, haystack) {
		var i = haystack.length;
		while(i--) {
			if(needle.callback === haystack[i].callback && needle.target === haystack[i].target) {
				return true;
			}
		}
		return false;
	}

	return {
		listeners: _listeners,
		addEventListener: function(type, callback) {
			if(!_listeners[type]) _listeners[type] = [];
			var newListener = {type:type, callback:callback, target:this};
			if(!findInArray(newListener, _listeners[type])) {
				_listeners[type].push(newListener);
			}

		},
		removeEventListener: function(type, callback) {
			if(!_listeners[type]) {
				throw 'removeEventListener: type not registered';
			}
			if(!callback || typeof callback !== 'function') {
				delete _listeners[type];
			} else {
				var i = 0,
					l = _listeners[type].length;
				for(;i < l; i ++) {
					if(_listeners[type][i] && _listeners[type][i].callback === callback && _listeners[type][i].target === this) {
						_listeners[type].splice(i, 1);
					}
				}
				if(_listeners[type].length === 0) {
					delete _listeners[type];
				}
			}
		},
		dispatchEvent: function(type, data) {
			if(_listeners[type]) {
				for(var i = 0, l = _listeners[type].length; i < l; i ++) {
					if(_listeners[type][i] && !_listeners[type][i].callback) {
						console.error('VALID CALLBACK NOT FOUND ON LISTENER: ', _listeners[type][i]);
						break;
					}
					if(_listeners[type][i] && _listeners[type][i].target === this) {
						_listeners[type][i].callback.apply(_listeners[type][i].target, [data]);
					}
				}
			}
		}
	}
}

var globalEvents = new EventSandbox();
