var spritePool = [];

function newSprite(vars) {
	/*trace(spritePool.length + ' in the Sprite pool');*/
	if(spritePool.length > 0) {
		var sprite = spritePool.splice(0, 1)[0];
		var i; for(i in vars) {
			sprite[i] = vars[i];
		}
		trace(spritePool.length + ' in the Sprite pool', 'getting ' + sprite.className + ' from pool');
		return sprite;
	}
	trace('making new Sprite');
	return new Sprite(vars);
}

function Sprite(vars) {

	vars = vars || {};
	vars.events = vars.events || globalEvents;

	var element;

	if(vars.type === 'svg' || vars.type === 'g' || vars.type === 'circle' || vars.type === 'ellipse' || vars.type === 'line' || vars.type === 'path' || vars.type === 'polygon' || vars.type === 'polyline' || vars.type === 'rect') {
		element = document.createElementNS("http://www.w3.org/2000/svg", vars.type);
	} else {
		element = document.createElement(vars.type || 'div');
	}

	var style = element.style;
	var _private = {};

	style.opacity = vars.alpha || 1;
	style.display = vars.display || 'block';
	style.position =  vars.position || 'absolute';

	if(vars.border) {
		style.border = vars.border;
	}

	var sprite = {

		className: vars.className || 'Sprite',

		element: element,

		children: [],
		_private: _private,

		listeners: [],
		dispatchEvent: vars.events.dispatchEvent,
		addEventListener: function(type, callback) {
			var l = ELEMENT_EVENTS.length;
			while(l--) {
				if(type === ELEMENT_EVENTS[l]) {
					element.addEventListener(type, this.event, false);
				}
			}
			vars.events.addEventListener.apply(this, [type, callback]);
			this.listeners.push({type:type, callback:callback});
		},
		removeEventListener: function(type, callback) {
			var l = ELEMENT_EVENTS.length;
			while(l--) {
				if(type === ELEMENT_EVENTS[l]) {
					element.removeEventListener(type, this.event, false);
				}
			}
			vars.events.removeEventListener.apply(this, [type, callback]);
		},

		addChild: function(child) {
			if(!element[child.element]) {
				element.appendChild(child.element);
			}
			child.parent = this;
			if(child.id) this[child.id] = child;
			this.children.push(child);
			child.dispatchEvent(CHILD_ADDED, child);
		},

		childAdded: function(child) {
			var i = this.children.length;
			while(i--) {
				if(this.children[i].topParentName === 'Stage' && this.children[i] === child) {
					this.children[i].dispatchEvent(CHILD_ADDED, this.children[i]);
				}
			}
		},

		moveToTop: function(child) {
			var highestZ = 0;
			var i = this.children.length;
			while(i--) {
				if(this.children[i].zIndex > highestZ && !this.children[i] === child) {
					highestZ = this.children[i].zIndex + 1;
				}
			}
			child.zIndex = highestZ;
		},

		get topParentName() {
			if(this.parent) {
				return this.parent.topParentName;
			}
			return name;
		},

		removeChild: function(child) {
			if(child.removeLoader) child.removeLoader();
			if(child.element.parentNode) {
				child.element.parentNode.removeChild(child.element);
			}
			if(child.id) this[child.id] = null;
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					this.children.splice(i, 1);
					child = null;
				}
			}
		},

		getClass: function() {
			return element.className;
		},

		setClass: function(value) {
			element.setAttribute('class', value);
		},

		hasClass: function(cls) {
			return new RegExp('(^|\\s)' + cls + '(\\s|$)').test(element.className);
		},

		event: function(e) {
			if(!_private.selectable && !TOUCH_DEVICE && vars.type !== 'input' && vars.type !== 'textarea') {
				e.preventDefault();
			}
			if(TOUCH_DEVICE && e.touches && e.touches.length === 1 && stage.zoom <= 1) {
				e.preventDefault();
			}
			if(vars.type === 'img') {
				if(e.target.width && !vars.width) {
					sprite.width = e.target.width;
				}
				if(e.target.height && !vars.height) {
					sprite.height = e.target.height;
				}
			}
			var event = {
				type: e.type,
				target:sprite,
				currentTarget:sprite,
				keyCode: e.which,
				pageX:e.pageX,
				pageY:e.pageY,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX: e.clientX - sprite.stageX,
				mouseY: e.clientY - sprite.stageY,
				preventDefault: function() {
					e.preventDefault();
				}
			};

			if(e.type === FOCUS) {
				if(stage.activeFocus && stage.activeFocus !== sprite) {
					stage.activeFocus.dispatchEvent(BLUR, event);
					stage.activeFocus = sprite;
				}
			}

			if(e.type === MOUSE_DOWN || e.type === TOUCH_START) {
				if(stage.activeFocus) stage.activeFocus.dispatchEvent(BLUR, event);
				sprite.focus();
				setTimer(sprite.dispatchEvent, 0, [FOCUS, event], sprite);
			}

			if(e.touches) event.touches = e.touches;
			sprite.dispatchEvent(e.type, event);
		},

		hitTestPoint: function(xPoint, yPoint) {
			return style.display !== 'none' && xPoint >= this.stageX && xPoint <= this.stageX + _private.width && yPoint >= this.stageY && yPoint <= this.stageY + _private.height;
		},

		hitTestAbs: function(xPoint, yPoint) {
			return style.display !== 'none' && xPoint >= this.absX && xPoint <= this.absX + _private.width && yPoint >= this.absY && yPoint <= this.absY + _private.height;
		},

		globalToLocal: function (point) {
			return {x:point.x - this.stageX, y:point.y - this.stageY};
		},

		localToGlobal: function (point) {
			return {x:point.x + this.stageX, y:point.y + this.stageY};
		},

		/* general */

		get src() {
			return _private.src;
		},
		set src(value) {
			_private.src = value;
			element.src = value;
		},

		get text() {
			return _private.text;
		},
		set text(value) {
			_private.text = value;
			element.innerHTML = value;
		},

		get innerHTML() {
			return _private.text;
		},
		set innerHTML(value) {
			_private.text = value;
			element.innerHTML = value;
		},

		get alpha() {
			return _private.alpha;
		},
		set alpha(value) {
			_private.alpha = value;
			style.opacity = value;
		},

		get opacity() {
			return _private.alpha;
		},
		set opacity(value) {
			_private.alpha = value;
			style.opacity = value;
		},

		get display() {
			return _private.display;
		},
		set display(value) {
			_private.display = value;
			style.display = value;
		},

		get position() {
			return _private.position;
		},
		set position(value) {
			_private.position = value;
			style.position = value;
		},

		get top() {
			return _private.y || 0;
		},
		set top(value) {
			_private.y = value;
			style.top = value + 'px';
		},

		get x() {
			return _private.x || 0;
		},
		set x(value) {
			_private.x = value;
			if(vars.type === 'rect') {
				element.setAttribute('x', value);
			} else {
				style.left = value + 'px';
			}
		},

		get y() {
			return _private.y || 0;
		},
		set y(value) {
			_private.y = value;
			if(vars.type === 'rect') {
				element.setAttribute('y', value);
			} else {
				style.top = value + 'px';
			}
		},

		get translate() {
			return _private.translate;
		},
		set translate(value) {
			_private.translate = value;
			_private.translateX = value[0];
			_private.translateY = value[1];
			style['-webkit-transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
			style['-ms-transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
			style['transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
		},

		get translateX() {
			return _private.translateX;
		},
		set translateX(value) {
			_private.translateX = value;
			_private.translateY = _private.translateY || 0;
			style['-webkit-transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
			style['-ms-transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
			style['transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
		},

		get translateY() {
			return _private.translateY;
		},
		set translateY(value) {
			_private.translateY = value;
			_private.translateX = _private.translateX || 0;
			style['-webkit-transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
			style['-ms-transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
			style['transform'] = 'translate(' + _private.translateX + 'px,' + _private.translateY + 'px)';
		},

		get stageX() {
			if(this.parent) {
				return this.parent.stageX + this.x;
			}
			return this.x;
		},

		get stageY() {
			if(this.parent) {
				return this.parent.stageY + this.y;
			}
			return this.y;
		},

		get absX() {
			var x = 0;
			var el = this.element;
			if (el.offsetParent) {
				do { x += el.offsetLeft; } while (el = el.offsetParent);
			}
			return x;
		},

		get absY() {
			var y = 0;
			var el = this.element;
			if (el.offsetParent) {
				do { y += el.offsetTop; } while (el = el.offsetParent);
			}
			return y;
		},

		get right() {
			return _private.right;
		},
		set right(value) {
			_private.right = value;
			style.right = value + 'px';
		},

		get bottom() {
			return _private.bottom;
		},
		set bottom(value) {
			_private.bottom = value;
			style.bottom = value + 'px';
		},

		get left() {
			return _private.x || 0;
		},
		set left(value) {
			_private.x = value;
			style.left = value + 'px';
		},

		get width() {
			_private.width = element.offsetWidth || _private.width || 0;
			return _private.width;
		},
		set width(value) {
			_private.width = value;
			if(vars.type === 'svg' || vars.type === 'rect') {
				element.setAttribute('width', value);
			} else {
				style.width = value + 'px';
			}
		},

		get height() {
			_private.height = element.offsetHeight || _private.height || 0;
			return _private.height;
		},
		set height(value) {
			_private.height = value;
			if(vars.type === 'svg' || vars.type === 'rect') {
				element.setAttribute('height', value);
			} else {
				style.height = value + 'px';
			}
		},

		get zIndex() {
			return _private.zIndex;
		},
		set zIndex(value) {
			_private.zIndex = value;
			style.zIndex = value;
		},

		get cursor() {
			return _private.cursor;
		},
		set cursor(value) {
			_private.cursor = value;
			style.cursor = value;
		},

		get shadow() {
			return _private.shadow;
		},
		set shadow(value) {
			_private.shadow = value;
			style.boxShadow = value;
		},

		get visibility() {
			return _private.visibility;
		},
		set visibility(value) {
			_private.visibility = value;
			style.visibility = value;
		},

		get overflow() {
			return _private.overflow;
		},
		set overflow(value) {
			_private.overflow = value;
			style.overflow = value;
		},


		/* background */

		get background() {
			return _private.background;
		},
		set background(value) {
			_private.background = value;
			style.background = value;
		},


		get backgroundColor() {
			return _private.bgColor;
		},
		set backgroundColor(value) {
			_private.bgColor = value;
			style.backgroundColor = value;
		},

		get parent(){
			return _private.parent;
		},
		set parent(value){
			_private.parent = value;
		},

		/* border */

		get border() {
			return _private.border;
		},
		set border(value) {
			_private.border = value;
			style.border = value;
		},
		get borderTop() {
			return _private.borderTop;
		},
		set borderTop(value) {
			_private.borderTop = value;
			style.borderTop = value;
		},
		get borderRight() {
			return _private.borderRight;
		},
		set borderRight(value) {
			_private.borderRight = value;
			style.borderRight = value;
		},
		get borderBottom() {
			return _private.borderBottom;
		},
		set borderBottom(value) {
			_private.borderBottom = value;
			style.borderBottom = value;
		},
		get borderLeft() {
			return _private.borderLeft;
		},
		set borderLeft(value) {
			_private.borderLeft = value;
			style.borderLeft = value;
		},

		get borderColor() {
			return _private.borderColor;
		},
		set borderColor(value) {
			_private.borderColor = value;
			style.borderColor = value;
		},

		get borderRadius() {
			return _private.borderRadius;
		},
		set borderRadius(value) {
			_private.borderRadius = value;
			style.borderRadius = value + 'px';
		},


		/* fonts */

		get font() {
			return _private.font;
		},
		set font(value) {
			_private.font = value;
			style.font = value;
		},

		get fontColor() {
			return _private.fontColor;
		},
		set fontColor(value) {
			_private.fontColor = value;
			style.color = value;
		},

		get fontSize() {
			return _private.fontSize;
		},
		set fontSize(value) {
			_private.fontSize = value;
			style.fontSize = value + 'px';
		},

		get fontFamily() {
			return _private.fontFamily;
		},
		set fontFamily(value) {
			_private.fontFamily = value;
			style.fontFamily = value;
		},

		get fontWeight() {
			return _private.fontWeight;
		},
		set fontWeight(value) {
			_private.fontWeight = value;
			style.fontWeight = value;
		},

		get textAlign() {
			return _private.textAlign;
		},
		set textAlign(value) {
			_private.textAlign = value;
			style.textAlign = value;
		},

		get textAlignVertical() {
			return _private.textAlignVertical;
		},
		set textAlignVertical(value) {
			_private.textAlignVertical = value;
			if(value === 'middle' || value === 'bottom') {
				style.position = 'relative';
				style.display = 'table-cell';
			}
			style.verticalAlign = value;
		},

		get textDecoration() {
			return _private.textDecoration;
		},
		set textDecoration(value) {
			_private.textDecoration = value;
			style.textDecoration = value;
		},

		get textTransform() {
			return _private.textTransform;
		},
		set textTransform(value) {
			_private.textTransform = value;
			style.textTransform = value;
		},

		get letterSpacing() {
			return _private.letterSpacing;
		},
		set letterSpacing(value) {
			_private.letterSpacing = value;
			style.letterSpacing = value;
		},

		get lineHeight() {
			return _private.lineHeight;
		},
		set lineHeight(value) {
			_private.lineHeight = value;
			style.lineHeight = value;
		},

		get list() {
			return _private.list;
		},
		set list(value) {
			_private.list = value;
			style.list = value;
		},

		get value() {
			return element.value;
		},
		set value(value) {
			element.value = value;
		},


		/* margin */

		get margin() {
			return _private.margin;
		},
		set margin(value) {
			_private.margin = value;
			style.margin = value + 'px';
		},

		get marginTop() {
			return _private.marginTop;
		},
		set marginTop(value) {
			_private.marginTop = value;
			style.marginTop = value + 'px';
		},

		get marginRight() {
			return _private.marginRight;
		},
		set marginRight(value) {
			_private.marginRight = value;
			style.marginRight = value + 'px';
		},

		get marginBottom() {
			return _private.marginBottom;
		},
		set marginBottom(value) {
			_private.marginBottom = value;
			style.marginBottom = value + 'px';
		},

		get marginLeft() {
			return _private.marginLeft;
		},
		set marginLeft(value) {
			_private.marginLeft = value;
			style.marginLeft = value + 'px';
		},


		/* padding */

		get padding() {
			return _private.padding;
		},
		set padding(value) {
			_private.padding = value;
			style.padding = value + 'px';
		},

		get paddingTop() {
			return _private.paddingTop;
		},
		set paddingTop(value) {
			_private.paddingTop = value;
			style.paddingTop = value + 'px';
		},

		get paddingRight() {
			return _private.paddingRight;
		},
		set paddingRight(value) {
			_private.paddingRight = value;
			style.paddingRight = value + 'px';
		},

		get paddingBottom() {
			return _private.paddingBottom;
		},
		set paddingBottom(value) {
			_private.paddingBottom = value;
			style.paddingBottom = value + 'px';
		},

		get paddingLeft() {
			return _private.paddingLeft;
		},
		set paddingLeft(value) {
			_private.paddingLeft = value;
			style.paddingLeft = value + 'px';
		},
		get selectable() {
			return _private.selectable;
		},
		set selectable(value) {
			_private.selectable = value;
			if(!_private.selectable && vars.type !== 'input' && vars.type !== 'textarea') {
				style.cursor = 'default';
				style['-webkit-touch-callout'] = 'none';
				style['-webkit-user-select'] = 'none';
				style['-khtml-user-select'] = 'none';
				style['-moz-user-select'] = 'none';
				style['-ms-touch-callout'] = 'none';
				style['user-select'] = 'none';
			} else {
				style.cursor = 'auto';
				style['-webkit-touch-callout'] = 'auto';
				style['-webkit-user-select'] = 'auto';
				style['-khtml-user-select'] = 'auto';
				style['-moz-user-select'] = 'auto';
				style['-ms-touch-callout'] = 'auto';
				style['user-select'] = 'auto';
			}
		},

		get rotate() {
			return _private.rotate;
		},
		set rotate(value) {
			_private.rotate = value;
			style['-webkit-transform'] = 'rotate('+value+'deg)';
			style['-moz-transform'] = 'rotate('+value+'deg)';
			style['-o-transform'] = 'rotate('+value+'deg)';
			style.transform = 'rotate('+value+'deg)';
		},

		get textWrap() {
			return _private.textWrap;
		},
		set textWrap(value) {
			_private.textWrap = value;
			style['white-space'] = value ? 'normal' : 'nowrap';
		},

		get transition() {
			return _private.transition;
		},
		set transition(value) {
			_private.transition = value;
			value = value === 0 ? '' : 'all ' + value + 's';
			style.WebkitTransition = value;
			style.MozTransition = value;
			style.OTransition = value;
			style.transition = value;
		},

		/*svg support*/

		get d() {
			return element.getAttribute('d');
		},
		set d(value) {
			element.setAttribute('d', value);
		},

		get points() {
			return element.getAttribute('points');
		},
		set points(value) {
			element.setAttribute('points', value);
		},

		get cx() {
			return _private.cx;
		},
		set cx(value) {
			_private.cx = value;
			element.setAttribute('cx', value);
		},

		get cy() {
			return _private.cy;
		},
		set cy(value) {
			_private.cy = value;
			element.setAttribute('cy', value);
		},

		get x1() {
			return _private.x1;
		},
		set x1(value) {
			_private.x1 = value;
			element.setAttribute('x1', value);
		},

		get y1() {
			return _private.y1;
		},
		set y1(value) {
			_private.y1 = value;
			element.setAttribute('y1', value);
		},

		get x2() {
			return _private.x2;
		},
		set x2(value) {
			_private.x2 = value;
			element.setAttribute('x2', value);
		},

		get y2() {
			return _private.y2;
		},
		set y2(value) {
			_private.y2 = value;
			element.setAttribute('y2', value);
		},

		get r() {
			return _private.r;
		},
		set r(value) {
			_private.r = value;
			element.setAttribute('r', value);
		},

		get rx() {
			return _private.rx;
		},
		set rx(value) {
			_private.rx = value;
			element.setAttribute('rx', value);
		},

		get ry() {
			return _private.ry;
		},
		set ry(value) {
			_private.ry = value;
			element.setAttribute('ry', value);
		},

		get fill() {
			return _private.fill;
		},
		set fill(value) {
			_private.fill = value;
			element.setAttribute('fill', value);
		},

		get stroke() {
			return _private.stroke;
		},
		set stroke(value) {
			_private.stroke = value;
			element.setAttribute('stroke', value);
		},

		get strokeWidth() {
			return _private.strokeWidth;
		},
		set strokeWidth(value) {
			_private.strokeWidth = value;
			element.setAttribute('stroke-width', value);
		},

		focus: function() {
			stage.activeFocus = sprite;
			if(vars.type === 'input' || vars.type === 'textarea') element.focus();
		},
		blur: function() {
			if(vars.type === 'input' || vars.type === 'textarea') element.blur();
		}
	};

	sprite.removeListeners = function() {
		var i = sprite.listeners.length;
		while(i --) {
			sprite.removeEventListener(sprite.listeners[i].type, sprite.listeners[i].callback);
		}
		sprite.listeners = [];
	};

	sprite.unsetProps = function() {
		var i; for(i in _private) {
			delete _private[i];
		}
	};

	sprite.unsetStyle = function() {
		var i = element.style.length;
		while(i--) {
			element.style[element.style[i]] = '';
		}
	};

	sprite.unsetAttr = function() {
		var i = element.attributes.length;
		while(i--) {
			element.setAttribute(element.attributes[i].name, '');
		}
	};

	sprite.clean = function() {
		sprite.unsetProps();
		sprite.unsetStyle();
		sprite.unsetAttr();
		sprite.removeListeners();
		element.innerHTML = '';
		sprite.alpha = 1;
		sprite.display = 'block';
		sprite.position = 'absolute';
		sprite.text = '';
		sprite.x = 0;
		sprite.y = 0;
		sprite.translateX = 0;
		sprite.translateY = 0;
		sprite.transition = 0;
	};

	sprite.addEventListener(CHILD_ADDED, sprite.childAdded);

	if(!_private.selectable) {
		sprite.selectable = false;
	}

	if(vars.id) {
		sprite.id = vars.id;
	}

	if(vars.collection) {
		sprite.setClass(vars.collection);
	}

	element.vars = sprite;
	sprite.style = element.style;

	var i; for(i in vars) {
		sprite[i] = vars[i];
	}

	return sprite;

}