function Tween(target, duration, vars) {

	var delay = vars.delay || 0;

	function init(){
		target.transition = duration;
		var i; for(i in vars) {
			if(!/delay|ease|css|onComplete|onCompleteScope|onCompleteParams|onUpdate|onUpdateScope|onUpdateParams/.test(i)) {
				target[i] = vars[i];
			}
		}
	}

	function complete() {
		target.transition = 0;
		if(vars.onComplete) vars.onComplete.apply(vars.onCompleteScope || target, vars.onCompleteParams || [false]);
	}

	setTimeout(init, delay);
	setTimeout(complete, (duration * 1000) + delay);
}


/*	firefox appears to have a threading issue with tweens when back to back
	var isFirefox = navigator.userAgent.match(/Firefox/i) != null,
	threadDelay = isFirefox ? 0 : 0;*/
