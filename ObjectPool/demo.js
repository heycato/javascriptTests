function demo() {

	var tile,
		count = 10,
		selection = [],
		images = ['dx', 'dxlogo', 'dxlogowire', 'dxredwhiteshadow'],
		container = thumbsContainer();

	stage.addChild(container);

	function init() {
		while(count --) {
			thumbBuild(count);
			thumbsLayout();
		}
	}

	function rand() {
		return Math.floor(Math.random() * images.length);
	}


	recycleAllBtn();
	recycleSelectionBtn();
	addTenBtn();
	tracePoolsBtn();
	clearPoolsBtn();
	init();

	//make dummy thumbs
	function thumbBuild(id) {
		var i = rand(),
			bg = thumbBg(),
			img = thumbImg(images[i]),
			label = thumbLabel(images[i]);

		img.y = 10;
		label.x = 17;
		label.y = 60;
		bg._id = id;
		bg.addChild(img);
		bg.addChild(label);
		container.addChild(bg);
	}

	function thumbBg() {
		var bg = newSprite();
		bg.width = 80;
		bg.height = 80;
		bg.backgroundColor = '#DEDEDE';
		bg.element.style['overflow'] = 'hidden';
		bg.addEventListener(CLICK, select);
		return bg;
	}

	function thumbImg(image) {
		var img = newBitmap();
		img.width = 80;
		img.height = 40;
		img.src = image + '.png';
		return img;
	}

	function thumbLabel(text) {
		var lbl = newSprite();
		lbl.width = 60;
		lbl.height = 16;
		lbl.fontFamily = 'Helvetica';
		lbl.fontSize = 10;
		lbl.fontColor = '#777777';
		lbl.text = text;
		return lbl;
	}

	function thumbsContainer() {
		var el = newSprite(),
			winW = window.innerWidth;
		el.width = 400;
		el.height = 300;
		el.x = (winW - el.width) * 0.5;
		el.y = 50;
		return el;
	}

	function thumbsLayout() {
		tile = new Tile();
		tile.gap = 10;
		tile.axis = 'y';
		tile.align = 'left';
		tile.wrap = true;
		tile.perpLength = container.width;

		var i,
			thumbs = container.children,
			length = thumbs.length;

		for(i = 0; i < length; i++) {
			tile.addItem(thumbs[i].width, thumbs[i].height);
		}

		tile.layoutItems();

		for(i = 0; i < length; i++) {
			var position = tile.getPosition(i);
			Tween(thumbs[i], 0.35, {x:position.x, y:position.y + 1});
		}
	}

	function recycleAllBtn() {
		var btn = newSprite({type:'button'});
		btn.width = 120;
		btn.height = 20;
		btn.x = 20;
		btn.y = 20;
		btn.backgroundColor = '#DEDEDE';
		btn.border = 'none';
		btn.text = 'recycle all';
		btn.addEventListener(CLICK, recycleAll);
		btn.addEventListener(MOUSE_OVER, hover);
		stage.addChild(btn);
	}

	function recycleSelectionBtn() {
		var btn = newSprite({type:'button'});
		btn.width = 120;
		btn.height = 20;
		btn.x = 20;
		btn.y = 50;
		btn.backgroundColor = '#DEDEDE';
		btn.border = 'none';
		btn.text = 'recycle selection';
		btn.addEventListener(CLICK, recycleSelection);
		btn.addEventListener(MOUSE_OVER, hover);
		stage.addChild(btn);
	}

	function addTenBtn() {
		var btn = newSprite({type:'button'});
		btn.width = 120;
		btn.height = 20;
		btn.x = 20;
		btn.y = 80;
		btn.backgroundColor = '#DEDEDE';
		btn.border = 'none';
		btn.text = 'add ten';
		btn.addEventListener(CLICK, addTen);
		btn.addEventListener(MOUSE_OVER, hover);
		stage.addChild(btn);
	}

	function tracePoolsBtn() {
		var btn = newSprite({type:'button'});
		btn.width = 120;
		btn.height = 20;
		btn.x = 20;
		btn.y = 110;
		btn.backgroundColor = '#DEDEDE';
		btn.border = 'none';
		btn.text = 'trace pools';
		btn.addEventListener(CLICK, tracePools);
		btn.addEventListener(MOUSE_OVER, hover);
		stage.addChild(btn);
	}

	function clearPoolsBtn() {
		var btn = newSprite({type:'button'});
		btn.width = 120;
		btn.height = 20;
		btn.x = 20;
		btn.y = 140;
		btn.backgroundColor = '#DEDEDE';
		btn.border = 'none';
		btn.text = 'clear pools';
		btn.addEventListener(CLICK, clearPools);
		btn.addEventListener(MOUSE_OVER, hover);
		stage.addChild(btn);
	}

	function select(e) {
		if(!e.target.selected) {
			e.target.backgroundColor = 'rgba(0, 127, 255, 0.5';
			e.target.selected = true;
			selection.push(e.target);
		} else {
			e.target.backgroundColor = '#DEDEDE';
			e.target.selected = false;
			trace(e.target);
			var i = selection.indexOf(e.target);
			selection.splice(i, 1);
		}
	}

	function recycleAll() {
		stage.recycleChildren(container.children);
	}

	function recycleSelection() {
		var i = selection.length,
			k = container.children.length;

		while(i --) {
			var k = container.children.indexOf(selection[i]);
			if(k !== -1) {
				trace('match');
				stage.recycleChildren(selection[i].children);
				selection[i].clean();
				selection[i].selected = false;
				stage.pushToPool(selection[i]);
				container.children[k].element.parentNode.removeChild(container.children[k].element);
				container.children.splice(k, 1);
				selection.splice(i, 1);
			}
		}
		thumbsLayout();
	}

	function addTen() {
		count = 10;
		init();
	}

	function tracePools() {
		var i = spritePool.length,
			k = bitmapPool.length;
		while(i -- ) {
			console.dir(spritePool[i].element);
		}
		while(k -- ) {
			console.dir(bitmapPool[k].element);
		}
		trace('spritePool:', spritePool.length, spritePool);
		trace('bitmapPool:', bitmapPool.length, bitmapPool);
	}

	function clearPools() {
		var sLen = spritePool.length,
			bLen = bitmapPool.length;
		while(sLen --) {
			spritePool.splice(sLen, 1);
		}
		while(bLen --) {
			bitmapPool.splice(bLen, 1);
		}
		trace('spritePool:', spritePool.length, 'bitmapPool:', bitmapPool.length);
	}

	function hover(e) {
		e.target.backgroundColor = 'rgba(0, 127, 255, 0.5';
		e.target.addEventListener(MOUSE_OUT, out);
	}

	function out(e) {
		e.target.backgroundColor = '#DEDEDE';
		e.target.removeEventListener(MOUSE_OUT, out);
	}

	thumbBuild();

}

