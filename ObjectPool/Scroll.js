var Scrolls = {

	arr: [],

	register: function(scroll) {
		this.arr.push(scroll);
	},

	unRegister: function(scroll) {
		var i = this.arr.length;
		while(i--) {
			if(scroll === this.arr[i]) {
				this.arr.splice(i, 1);
			}
		}
	},

	resize: function() {
		if(this.arr.length > 0) {
			var i = this.arr.length;
			while(i--) {
				this.arr[i].resize();
			}
		}
	},

	hitMask: function(x, y) {
		var i = this.arr.length;
		while(i--) {
			this.arr[i].active = this.arr[i].mask.hitTestPoint(x, y);
		}
	},

	hitScrollBar: function(x, y) {
		var i = this.arr.length;
		while(i--) {
			if(this.arr[i].bar && this.arr[i].bar.handle.hitTestAbs(x, y)) {
				return this.arr[i].bar.back.hitTestAbs(x, y);
			}
		}
		return false;
	},

	get ing() {
		var i = this.arr.length;
		while(i--) {
			if(this.arr[i].ing) {
				return this.arr[i].ing;
			}
		}
		return false;
	}
};

function Scroll(obj, mask, vars) {

	vars.events = vars.events || globalEvents;

	var _percent = 0,
		_position = 0,
		_scrollDist = 0,
		_scrolling = false;

	var scroll = {
		obj: obj,
		mask: mask,
		listeners: vars.events.listeners,
		dispatchEvent: vars.events.dispatchEvent,
		addEventListener: vars.events.addEventListener,
		removeEventListener: vars.events.removeEventListener,
		types: vars.types || ['bar','wheel','touch'],
		axis: vars.axis || 'y',
		align: vars.align || 'center',
		margin: vars.margin || 0,
		maxTime: vars.maxTime || 300,
		minTime: vars.minTime || 1,
		maxSpeed: vars.maxSpeed || 25,
		minSpeed: vars.minSpeed || 2,
		color: vars.color || '#00FF00',
		hover: vars.hover || '#FF0000',
		alpha: vars.alpha || 0.35,
		zIndex: vars.zIndex,
		side: vars.side || 'right',
		offsetX: vars.offsetX || 0,
		offsetY: vars.offsetY || 0,
		width: vars.width || 7,
		get percent(){
			return _percent;
		},
		set percent(value){
			value = value < 0 ? 0 : value;
			value = value > 1 ? 1 : value;
			_percent = value;
		},
		get position(){
			return _position;
		},
		set position(value){
			_position = value;
		},
		get scrollDist(){
			return _scrollDist;
		},
		set scrollDist(value){
			_scrollDist = value;
		},
		get ing(){
			return _scrolling;
		},
		set ing(value){
			_scrolling = value;
		}
	};

	scroll.perpAxis = vars.axis === 'x' ? 'y' : 'x';
	scroll.dim = vars.axis === 'x' ? 'width' : 'height';
	scroll.perpDim = vars.axis === 'x' ? 'height' : 'width';

	scroll.alignment = function() {
		switch(scroll.align) {
			case 'middle':
			case 'center':
				return 0.5;
			case 'right':
			case 'bottom':
				return 1;
		}
		return 0;
	};

	scroll.update = function(speed) {
		if(this.obj[this.dim] < mask[this.dim]) {
			_percent = scroll.alignment();
			speed = 0.35;
		}

		_scrollDist = (this.margin + this.obj[this.dim] + this.margin) - this.mask[this.dim];
		_position = -(_scrollDist * _percent) + this.margin;
		var tweenVars = {};
		tweenVars[vars.axis] = Mth.round(_position);
		obj.transition = speed;
//		Tween(obj, Mth.round(speed), tweenVars);
		obj[vars.axis] = Mth.round(_position);

		if(scroll.bar) {
			scroll.bar.moveHandle();
		}
		scroll.dispatchEvent(SCROLL);
	};

	scroll.resize = function() {
		if(scroll.bar) {
			scroll.bar.resize();
		}
		if(this.obj[this.dim] < mask[this.dim]) {
			scroll.update(0.35);
		}
	};

	scroll.remove = function() {
		if(scroll.bar) {
			scroll.bar.remove();
		}
		if(scroll.wheel) {
			scroll.wheel.remove();
		}
		if(scroll.hover) {
			scroll.hover.remove();
		}
		if(scroll.touch) {
			scroll.touch.remove();
		}
	};

	function buildBehaviors() {
		var i = vars.types.length;
		while(i--) {
			switch (vars.types[i]) {
				case 'bar':
					scroll.bar = new ScrollBar(scroll);
					break;
				case 'wheel':
					scroll.wheel = new ScrollWheel(scroll);
					break;
				case 'hover':
					scroll.hover = new ScrollHover(scroll);
					break;
				case 'touch':
					scroll.touch = new ScrollTouch(scroll);
					break;
			}
		}
	}

	buildBehaviors();
	scroll.resize();
	scroll.update();
	Scrolls.register(scroll);

	return scroll;
}

function ScrollTouch(scroll) {

	var startPoint = 0,
		startPercent = 0,
		moveDist = 0;

	scroll.mask.addEventListener(TOUCH_START, onStart);

	function onStart(e) {
		startPoint = getPoint(e);
		startPercent = scroll.percent;
		scroll.mask.addEventListener(TOUCH_END, onEnd);
		scroll.mask.addEventListener(TOUCH_MOVE, onMove);
	}

	function onMove(e) {
		scroll.ing = true;
		moveDist = getPoint(e) - startPoint;
		scroll.percent = -(moveDist / (scroll.scrollDist)) + startPercent;
		scroll.percent = scroll.percent < 0 ? 0 : scroll.percent;
		scroll.percent = scroll.percent > 1 ? 1 : scroll.percent;

		scroll.update(0);
	}

	function getPoint(e) {
		if(scroll.axis === 'x') {
			return touchDevice ? event.touches[0].pageX : event.clientX;
		}
		return touchDevice ? event.touches[0].pageY : event.clientY;
	}

	function onEnd(e) {
		if(scroll.ing) event.stopPropagation();
		scroll.ing = false;
		scroll.mask.removeEventListener(TOUCH_END, onEnd);
		scroll.mask.removeEventListener(TOUCH_MOVE, onMove);
	}

	this.remove = function() {
		scroll.mask.removeEventListener(TOUCH_START, onStart);
	};

}

function ScrollWheel(scroll) {

	scroll.active = false;

	stage.addEventListener(MOUSE_WHEEL, onMouseWheel);
	stage.addEventListener("wheel", onMouseWheel);
	stage.addEventListener("MozMousePixelScroll", onMousePixel);

	function onMouseWheel(e) {
		if(scroll.active) {
			var delta = scroll.axis === 'x' ? e.deltaX : e.deltaY;
			update(delta);
		}
	}

	function onMousePixel(e) {
		if(scroll.active) {
			var delta = scroll.axis === 'x' ? e.deltaX : e.deltaY;
			delta *= 120;
			update(e);
		}
	}

	function update(delta) {
		delta = scroll.align === 'bottom' ? delta = -delta : delta;
		scroll.percent += (delta * 0.0001);
		scroll.percent = scroll.percent < 0 ? 0 : scroll.percent;
		scroll.percent = scroll.percent > 1 ? 1 : scroll.percent;

		scroll.update(0);
	}

	this.remove = function() {
		stage.removeEventListener(MOUSE_WHEEL, onMouseWheel);
	};
}

function ScrollHover(scroll) {

	var percent = 0;
	var weightedTime = 5;
	var mouseTimer;

	scroll.mask.addEventListener(MOUSE_MOVE, function(e) {
		clearTimeout(mouseTimer);
		mouseTimer = setTimeout(function() {
			update(e);
		}, 20);
	});

	function update(e) {
		var mousePoint = scroll.axis === 'x' ? e.mouseX : e.mouseY;
		percent = mousePoint / scroll.mask[scroll.dim];
		scroll.percent = percent > 0.5 ? 1 : 0;

		var distance = Mth.abs(scroll.mask[scroll.dim] - scroll.obj[scroll.dim]);

		weightedTime = (distance / (500 * scroll.maxSpeed)) + ease() + scroll.minSpeed;

		scroll.update(weightedTime);
	}

	function ease() {
		var half = percent < 0.5 ? percent * 2 : (percent * -2) + 2;
		return Mth.round((Mth.pow(half, 5) * scroll.maxSpeed) + scroll.minSpeed);
	}

	this.remove = function() {
		scroll.mask.removeEventListener(MOUSE_MOVE, update);
	};
}

function ScrollBar(scroll) {

	var obj = scroll.obj,
		mask = scroll.mask,
		parent = mask.parent,
		moveDist = 0,
		startPoint = 0;

	var back = this.back = new Sprite();
	back.alpha = 0;
	back.selectable = true;
	back.backgroundColor = scroll.color;
	if(scroll.zIndex) back.zIndex = scroll.zIndex;
	parent.addChild(back);

	var handle = this.handle = new Sprite();
	handle.alpha = 0;
	handle.backgroundColor = scroll.color;
	if(scroll.zIndex) handle.zIndex = scroll.zIndex;
	handle.selectable = false;
	handle.cursor = 'default';
	parent.addChild(handle);

	handle.addEventListener(MOUSE_OVER, onMouseOver);
	handle.addEventListener(MOUSE_OUT, onMouseOut);
	handle.addEventListener(MOUSE_DOWN, onMouseDown);

	this.moveHandle = function() {
		handle.transition = 0;
		handle[scroll.axis] = ((back[scroll.dim] - handle[scroll.dim]) * scroll.percent) + back[scroll.axis];
	};

	this.resize = function() {
		back[scroll.perpDim] = scroll.width;
		back[scroll.dim] = mask[scroll.dim];
		back[scroll.axis] = mask[scroll.axis] + getOffset();

		handle[scroll.perpDim] = scroll.width;
		var handleSize = (mask[scroll.dim] / obj[scroll.dim]) * mask[scroll.dim];
		if(handleSize >= back[scroll.dim]) {
			Tween(back, 1, {alpha:0});
			Tween(handle, 1, {alpha:0});
			handle.transition = 0;
			handle[scroll.dim] = back[scroll.dim];
		} else {
			Tween(back, 1, {alpha:scroll.alpha});
			Tween(handle, 1, {alpha:1});
			handle.transition = 0;
			handle[scroll.dim] = handleSize;
		}

		if(scroll.side === 'left' || scroll.side === 'top') {
			back[scroll.perpAxis] = mask[scroll.perpAxis] - scroll.width + getPerpOffset();
			handle[scroll.perpAxis] = mask[scroll.perpAxis] - scroll.width + getPerpOffset();
		} else {
			back[scroll.perpAxis] = mask[scroll.perpAxis] + mask[scroll.perpDim] + getPerpOffset();
			handle[scroll.perpAxis] = mask[scroll.perpAxis] + mask[scroll.perpDim] + getPerpOffset();
		}

		this.moveHandle();
	};

	this.remove = function() {
		parent.removeChild(handle);
		parent.removeChild(back);
		handle.removeEventListener(MOUSE_OVER, onMouseOver);
		handle.removeEventListener(MOUSE_OUT, onMouseOut);
		handle.removeEventListener(MOUSE_DOWN, onMouseDown);
	};

	function onMouseOver(e) {
		Tween(handle, 0.35, {backgroundColor:scroll.hover});
	}

	function onMouseOut(e) {
		if(!scroll.ing) {
			Tween(handle, 0.35, {backgroundColor:scroll.color});
		}
	}

	function onMouseDown(e) {
		scroll.ing = true;
		Tween(handle, 0.35, {backgroundColor:scroll.hover});
		startPoint = getPoint(e) - back[scroll.axis] - handle[scroll.axis];
		stage.addEventListener(MOUSE_UP, onMouseUp, false);
		stage.addEventListener(MOUSE_MOVE, onMouseMove, false);
	}

	function onMouseMove(e) {
		scroll.ing = true;
		moveDist = getPoint(e) - startPoint - back[scroll.axis];
		scroll.percent = (moveDist - back[scroll.axis]) / (back[scroll.dim] - handle[scroll.dim]);
		scroll.update(0.35);
	}

	function onMouseUp(event) {
		setTimeout(function(){
			scroll.ing = false;
		}, 10);
		Tween(handle, 0.35, {backgroundColor:scroll.color});
		stage.removeEventListener(MOUSE_MOVE, onMouseMove);
		stage.removeEventListener(MOUSE_UP, onMouseUp);
	}

	function getPoint(e) {
		if(scroll.axis === 'x') {
			return e.clientX;
		}
		return e.clientY;
	}

	function getOffset() {
		if(scroll.axis === 'x') {
			return scroll.offsetX;
		}
		return scroll.offsetY;
	}

	function getPerpOffset() {
		if(scroll.axis === 'y') {
			return scroll.offsetX;
		}
		return scroll.offsetY;
	}

}