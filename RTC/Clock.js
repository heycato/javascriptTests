function Clock() {
	var clock = new Element({type:'button'}),
		startTime, interval;

	clock._width = 80;
	clock._height = 40;
	clock._backgroundColor = '#555555';
	clock._color = '#888888';
	clock._fontSize = 12;
	clock._textAlign = 'center';
	clock.innerHTML = '0:0:0';
	clock.id = 'clock';
	clock._border = 'none';
	clock._outline = 0;

	function update() {
		clock.innerHTML = buildTimeString();
	}

	function buildTimeString() {
		var time = new Date().getTime(),
			diff = time - startTime,
			secs = diff/1000,
			minutes = secs / 60;
		secs = Math.floor(secs % 60);
		var hours = Math.floor(minutes / 60);
		minutes = Math.floor(minutes % 60);
		return hours + ':' + minutes + ':' + secs;
	}

	clock.start = function() {
		startTime = new Date().getTime();
		interval = setInterval(update, 1000);
	};

	clock.stop = function() {
		clearInterval(interval);
	};

	return clock;
}