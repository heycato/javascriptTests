function Element() {
	var vars = UTIL.argsToVars(arguments),
		self = document.createElement(vars.type || 'div'),
		name = 'Element',
		msg = vars.msg || GLOBALS.MSG,
		tweenEndTimer,
		pixelValues = CONSTANTS.STYLE_PIXEL_VALUES;
	self._private = {};

	for(var key in self.style) {
		(function() {
			var propName = key;
			Object.defineProperty(self, '_' + propName, {
				get: function() { return self._private[propName] },
				set: function(value) { 
					self._private[propName] = value;
					self.style[propName] = pixelValues.indexOf(propName) > -1 ? value + 'px' : value;
				}
			});
		})();
	}

	self._position = 'absolute';
	self._display = 'block';
	self._padding = 0;
	self._margin = 0;
	self.childElements = [];
	self.messages = {};

	self.listen = function(route, callback) {
		if(!self.messages[route]) self.messages[route] = {};
		self.messages[route][callback.name] = callback;
		self.addEventListener(route, callback, false);
	};

	self.ignore = function(route, callback) {
		if(self.messages[route] && self.messages[route][callback.name]) {
			delete self.messages[route][callback.name];
			if(UTIL.objectIsEmpty(self.messages[route])) delete self.messages[route];
		}
		self.removeEventListener(route, callback, false);
	};

	self.addChild = function(child) {
		child.parentElement = self;
		if(child.checkTopParent() === 'View') child.init.call(child);
		self.childElements.push(child);
		self.appendChild(child);
	};

	self.removeChild = function(child) {
		self.childElements.splice(self.childElements.indexOf(child), 1);
		self.removeChild(child);
	};

	self.checkTopParent = function() {
		return self.parentElement ? self.parentElement.checkTopParent() : name;
	};

	self.tween = function(properties, duration, delay) {
		var dur =  duration || CONSTANTS.TRANSITION_SPEED;

		self.style.transition = 'all ' + dur + 's';

		for(var key in properties) {
			if(properties.hasOwnProperty(key)) self[key] = properties[key];
		}
		
		clearTimeout(tweenEndTimer);
		tweenEndTimer = setTimeout(function() {
			self.style.transition = 'none';
			msg.notify(ROUTES.TWEEN_COMPLETE, {target:self, properties:properties});
		}, dur * 1000);
	};

	self.init = function() {
		var i = self.children.length;
		while(i--) {
			self.children[i].init();
		}
		self.style['width'] = self.style['width'] || self.offsetWidth;
		self.style['height'] = self.style['height'] || self.offsetHeight;
	};

	return self;

}