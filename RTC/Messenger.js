function Messenger() {

	function isInArray(item, arr) {
		var i = arr.length;
		while(i --) {
			if(item.callback === arr[i].callback && item.target === arr[i].target) {
				return true;
			}
		}
		return false;
	}

	return {
		messages: {},
		listen: function(type, callback) {
			if(!this.messages[type]) this.messages[type] = [];
			var msg = {type:type, callback:callback, target:this};
			if(!isInArray(msg, this.messages[type])) {
				this.messages[type].push(msg);
			} else {
				console.error('listen - type, callback and target already exist');
			}
		},
		ignore: function(type, callback) {
			if(!this.messages[type]) {
				console.error('ignore - type not registered: ', callback);
			}
			if(!callback) {
				delete this.messages[type]; 
				return;
			}
			var i = this.messages[type].length;
			while(i --) {
				if(this.messages[type][i].callback === callback && this.messages[type][i].target === this) {
					this.messages[type].splice(i, 1);
				}
			}
		},
		notify: function(type, data) {
			if(this.messages[type]) {
				var i = this.messages[type].length;
				while(i --) {
					if(!this.messages[type][i].callback) {
						console.error('notify - invalid callback: ', this.messages[type][i]); 
						return;
					}
					if(this.messages[type][i].target === this) {
						this.messages[type][i].callback.apply(this.messages[type][i].target, [data]);
					}
				}
			}
		}
	}
}
