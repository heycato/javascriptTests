function ChatBubble(text, side) {

	var self = new Element(),
		hAlign = side;

	self._border = 'none';
	self._borderRadius = 6;
	self._color = '#FFFFFF';
	self._padding = 5;
	self._fontFamily = 'Lucida Grande';
	self._fontSize = 12;
	self._textAlign = hAlign === 'right' ? 'right' : 'left';
	self.innerHTML = text;
	self._maxWidth = 200;
	self._wordWrap = 'break-word';

	self.updateSize = function(parentWidth) {
		var tempAlign = hAlign === 'right' ? parentWidth - self.offsetWidth : 0;
		self._left = tempAlign;
	};

	return self;
}
