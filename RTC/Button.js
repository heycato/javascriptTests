function Button(label) {
	var btn = new Element({type:'button'});

	btn._width = 60;
	btn._height = 40;
	btn._backgroundColor = '#555555';
	btn._color = '#CCCCCC';
	btn._fontSize = 12;
	btn.innerHTML = label;
	btn.id = label;
	btn._border = 'none';
	btn._outline = 0;

	btn.listen(ROUTES.MOUSE_OVER, mouseOver);
	btn.listen(ROUTES.CLICK, function() {
		GLOBALS.MSG.notify(label);
	});

	btn.disableMouseOver = function() {
		btn.ignore(ROUTES.MOUSE_OVER, mouseOver);
	};

	btn.enableMouseOver = function() {
		btn.listen(ROUTES.MOUSE_OVER, mouseOver);
	};

	function mouseOver() {
		btn.tween({_backgroundColor:'#444444'});
		btn.listen(ROUTES.MOUSE_OUT, mouseOut);
	}

	function mouseOut() {
		btn.tween({_backgroundColor:'#555555'});
		btn.ignore(ROUTES.MOUSE_OUT, mouseOut);
	}

	return btn;
}
