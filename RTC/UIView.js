function UIView() {

	var self = new Element();

	self._width = GLOBALS.WIDTH;
	self._height = 40;
	self._left = 0;
	self._top = 0;
	self._backgroundColor = '#555555';

	self.callBtn = new Button('call');
	self.endBtn = new Button('end');
	self.answerBtn = new Button('answer');
	self.muteBtn = new Button('mute');
	self.clockDisplay = new Clock();
	self.localAudio = new Element({type:'video'});
	self.remoteAudio = new Element({type:'video'});
	self.audioAlert = new Element({type:'audio'});
	self.chatView = new ChatView();
	self.messageInput = new Input();

	self.localAudio._width = 0;
	self.localAudio._height = 0;
	self.localAudio._backgroundColor = '#000000';

	self.remoteAudio._width = 0;
	self.remoteAudio._height = 0;
	self.remoteAudio._backgroundColor = '#000000';

	self.callBtn._left = 0;
	self.callBtn._top = 0;

	self.endBtn._left = 0;
	self.endBtn._top = 0;
	self.endBtn._display = 'none';
	self.endBtn._opacity = 0;

	self.answerBtn._left = 0;
	self.answerBtn._top = 0;
	self.answerBtn._display = 'none';
	self.answerBtn._opacity = 0;

	self.muteBtn._left = self.callBtn._width + self.callBtn._left;
	self.muteBtn._top = 0;
	self.muteBtn.disabled = true;
	self.muteBtn._color = '#888888';

	self.clockDisplay._left = GLOBALS.WIDTH - 80;
	self.clockDisplay._top = 0;

	self.messageInput._width = GLOBALS.WIDTH;
	self.messageInput._height = 40;
	self.messageInput._left = 0;
	self.messageInput._top = GLOBALS.HEIGHT - 40;

	self.addChild(self.callBtn);
	self.addChild(self.endBtn);
	self.addChild(self.answerBtn);
	self.addChild(self.muteBtn);
	self.addChild(self.clockDisplay);
	self.addChild(self.remoteAudio);
	self.addChild(self.localAudio);
	self.addChild(self.audioAlert);
	self.addChild(self.chatView);
	self.addChild(self.messageInput);
	GLOBALS.VIEW.addChild(self);

	self.startAlertPulse = function(btn) {
		btn.pulseTimer = setInterval(function() {
			btn.tween({_backgroundColor: btn._backgroundColor === '#444444' ? '#555555' : '#444444'});
		},500);
	};

	self.endAlertPulse = function(btn) {
		clearTimeout(btn.pulseTimer);
	};

	return self;
}
