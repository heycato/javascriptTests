function View() {
	var self = document.body,
		name = 'View',
		pixelValues = CONSTANTS.STYLE_PIXEL_VALUES;

	self.style.width = window.innerWidth;
	self.style.height = window.innerHeight;
	self.style.padding = '0px';
	self.style.margin = '0px';
	self.style.overFlow = 'hidden';
	self.messages = {};

	self._private = {};

	for(var key in self.style) {
		(function() {
			var propName = key;
			Object.defineProperty(self, '_' + propName, {
				get: function() { return self._private[propName] },
				set: function(value) { 
					self._private[propName] = value;
					self.style[propName] = pixelValues.indexOf(propName) > -1 ? value + 'px' : value;
				}
			});
		})();
	}

	self.listen = function(route, callback) {
		if(!self.messages[route]) self.messages[route] = {};
		self.messages[route][callback.name] = callback;
		self.addEventListener(route, callback, false);
	};

	self.ignore = function(route, callback) {
		if(self.messages[route] && self.messages[route][callback.name]) {
			delete self.messages[route][callback.name];
			if(UTIL.objectIsEmpty(self.messages[route])) delete self.messages[route];
		}
		self.removeEventListener(route, callback, false);
	};

	self.addChild = function(child) {
		self.appendChild(child);
		child.parentElement = self;
		child.init.call(child);
	};

	self.removeChild = function(child) {
		self.removeChild(child);
	};

	self.checkTopParent = function() {
		return name;
	};

	return self;
}