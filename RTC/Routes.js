var ROUTES = function() {
	// place for constant routes
	var load = 'load',
		resize = 'resize',

		click = 'click',
		mouseover = 'mouseover',
		mouseout = 'mouseout',
		focus = 'focus',
		blur = 'blur',
		keyDown = 'keydown',

		call = 'call',
		callInitialized = 'callInitialized',
		callEnd = 'end',
		callEnded = 'callEnded',
		callIncoming = 'callIncoming',
		callAnswer = 'answer',
		callAnswered = 'callAnswered',
		callMute = 'mute',

		messageSend = 'messageSend',
		messageReceived = 'messageReceived',
		updateChat = 'updateChat',

		streamAdded = 'streamAdded',

		tweenComplete = 'tweenComplete',
		mutation = 'mutation';

	return {
		get LOAD() { return load },
		get RESIZE() { return resize },

		get CLICK() { return click },
		get MOUSE_OVER() { return mouseover },
		get MOUSE_OUT() { return mouseout },
		get FOCUS() { return focus },
		get BLUR() { return blur },
		get KEY_DOWN() { return keyDown },

		get CALL() { return call },
		get CALL_INITIALIZED() { return callInitialized },
		get END() { return callEnd },
		get CALL_ENDED() { return callEnded },
		get CALL_INCOMING() { return callIncoming },
		get ANSWER() { return callAnswer },
		get CALL_ANSWERED() { return callAnswered },
		get MUTE() { return callMute },

		get MESSAGE_SEND() { return messageSend },
		get MESSAGE_RECEIVED() { return messageReceived },
		get UPDATE_CHAT() { return updateChat },

		get STREAM_ADDED() { return streamAdded },

		get TWEEN_COMPLETE() { return tweenComplete },
		get MUTATION() { return mutation }
	}
}();