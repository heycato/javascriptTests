function ChatView() {
	var self = new Element(),
		gap = 10;

	self._width = GLOBALS.WIDTH - 10;
	self._height = GLOBALS.HEIGHT - 90;
	self._left = 5;
	self._top = 45;
	self._overflow = 'auto';

	self.updateSize = function(e) {
		self.tween({_width: e.width - 10, _height: e.height - 90});
		vAlignChildren();
	};

	self.newLocalBubble = function(text) {
		var bubble = new ChatBubble(text, 'left');
		bubble._backgroundColor = '#666666';
		bubble._color = '#FFFFFF';
		self.addChild(bubble);
		bubble.updateSize(self._width);
		bubble._top = getY(bubble);
		self.scrollTop = bubble._top
	};

	self.newRemoteBubble = function(text) {
		var bubble = new ChatBubble(text, 'right');
		bubble._backgroundColor = '#CCCCCC';
		bubble._color = '#555555';
		self.addChild(bubble);
		bubble.updateSize(self._width);
		bubble._top = getY(bubble);
		self.scrollTop = bubble._top;
	};

	function getY(el) {
		var i = self.childElements.indexOf(el),
			prevChild = (i !== -1) ? self.childElements[i - 1] : undefined;

		return prevChild ? prevChild._top + prevChild.offsetHeight + gap : 0;
	}

	function vAlignChildren() {
		var len = self.childElements.length, i = 0;
		for(; i < len; i ++) {
			var curChild = self.childElements[i];
			curChild.updateSize(self._width);
			curChild._top = getY(curChild);
		}
	}

	return self;

}
