function App() {
	var ui = new UIController(),
		rtc = new RTCController({
			servers: {
				'iceServers': [{
					'url':'stun:stun.phoneserve.com'
				}]
			}
		});
	GLOBALS.VIEW._backgroundColor = '#333333';
}
var resizeTimer;
window.addEventListener(ROUTES.LOAD, App);
window.addEventListener(ROUTES.RESIZE, function() {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function() {
		GLOBALS.MSG.notify(ROUTES.RESIZE, {width:window.innerWidth, height:window.innerHeight});
	}, 200);
});

function triggerCall() {
	GLOBALS.MSG.notify(ROUTES.CALL_INCOMING);
}


