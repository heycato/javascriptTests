function Input(val) {
	var input = new Element({type:'input'}),
		value = val || 'enter chat message here';
	input._width = 120;
	input._height = 40;
	input._border = 'none';
	input._outline = 0;
	input._textAlign = 'center';
	input._backgroundColor = '#555555';
	input._color = '#888888';
	input._fontSize = 12;
	input.value = value;
	input.listen(ROUTES.FOCUS, inputFocus);
	input.listen(ROUTES.MOUSE_OVER, mouseOver);

	function mouseOver() {
		input.tween({_backgroundColor:'#444444'});
		input.listen(ROUTES.MOUSE_OUT, mouseOut);
	}

	function mouseOut() {
		input.tween({_backgroundColor:'#555555'});
		input.ignore(ROUTES.MOUSE_OUT, mouseOut);
	}

	function inputFocus() {
		input.value = '';
		input.tween({_color: '#CCCCCC'});
		input.listen(ROUTES.BLUR, inputBlur);
		input.listen(ROUTES.KEY_DOWN, checkKey);
	}

	function checkKey(e) {
		if(e.which === 13 && input.value !== 'enter chat message here' && input.value !== '') {
			GLOBALS.MSG.notify(ROUTES.MESSAGE_SEND, {value:input.value});
			input.value = '';
		}
	}

	function inputBlur() {
		// check for valid ip address format
		input._color = '#888888';
		input.ignore(ROUTES.KEY_DOWN, checkKey);
		if(input.value === '') {
			input.value = value;
		}
	}

	return input;
}
