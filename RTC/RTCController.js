function RTCController() {

	var self = this,
		vars = UTIL.argsToVars(arguments),
		locPC,
		rmtPC,
		servers = vars.servers || null;

	GLOBALS.MSG.listen(ROUTES.CALL, call);
	GLOBALS.MSG.listen(ROUTES.END, callEnd);
	GLOBALS.MSG.listen(ROUTES.ANSWER, callAnswer);
	GLOBALS.MSG.listen(ROUTES.MESSAGE_SEND, messageSend);
	GLOBALS.MSG.listen(ROUTES.MESSAGE_RECEIVED, messageReceived);

	function call() {
		getUserMedia({audio:true}, function(e) {
			locPC.addStream(e);
			locPC.createOffer(getLocalDescription, err);
			addStream({stream:URL.createObjectURL(e), type:'local'});
		}, err);

		locPC = new RTCPeerConnection(servers);
		locPC.onicecandidate = getLocalIceCandidate;

		rmtPC = new RTCPeerConnection(servers);
		rmtPC.onicecandidate = getRemoteIceCandidate;
		rmtPC.onaddstream = function(e) {
			addStream({stream:URL.createObjectURL(e.stream), type:'remote'});
		};

		GLOBALS.MSG.notify(ROUTES.CALL_INITIALIZED);
	}

	function callEnd() {
		locPC.close();
		rmtPC.close();
		locPC = null;
		rmtPC = null;
		GLOBALS.MSG.notify(ROUTES.CALL_ENDED);
	}

	function callIncoming() {

		GLOBALS.MSG.notify(ROUTES.CALL_INCOMING);
	}

	function callAnswer() {
		call();
		GLOBALS.MSG.notify(ROUTES.CALL_ANSWERED);
	}

	function messageSend(e) {
		e.type = 'local';
		GLOBALS.MSG.notify(ROUTES.UPDATE_CHAT, e);
	}

	function messageReceived(e) {
		e.type = 'remote';
		GLOBALS.MSG.notify(ROUTES.UPDATE_CHAT, e);
	}

	function err(e) {
		console.warn('ERROR:', e);
	}


	function getLocalDescription(description){
		locPC.setLocalDescription(description);
		rmtPC.setRemoteDescription(description);
		rmtPC.createAnswer(getRemoteDescription, err);
	}

	function getRemoteDescription(description){
		rmtPC.setLocalDescription(description);
		locPC.setRemoteDescription(description);
	}

	function getLocalIceCandidate(event){
		if (event.candidate) {
			rmtPC.addIceCandidate(new RTCIceCandidate(event.candidate));
		}
	}

	function getRemoteIceCandidate(event){
		if (event.candidate) {
			locPC.addIceCandidate(new RTCIceCandidate(event.candidate));
		}
	}

	function addStream(e) {
		GLOBALS.MSG.notify(ROUTES.STREAM_ADDED, e);
	}

	return self;
}
