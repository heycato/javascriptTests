function Observer() {
	var vars = (arguments[0] && (typeof arguments[0] === 'object')) ? arguments[0] : {},
		msg = vars.msg || GLOBALS.MSG;
		self = new MutationObserver(function(e) { msg.notify(ROUTES.MUTATION , e) });

	return self;
}
