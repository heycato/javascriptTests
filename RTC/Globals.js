var GLOBALS = function() {
	// place for singletons
	
	var view = new View(),
		msg = new Messenger(),
		chatLog = {},

		width = window.innerWidth,
		height = window.innerHeight;

	msg.listen(ROUTES.RESIZE, function(e) {
		width = e.width;
		height = e.height;
		view._width = e.width;
		view._height = e.height;
	});

	return {
		get VIEW() { return view },
		get MSG() { return msg },
		get CHAT_LOG() { return chatLog },
		get WIDTH() { return width },
		set WIDTH(val) { width = val },
		get HEIGHT() { return height },
		set HEIGHT(val) { height = val }
	};

}();