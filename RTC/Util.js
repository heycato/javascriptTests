var UTIL = function() {
	
	var self = this;

	function objectIsEmpty(obj) {
		for(var key in obj) {
			if(!obj.hasOwnProperty(key)) {
				return true;
			}
		}
		return false;
	}

	function argsToVars(args) {
		return (args[0] && (typeof args[0] === 'object')) ? args[0] : {};
	}

	function getWindowSize() {
		GLOBALS.WIDTH = window.innerWidth;
		GLOBALS.HEIGHT = window.innerHeight;
		return {width:GLOBALS.WIDTH, height:GLOBALS.HEIGHT};
	}

	return {
		objectIsEmpty: objectIsEmpty,
		argsToVars: argsToVars,
		getWindowSize: getWindowSize
	}

}();