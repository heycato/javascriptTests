function UIController() {
	var self = this,
		uiView = new UIView();

	GLOBALS.MSG.listen(ROUTES.CALL_INITIALIZED, callInitialized);
	GLOBALS.MSG.listen(ROUTES.CALL_ENDED, callEnded);
	GLOBALS.MSG.listen(ROUTES.CALL_INCOMING, callIncoming);
	GLOBALS.MSG.listen(ROUTES.CALL_ANSWERED, callAnswered);
	GLOBALS.MSG.listen(ROUTES.MUTE, callMute);
	GLOBALS.MSG.listen(ROUTES.STREAM_ADDED, streamAdded);
	GLOBALS.MSG.listen(ROUTES.RESIZE, updateSize);
	GLOBALS.MSG.listen(ROUTES.UPDATE_CHAT, updateChat);

	function callInitialized() {
		uiView.muteBtn.disabled = false;
		uiView.muteBtn._color = '#CCCCCC';

		uiView.callBtn.tween({_opacity:0});
		uiView.callBtn._display = 'none';

		uiView.endBtn._display = 'block';
		uiView.endBtn.tween({_opacity:1});
	}

	function callEnded() {
		uiView.localAudio.pause();
		uiView.localAudio.muted = true;
		uiView.localAudio.src = '';

		uiView.remoteAudio.pause();
		uiView.remoteAudio.src = '';

		setTimeout(function() {
			uiView.localAudio.tween({_top: GLOBALS.HEIGHT - uiView._height, _height:0});
			uiView.remoteAudio.tween({_top: GLOBALS.HEIGHT - uiView._height, _height:0});
		}, 1000);

		uiView.muteBtn.disabled = true;
		callMute();
		uiView.muteBtn._color = '#888888';

		uiView.endBtn.tween({_opacity:0});
		uiView.endBtn._display = 'none';

		uiView.callBtn._display = 'block';
		uiView.callBtn.tween({_opacity:1});

		uiView.clockDisplay.stop();
		uiView.clockDisplay._color = '#888888';
	}

	function callIncoming() {
		uiView.audioAlert.src = 'alertTone.mp3';
		uiView.audioAlert.play();

		uiView.callBtn.tween({_opacity:0});
		uiView.callBtn._display = 'none';

		uiView.answerBtn._display = 'block';
		uiView.answerBtn.tween({_opacity:1});
		uiView.answerBtn.tween({_color:'#0088FF'});
		uiView.startAlertPulse(uiView.answerBtn);
		uiView.answerBtn.disableMouseOver();
	}

	function callAnswered() {
		uiView.muteBtn.disabled = false;
		uiView.muteBtn._color = '#CCCCCC';
		uiView.audioAlert.pause();
		uiView.audioAlert.src = '';

		uiView.answerBtn.tween({_color:'#888888'});
		uiView.endAlertPulse(uiView.answerBtn);
		uiView.answerBtn.enableMouseOver();
		uiView.answerBtn.tween({_opacity:0});
		uiView.answerBtn._display = 'none';

		uiView.endBtn._display = 'block';
		uiView.endBtn.tween({_opacity:1});
	}

	function callMute() {
		if(uiView.localAudio.muted) {
			uiView.localAudio.muted = false;
			uiView.muteBtn.tween({_color:'#CCCCCC'});
			uiView.endAlertPulse(uiView.muteBtn);
			uiView.muteBtn.enableMouseOver();
		} else {
			uiView.localAudio.muted = true;
			uiView.muteBtn.tween({_color:'#FF0000'});
			uiView.startAlertPulse(uiView.muteBtn);
			uiView.muteBtn.disableMouseOver();
		}
		uiView.muteBtn.tween({_backgroundColor:'#555555'});
	}

	function streamAdded(e) {
		if(e.type === 'remote') {
			setRemoteStream(e.stream);
		} else if(e.type === 'local') {
			setLocalStream(e.stream);
		}
	}

	function setRemoteStream(stream) {
		uiView.remoteAudio.src = stream;
		uiView.remoteAudio.play();
	}

	function setLocalStream(stream) {
		uiView.muteBtn.disabled = false;
		uiView.clockDisplay.start();
		uiView.clockDisplay.tween({_color: '#CCCCCC'});
		uiView.localAudio.src = stream;
		uiView.localAudio.play();
	}

	var turn = false;
	function updateChat(e) {
		if(turn) {
			uiView.chatView.newLocalBubble(e.value);
			turn = false;
		} else {
			uiView.chatView.newRemoteBubble(e.value);
			turn = true;
		}
		/*if(e.type === 'remote') {
			console.log('make a new remote chat bubble with message:', e.value);
			uiView.chatView.newRemoteBubble(e.value);
		} else if(e.type === 'local') {
			console.log('make a new local chat bubble with message:', e.value);
			uiView.chatView.newRemoteBubble(e.value);
		}*/
	}

	function updateSize(e) {
		uiView.tween({_width: e.width});
		uiView.clockDisplay.tween({_left: e.width - 80});
		uiView.messageInput.tween({_width: e.width, _top: e.height - 40});
		uiView.chatView.updateSize(e);
	}

	return self;
}
